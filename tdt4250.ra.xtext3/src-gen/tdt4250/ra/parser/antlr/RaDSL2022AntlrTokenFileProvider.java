/*
 * generated by Xtext 2.27.0
 */
package tdt4250.ra.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class RaDSL2022AntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("tdt4250/ra/parser/antlr/internal/InternalRaDSL2022.tokens");
	}
}
