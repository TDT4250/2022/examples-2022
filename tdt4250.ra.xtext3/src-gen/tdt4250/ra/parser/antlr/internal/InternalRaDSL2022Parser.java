package tdt4250.ra.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import tdt4250.ra.services.RaDSL2022GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRaDSL2022Parser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Department'", "'{'", "'shortName'", "'staff'", "','", "'}'", "'courses'", "'resourceAllocations'", "'Person'", "'Course'", "'code'", "'roles'", "'ResourceAllocation'", "'factor'", "'person'", "'course'", "'role'", "'Role'", "'workload'", "'-'", "'.'", "'E'", "'e'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRaDSL2022Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRaDSL2022Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRaDSL2022Parser.tokenNames; }
    public String getGrammarFileName() { return "InternalRaDSL2022.g"; }



     	private RaDSL2022GrammarAccess grammarAccess;

        public InternalRaDSL2022Parser(TokenStream input, RaDSL2022GrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Department";
       	}

       	@Override
       	protected RaDSL2022GrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDepartment"
    // InternalRaDSL2022.g:64:1: entryRuleDepartment returns [EObject current=null] : iv_ruleDepartment= ruleDepartment EOF ;
    public final EObject entryRuleDepartment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDepartment = null;


        try {
            // InternalRaDSL2022.g:64:51: (iv_ruleDepartment= ruleDepartment EOF )
            // InternalRaDSL2022.g:65:2: iv_ruleDepartment= ruleDepartment EOF
            {
             newCompositeNode(grammarAccess.getDepartmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDepartment=ruleDepartment();

            state._fsp--;

             current =iv_ruleDepartment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDepartment"


    // $ANTLR start "ruleDepartment"
    // InternalRaDSL2022.g:71:1: ruleDepartment returns [EObject current=null] : ( () otherlv_1= 'Department' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )? (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )? (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )? (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )? otherlv_24= '}' ) ;
    public final EObject ruleDepartment() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_shortName_5_0 = null;

        EObject lv_staff_8_0 = null;

        EObject lv_staff_10_0 = null;

        EObject lv_courses_14_0 = null;

        EObject lv_courses_16_0 = null;

        EObject lv_resourceAllocations_20_0 = null;

        EObject lv_resourceAllocations_22_0 = null;



        	enterRule();

        try {
            // InternalRaDSL2022.g:77:2: ( ( () otherlv_1= 'Department' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )? (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )? (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )? (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )? otherlv_24= '}' ) )
            // InternalRaDSL2022.g:78:2: ( () otherlv_1= 'Department' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )? (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )? (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )? (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )? otherlv_24= '}' )
            {
            // InternalRaDSL2022.g:78:2: ( () otherlv_1= 'Department' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )? (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )? (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )? (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )? otherlv_24= '}' )
            // InternalRaDSL2022.g:79:3: () otherlv_1= 'Department' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )? (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )? (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )? (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )? otherlv_24= '}'
            {
            // InternalRaDSL2022.g:79:3: ()
            // InternalRaDSL2022.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDepartmentAccess().getDepartmentAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getDepartmentAccess().getDepartmentKeyword_1());
            		
            // InternalRaDSL2022.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalRaDSL2022.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalRaDSL2022.g:91:4: (lv_name_2_0= ruleEString )
            // InternalRaDSL2022.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDepartmentAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDepartmentRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"tdt4250.ra.RaDSL2022.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getDepartmentAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalRaDSL2022.g:113:3: (otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalRaDSL2022.g:114:4: otherlv_4= 'shortName' ( (lv_shortName_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getDepartmentAccess().getShortNameKeyword_4_0());
                    			
                    // InternalRaDSL2022.g:118:4: ( (lv_shortName_5_0= ruleEString ) )
                    // InternalRaDSL2022.g:119:5: (lv_shortName_5_0= ruleEString )
                    {
                    // InternalRaDSL2022.g:119:5: (lv_shortName_5_0= ruleEString )
                    // InternalRaDSL2022.g:120:6: lv_shortName_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDepartmentAccess().getShortNameEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_shortName_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    						}
                    						set(
                    							current,
                    							"shortName",
                    							lv_shortName_5_0,
                    							"tdt4250.ra.RaDSL2022.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRaDSL2022.g:138:3: (otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRaDSL2022.g:139:4: otherlv_6= 'staff' otherlv_7= '{' ( (lv_staff_8_0= rulePerson ) ) (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )* otherlv_11= '}'
                    {
                    otherlv_6=(Token)match(input,14,FOLLOW_4); 

                    				newLeafNode(otherlv_6, grammarAccess.getDepartmentAccess().getStaffKeyword_5_0());
                    			
                    otherlv_7=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_7, grammarAccess.getDepartmentAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalRaDSL2022.g:147:4: ( (lv_staff_8_0= rulePerson ) )
                    // InternalRaDSL2022.g:148:5: (lv_staff_8_0= rulePerson )
                    {
                    // InternalRaDSL2022.g:148:5: (lv_staff_8_0= rulePerson )
                    // InternalRaDSL2022.g:149:6: lv_staff_8_0= rulePerson
                    {

                    						newCompositeNode(grammarAccess.getDepartmentAccess().getStaffPersonParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_staff_8_0=rulePerson();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    						}
                    						add(
                    							current,
                    							"staff",
                    							lv_staff_8_0,
                    							"tdt4250.ra.RaDSL2022.Person");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalRaDSL2022.g:166:4: (otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==15) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalRaDSL2022.g:167:5: otherlv_9= ',' ( (lv_staff_10_0= rulePerson ) )
                    	    {
                    	    otherlv_9=(Token)match(input,15,FOLLOW_7); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getDepartmentAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalRaDSL2022.g:171:5: ( (lv_staff_10_0= rulePerson ) )
                    	    // InternalRaDSL2022.g:172:6: (lv_staff_10_0= rulePerson )
                    	    {
                    	    // InternalRaDSL2022.g:172:6: (lv_staff_10_0= rulePerson )
                    	    // InternalRaDSL2022.g:173:7: lv_staff_10_0= rulePerson
                    	    {

                    	    							newCompositeNode(grammarAccess.getDepartmentAccess().getStaffPersonParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_staff_10_0=rulePerson();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"staff",
                    	    								lv_staff_10_0,
                    	    								"tdt4250.ra.RaDSL2022.Person");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_11, grammarAccess.getDepartmentAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalRaDSL2022.g:196:3: (otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalRaDSL2022.g:197:4: otherlv_12= 'courses' otherlv_13= '{' ( (lv_courses_14_0= ruleCourse ) ) (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )* otherlv_17= '}'
                    {
                    otherlv_12=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_12, grammarAccess.getDepartmentAccess().getCoursesKeyword_6_0());
                    			
                    otherlv_13=(Token)match(input,12,FOLLOW_10); 

                    				newLeafNode(otherlv_13, grammarAccess.getDepartmentAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalRaDSL2022.g:205:4: ( (lv_courses_14_0= ruleCourse ) )
                    // InternalRaDSL2022.g:206:5: (lv_courses_14_0= ruleCourse )
                    {
                    // InternalRaDSL2022.g:206:5: (lv_courses_14_0= ruleCourse )
                    // InternalRaDSL2022.g:207:6: lv_courses_14_0= ruleCourse
                    {

                    						newCompositeNode(grammarAccess.getDepartmentAccess().getCoursesCourseParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_courses_14_0=ruleCourse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    						}
                    						add(
                    							current,
                    							"courses",
                    							lv_courses_14_0,
                    							"tdt4250.ra.RaDSL2022.Course");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalRaDSL2022.g:224:4: (otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==15) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalRaDSL2022.g:225:5: otherlv_15= ',' ( (lv_courses_16_0= ruleCourse ) )
                    	    {
                    	    otherlv_15=(Token)match(input,15,FOLLOW_10); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getDepartmentAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalRaDSL2022.g:229:5: ( (lv_courses_16_0= ruleCourse ) )
                    	    // InternalRaDSL2022.g:230:6: (lv_courses_16_0= ruleCourse )
                    	    {
                    	    // InternalRaDSL2022.g:230:6: (lv_courses_16_0= ruleCourse )
                    	    // InternalRaDSL2022.g:231:7: lv_courses_16_0= ruleCourse
                    	    {

                    	    							newCompositeNode(grammarAccess.getDepartmentAccess().getCoursesCourseParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_courses_16_0=ruleCourse();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"courses",
                    	    								lv_courses_16_0,
                    	    								"tdt4250.ra.RaDSL2022.Course");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,16,FOLLOW_11); 

                    				newLeafNode(otherlv_17, grammarAccess.getDepartmentAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalRaDSL2022.g:254:3: (otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRaDSL2022.g:255:4: otherlv_18= 'resourceAllocations' otherlv_19= '{' ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) ) (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )* otherlv_23= '}'
                    {
                    otherlv_18=(Token)match(input,18,FOLLOW_4); 

                    				newLeafNode(otherlv_18, grammarAccess.getDepartmentAccess().getResourceAllocationsKeyword_7_0());
                    			
                    otherlv_19=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_19, grammarAccess.getDepartmentAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalRaDSL2022.g:263:4: ( (lv_resourceAllocations_20_0= ruleResourceAllocation ) )
                    // InternalRaDSL2022.g:264:5: (lv_resourceAllocations_20_0= ruleResourceAllocation )
                    {
                    // InternalRaDSL2022.g:264:5: (lv_resourceAllocations_20_0= ruleResourceAllocation )
                    // InternalRaDSL2022.g:265:6: lv_resourceAllocations_20_0= ruleResourceAllocation
                    {

                    						newCompositeNode(grammarAccess.getDepartmentAccess().getResourceAllocationsResourceAllocationParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_resourceAllocations_20_0=ruleResourceAllocation();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    						}
                    						add(
                    							current,
                    							"resourceAllocations",
                    							lv_resourceAllocations_20_0,
                    							"tdt4250.ra.RaDSL2022.ResourceAllocation");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalRaDSL2022.g:282:4: (otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==15) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalRaDSL2022.g:283:5: otherlv_21= ',' ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) )
                    	    {
                    	    otherlv_21=(Token)match(input,15,FOLLOW_12); 

                    	    					newLeafNode(otherlv_21, grammarAccess.getDepartmentAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalRaDSL2022.g:287:5: ( (lv_resourceAllocations_22_0= ruleResourceAllocation ) )
                    	    // InternalRaDSL2022.g:288:6: (lv_resourceAllocations_22_0= ruleResourceAllocation )
                    	    {
                    	    // InternalRaDSL2022.g:288:6: (lv_resourceAllocations_22_0= ruleResourceAllocation )
                    	    // InternalRaDSL2022.g:289:7: lv_resourceAllocations_22_0= ruleResourceAllocation
                    	    {

                    	    							newCompositeNode(grammarAccess.getDepartmentAccess().getResourceAllocationsResourceAllocationParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_resourceAllocations_22_0=ruleResourceAllocation();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getDepartmentRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"resourceAllocations",
                    	    								lv_resourceAllocations_22_0,
                    	    								"tdt4250.ra.RaDSL2022.ResourceAllocation");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    otherlv_23=(Token)match(input,16,FOLLOW_13); 

                    				newLeafNode(otherlv_23, grammarAccess.getDepartmentAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_24=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_24, grammarAccess.getDepartmentAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDepartment"


    // $ANTLR start "entryRuleEString"
    // InternalRaDSL2022.g:320:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalRaDSL2022.g:320:47: (iv_ruleEString= ruleEString EOF )
            // InternalRaDSL2022.g:321:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalRaDSL2022.g:327:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalRaDSL2022.g:333:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalRaDSL2022.g:334:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalRaDSL2022.g:334:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalRaDSL2022.g:335:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalRaDSL2022.g:343:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRulePerson"
    // InternalRaDSL2022.g:354:1: entryRulePerson returns [EObject current=null] : iv_rulePerson= rulePerson EOF ;
    public final EObject entryRulePerson() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePerson = null;


        try {
            // InternalRaDSL2022.g:354:47: (iv_rulePerson= rulePerson EOF )
            // InternalRaDSL2022.g:355:2: iv_rulePerson= rulePerson EOF
            {
             newCompositeNode(grammarAccess.getPersonRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePerson=rulePerson();

            state._fsp--;

             current =iv_rulePerson; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePerson"


    // $ANTLR start "rulePerson"
    // InternalRaDSL2022.g:361:1: rulePerson returns [EObject current=null] : ( () otherlv_1= 'Person' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject rulePerson() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalRaDSL2022.g:367:2: ( ( () otherlv_1= 'Person' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalRaDSL2022.g:368:2: ( () otherlv_1= 'Person' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalRaDSL2022.g:368:2: ( () otherlv_1= 'Person' ( (lv_name_2_0= ruleEString ) ) )
            // InternalRaDSL2022.g:369:3: () otherlv_1= 'Person' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalRaDSL2022.g:369:3: ()
            // InternalRaDSL2022.g:370:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPersonAccess().getPersonAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPersonAccess().getPersonKeyword_1());
            		
            // InternalRaDSL2022.g:380:3: ( (lv_name_2_0= ruleEString ) )
            // InternalRaDSL2022.g:381:4: (lv_name_2_0= ruleEString )
            {
            // InternalRaDSL2022.g:381:4: (lv_name_2_0= ruleEString )
            // InternalRaDSL2022.g:382:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPersonAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPersonRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"tdt4250.ra.RaDSL2022.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePerson"


    // $ANTLR start "entryRuleCourse"
    // InternalRaDSL2022.g:403:1: entryRuleCourse returns [EObject current=null] : iv_ruleCourse= ruleCourse EOF ;
    public final EObject entryRuleCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourse = null;


        try {
            // InternalRaDSL2022.g:403:47: (iv_ruleCourse= ruleCourse EOF )
            // InternalRaDSL2022.g:404:2: iv_ruleCourse= ruleCourse EOF
            {
             newCompositeNode(grammarAccess.getCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourse=ruleCourse();

            state._fsp--;

             current =iv_ruleCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourse"


    // $ANTLR start "ruleCourse"
    // InternalRaDSL2022.g:410:1: ruleCourse returns [EObject current=null] : ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) ) otherlv_13= '}' ) ;
    public final EObject ruleCourse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_code_6_0 = null;

        EObject lv_roles_9_0 = null;

        EObject lv_roles_11_0 = null;



        	enterRule();

        try {
            // InternalRaDSL2022.g:416:2: ( ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) ) otherlv_13= '}' ) )
            // InternalRaDSL2022.g:417:2: ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) ) otherlv_13= '}' )
            {
            // InternalRaDSL2022.g:417:2: ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) ) otherlv_13= '}' )
            // InternalRaDSL2022.g:418:3: () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) ) otherlv_13= '}'
            {
            // InternalRaDSL2022.g:418:3: ()
            // InternalRaDSL2022.g:419:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCourseAccess().getCourseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCourseAccess().getCourseKeyword_1());
            		
            // InternalRaDSL2022.g:429:3: ( (lv_name_2_0= ruleEString ) )
            // InternalRaDSL2022.g:430:4: (lv_name_2_0= ruleEString )
            {
            // InternalRaDSL2022.g:430:4: (lv_name_2_0= ruleEString )
            // InternalRaDSL2022.g:431:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"tdt4250.ra.RaDSL2022.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getCourseAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalRaDSL2022.g:452:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) ) )
            // InternalRaDSL2022.g:453:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) )
            {
            // InternalRaDSL2022.g:453:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* ) )
            // InternalRaDSL2022.g:454:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getCourseAccess().getUnorderedGroup_4());
            				
            // InternalRaDSL2022.g:457:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )* )
            // InternalRaDSL2022.g:458:6: ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )*
            {
            // InternalRaDSL2022.g:458:6: ( ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) ) )*
            loop10:
            do {
                int alt10=3;
                int LA10_0 = input.LA(1);

                if ( LA10_0 == 21 && getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 0) ) {
                    alt10=1;
                }
                else if ( LA10_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 1) ) {
                    alt10=2;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalRaDSL2022.g:459:4: ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalRaDSL2022.g:459:4: ({...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) ) )
            	    // InternalRaDSL2022.g:460:5: {...}? => ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleCourse", "getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalRaDSL2022.g:460:103: ( ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) ) )
            	    // InternalRaDSL2022.g:461:6: ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalRaDSL2022.g:464:9: ({...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) ) )
            	    // InternalRaDSL2022.g:464:10: {...}? => (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCourse", "true");
            	    }
            	    // InternalRaDSL2022.g:464:19: (otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) ) )
            	    // InternalRaDSL2022.g:464:20: otherlv_5= 'code' ( (lv_code_6_0= ruleEString ) )
            	    {
            	    otherlv_5=(Token)match(input,21,FOLLOW_3); 

            	    									newLeafNode(otherlv_5, grammarAccess.getCourseAccess().getCodeKeyword_4_0_0());
            	    								
            	    // InternalRaDSL2022.g:468:9: ( (lv_code_6_0= ruleEString ) )
            	    // InternalRaDSL2022.g:469:10: (lv_code_6_0= ruleEString )
            	    {
            	    // InternalRaDSL2022.g:469:10: (lv_code_6_0= ruleEString )
            	    // InternalRaDSL2022.g:470:11: lv_code_6_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getCourseAccess().getCodeEStringParserRuleCall_4_0_1_0());
            	    										
            	    pushFollow(FOLLOW_14);
            	    lv_code_6_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getCourseRule());
            	    											}
            	    											set(
            	    												current,
            	    												"code",
            	    												lv_code_6_0,
            	    												"tdt4250.ra.RaDSL2022.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCourseAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRaDSL2022.g:493:4: ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) )
            	    {
            	    // InternalRaDSL2022.g:493:4: ({...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) ) )
            	    // InternalRaDSL2022.g:494:5: {...}? => ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleCourse", "getUnorderedGroupHelper().canSelect(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalRaDSL2022.g:494:103: ( ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) ) )
            	    // InternalRaDSL2022.g:495:6: ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCourseAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalRaDSL2022.g:498:9: ({...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' ) )
            	    // InternalRaDSL2022.g:498:10: {...}? => (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCourse", "true");
            	    }
            	    // InternalRaDSL2022.g:498:19: (otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}' )
            	    // InternalRaDSL2022.g:498:20: otherlv_7= 'roles' otherlv_8= '{' ( (lv_roles_9_0= ruleRole ) ) (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )* otherlv_12= '}'
            	    {
            	    otherlv_7=(Token)match(input,22,FOLLOW_4); 

            	    									newLeafNode(otherlv_7, grammarAccess.getCourseAccess().getRolesKeyword_4_1_0());
            	    								
            	    otherlv_8=(Token)match(input,12,FOLLOW_15); 

            	    									newLeafNode(otherlv_8, grammarAccess.getCourseAccess().getLeftCurlyBracketKeyword_4_1_1());
            	    								
            	    // InternalRaDSL2022.g:506:9: ( (lv_roles_9_0= ruleRole ) )
            	    // InternalRaDSL2022.g:507:10: (lv_roles_9_0= ruleRole )
            	    {
            	    // InternalRaDSL2022.g:507:10: (lv_roles_9_0= ruleRole )
            	    // InternalRaDSL2022.g:508:11: lv_roles_9_0= ruleRole
            	    {

            	    											newCompositeNode(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_4_1_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_roles_9_0=ruleRole();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getCourseRule());
            	    											}
            	    											add(
            	    												current,
            	    												"roles",
            	    												lv_roles_9_0,
            	    												"tdt4250.ra.RaDSL2022.Role");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalRaDSL2022.g:525:9: (otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) ) )*
            	    loop9:
            	    do {
            	        int alt9=2;
            	        int LA9_0 = input.LA(1);

            	        if ( (LA9_0==15) ) {
            	            alt9=1;
            	        }


            	        switch (alt9) {
            	    	case 1 :
            	    	    // InternalRaDSL2022.g:526:10: otherlv_10= ',' ( (lv_roles_11_0= ruleRole ) )
            	    	    {
            	    	    otherlv_10=(Token)match(input,15,FOLLOW_15); 

            	    	    										newLeafNode(otherlv_10, grammarAccess.getCourseAccess().getCommaKeyword_4_1_3_0());
            	    	    									
            	    	    // InternalRaDSL2022.g:530:10: ( (lv_roles_11_0= ruleRole ) )
            	    	    // InternalRaDSL2022.g:531:11: (lv_roles_11_0= ruleRole )
            	    	    {
            	    	    // InternalRaDSL2022.g:531:11: (lv_roles_11_0= ruleRole )
            	    	    // InternalRaDSL2022.g:532:12: lv_roles_11_0= ruleRole
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_4_1_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_roles_11_0=ruleRole();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getCourseRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"roles",
            	    	    													lv_roles_11_0,
            	    	    													"tdt4250.ra.RaDSL2022.Role");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop9;
            	        }
            	    } while (true);

            	    otherlv_12=(Token)match(input,16,FOLLOW_14); 

            	    									newLeafNode(otherlv_12, grammarAccess.getCourseAccess().getRightCurlyBracketKeyword_4_1_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCourseAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getCourseAccess().getUnorderedGroup_4());
            				

            }

            otherlv_13=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getCourseAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourse"


    // $ANTLR start "entryRuleResourceAllocation"
    // InternalRaDSL2022.g:575:1: entryRuleResourceAllocation returns [EObject current=null] : iv_ruleResourceAllocation= ruleResourceAllocation EOF ;
    public final EObject entryRuleResourceAllocation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceAllocation = null;


        try {
            // InternalRaDSL2022.g:575:59: (iv_ruleResourceAllocation= ruleResourceAllocation EOF )
            // InternalRaDSL2022.g:576:2: iv_ruleResourceAllocation= ruleResourceAllocation EOF
            {
             newCompositeNode(grammarAccess.getResourceAllocationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceAllocation=ruleResourceAllocation();

            state._fsp--;

             current =iv_ruleResourceAllocation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceAllocation"


    // $ANTLR start "ruleResourceAllocation"
    // InternalRaDSL2022.g:582:1: ruleResourceAllocation returns [EObject current=null] : ( () otherlv_1= 'ResourceAllocation' otherlv_2= '{' (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )? (otherlv_5= 'person' ( ( ruleEString ) ) )? (otherlv_7= 'course' ( ( ruleEString ) ) )? (otherlv_9= 'role' ( ( ruleEString ) ) )? otherlv_11= '}' ) ;
    public final EObject ruleResourceAllocation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_factor_4_0 = null;



        	enterRule();

        try {
            // InternalRaDSL2022.g:588:2: ( ( () otherlv_1= 'ResourceAllocation' otherlv_2= '{' (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )? (otherlv_5= 'person' ( ( ruleEString ) ) )? (otherlv_7= 'course' ( ( ruleEString ) ) )? (otherlv_9= 'role' ( ( ruleEString ) ) )? otherlv_11= '}' ) )
            // InternalRaDSL2022.g:589:2: ( () otherlv_1= 'ResourceAllocation' otherlv_2= '{' (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )? (otherlv_5= 'person' ( ( ruleEString ) ) )? (otherlv_7= 'course' ( ( ruleEString ) ) )? (otherlv_9= 'role' ( ( ruleEString ) ) )? otherlv_11= '}' )
            {
            // InternalRaDSL2022.g:589:2: ( () otherlv_1= 'ResourceAllocation' otherlv_2= '{' (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )? (otherlv_5= 'person' ( ( ruleEString ) ) )? (otherlv_7= 'course' ( ( ruleEString ) ) )? (otherlv_9= 'role' ( ( ruleEString ) ) )? otherlv_11= '}' )
            // InternalRaDSL2022.g:590:3: () otherlv_1= 'ResourceAllocation' otherlv_2= '{' (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )? (otherlv_5= 'person' ( ( ruleEString ) ) )? (otherlv_7= 'course' ( ( ruleEString ) ) )? (otherlv_9= 'role' ( ( ruleEString ) ) )? otherlv_11= '}'
            {
            // InternalRaDSL2022.g:590:3: ()
            // InternalRaDSL2022.g:591:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getResourceAllocationAccess().getResourceAllocationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getResourceAllocationAccess().getResourceAllocationKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getResourceAllocationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalRaDSL2022.g:605:3: (otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalRaDSL2022.g:606:4: otherlv_3= 'factor' ( (lv_factor_4_0= ruleEFloat ) )
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_17); 

                    				newLeafNode(otherlv_3, grammarAccess.getResourceAllocationAccess().getFactorKeyword_3_0());
                    			
                    // InternalRaDSL2022.g:610:4: ( (lv_factor_4_0= ruleEFloat ) )
                    // InternalRaDSL2022.g:611:5: (lv_factor_4_0= ruleEFloat )
                    {
                    // InternalRaDSL2022.g:611:5: (lv_factor_4_0= ruleEFloat )
                    // InternalRaDSL2022.g:612:6: lv_factor_4_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getResourceAllocationAccess().getFactorEFloatParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_factor_4_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResourceAllocationRule());
                    						}
                    						set(
                    							current,
                    							"factor",
                    							lv_factor_4_0,
                    							"tdt4250.ra.RaDSL2022.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRaDSL2022.g:630:3: (otherlv_5= 'person' ( ( ruleEString ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==25) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalRaDSL2022.g:631:4: otherlv_5= 'person' ( ( ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,25,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getResourceAllocationAccess().getPersonKeyword_4_0());
                    			
                    // InternalRaDSL2022.g:635:4: ( ( ruleEString ) )
                    // InternalRaDSL2022.g:636:5: ( ruleEString )
                    {
                    // InternalRaDSL2022.g:636:5: ( ruleEString )
                    // InternalRaDSL2022.g:637:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResourceAllocationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getResourceAllocationAccess().getPersonPersonCrossReference_4_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRaDSL2022.g:652:3: (otherlv_7= 'course' ( ( ruleEString ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==26) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRaDSL2022.g:653:4: otherlv_7= 'course' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,26,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getResourceAllocationAccess().getCourseKeyword_5_0());
                    			
                    // InternalRaDSL2022.g:657:4: ( ( ruleEString ) )
                    // InternalRaDSL2022.g:658:5: ( ruleEString )
                    {
                    // InternalRaDSL2022.g:658:5: ( ruleEString )
                    // InternalRaDSL2022.g:659:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResourceAllocationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getResourceAllocationAccess().getCourseCourseCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRaDSL2022.g:674:3: (otherlv_9= 'role' ( ( ruleEString ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==27) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalRaDSL2022.g:675:4: otherlv_9= 'role' ( ( ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,27,FOLLOW_3); 

                    				newLeafNode(otherlv_9, grammarAccess.getResourceAllocationAccess().getRoleKeyword_6_0());
                    			
                    // InternalRaDSL2022.g:679:4: ( ( ruleEString ) )
                    // InternalRaDSL2022.g:680:5: ( ruleEString )
                    {
                    // InternalRaDSL2022.g:680:5: ( ruleEString )
                    // InternalRaDSL2022.g:681:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResourceAllocationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getResourceAllocationAccess().getRoleRoleCrossReference_6_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getResourceAllocationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceAllocation"


    // $ANTLR start "entryRuleRole"
    // InternalRaDSL2022.g:704:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalRaDSL2022.g:704:45: (iv_ruleRole= ruleRole EOF )
            // InternalRaDSL2022.g:705:2: iv_ruleRole= ruleRole EOF
            {
             newCompositeNode(grammarAccess.getRoleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;

             current =iv_ruleRole; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalRaDSL2022.g:711:1: ruleRole returns [EObject current=null] : ( () otherlv_1= 'Role' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_workload_5_0 = null;



        	enterRule();

        try {
            // InternalRaDSL2022.g:717:2: ( ( () otherlv_1= 'Role' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )? otherlv_6= '}' ) )
            // InternalRaDSL2022.g:718:2: ( () otherlv_1= 'Role' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )? otherlv_6= '}' )
            {
            // InternalRaDSL2022.g:718:2: ( () otherlv_1= 'Role' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )? otherlv_6= '}' )
            // InternalRaDSL2022.g:719:3: () otherlv_1= 'Role' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )? otherlv_6= '}'
            {
            // InternalRaDSL2022.g:719:3: ()
            // InternalRaDSL2022.g:720:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRoleAccess().getRoleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getRoleKeyword_1());
            		
            // InternalRaDSL2022.g:730:3: ( (lv_name_2_0= ruleEString ) )
            // InternalRaDSL2022.g:731:4: (lv_name_2_0= ruleEString )
            {
            // InternalRaDSL2022.g:731:4: (lv_name_2_0= ruleEString )
            // InternalRaDSL2022.g:732:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRoleAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRoleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"tdt4250.ra.RaDSL2022.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalRaDSL2022.g:753:3: (otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRaDSL2022.g:754:4: otherlv_4= 'workload' ( (lv_workload_5_0= ruleEFloat ) )
                    {
                    otherlv_4=(Token)match(input,29,FOLLOW_17); 

                    				newLeafNode(otherlv_4, grammarAccess.getRoleAccess().getWorkloadKeyword_4_0());
                    			
                    // InternalRaDSL2022.g:758:4: ( (lv_workload_5_0= ruleEFloat ) )
                    // InternalRaDSL2022.g:759:5: (lv_workload_5_0= ruleEFloat )
                    {
                    // InternalRaDSL2022.g:759:5: (lv_workload_5_0= ruleEFloat )
                    // InternalRaDSL2022.g:760:6: lv_workload_5_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getRoleAccess().getWorkloadEFloatParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_workload_5_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRoleRule());
                    						}
                    						set(
                    							current,
                    							"workload",
                    							lv_workload_5_0,
                    							"tdt4250.ra.RaDSL2022.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getRoleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleEFloat"
    // InternalRaDSL2022.g:786:1: entryRuleEFloat returns [String current=null] : iv_ruleEFloat= ruleEFloat EOF ;
    public final String entryRuleEFloat() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEFloat = null;


        try {
            // InternalRaDSL2022.g:786:46: (iv_ruleEFloat= ruleEFloat EOF )
            // InternalRaDSL2022.g:787:2: iv_ruleEFloat= ruleEFloat EOF
            {
             newCompositeNode(grammarAccess.getEFloatRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEFloat=ruleEFloat();

            state._fsp--;

             current =iv_ruleEFloat.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEFloat"


    // $ANTLR start "ruleEFloat"
    // InternalRaDSL2022.g:793:1: ruleEFloat returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEFloat() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;
        Token this_INT_7=null;


        	enterRule();

        try {
            // InternalRaDSL2022.g:799:2: ( ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) )
            // InternalRaDSL2022.g:800:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            {
            // InternalRaDSL2022.g:800:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            // InternalRaDSL2022.g:801:3: (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            {
            // InternalRaDSL2022.g:801:3: (kw= '-' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRaDSL2022.g:802:4: kw= '-'
                    {
                    kw=(Token)match(input,30,FOLLOW_22); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEFloatAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            // InternalRaDSL2022.g:808:3: (this_INT_1= RULE_INT )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_INT) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRaDSL2022.g:809:4: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_23); 

                    				current.merge(this_INT_1);
                    			

                    				newLeafNode(this_INT_1, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_1());
                    			

                    }
                    break;

            }

            kw=(Token)match(input,31,FOLLOW_24); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getEFloatAccess().getFullStopKeyword_2());
            		
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_25); 

            			current.merge(this_INT_3);
            		

            			newLeafNode(this_INT_3, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_3());
            		
            // InternalRaDSL2022.g:829:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=32 && LA20_0<=33)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalRaDSL2022.g:830:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT
                    {
                    // InternalRaDSL2022.g:830:4: (kw= 'E' | kw= 'e' )
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==32) ) {
                        alt18=1;
                    }
                    else if ( (LA18_0==33) ) {
                        alt18=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 18, 0, input);

                        throw nvae;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalRaDSL2022.g:831:5: kw= 'E'
                            {
                            kw=(Token)match(input,32,FOLLOW_26); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getEKeyword_4_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalRaDSL2022.g:837:5: kw= 'e'
                            {
                            kw=(Token)match(input,33,FOLLOW_26); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getEKeyword_4_0_1());
                            				

                            }
                            break;

                    }

                    // InternalRaDSL2022.g:843:4: (kw= '-' )?
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==30) ) {
                        alt19=1;
                    }
                    switch (alt19) {
                        case 1 :
                            // InternalRaDSL2022.g:844:5: kw= '-'
                            {
                            kw=(Token)match(input,30,FOLLOW_24); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getHyphenMinusKeyword_4_1());
                            				

                            }
                            break;

                    }

                    this_INT_7=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_7);
                    			

                    				newLeafNode(this_INT_7, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_4_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEFloat"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000076000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000074000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000070000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000610000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000F010000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000C0000040L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000000E010000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x000000000C010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000008010000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020010000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000080000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000300000002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000040000040L});

}
