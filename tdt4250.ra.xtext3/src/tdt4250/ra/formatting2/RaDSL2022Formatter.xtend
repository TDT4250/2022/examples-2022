/*
 * generated by Xtext 2.27.0
 */
package tdt4250.ra.formatting2

import com.google.inject.Inject
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import tdt4250.ra.Course
import tdt4250.ra.Department
import tdt4250.ra.services.RaDSL2022GrammarAccess

class RaDSL2022Formatter extends AbstractFormatter2 {
	
	@Inject extension RaDSL2022GrammarAccess

	def dispatch void format(Department department, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (person : department.staff) {
			person.format
		}
		for (course : department.courses) {
			course.format
		}
		for (resourceAllocation : department.resourceAllocations) {
			resourceAllocation.format
		}
	}

	def dispatch void format(Course course, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (role : course.roles) {
			role.format
		}
	}
	
}
