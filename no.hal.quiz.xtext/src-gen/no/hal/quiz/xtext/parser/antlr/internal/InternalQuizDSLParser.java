package no.hal.quiz.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.hal.quiz.xtext.services.QuizDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQuizDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_XML_TEXT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'quiz'", "'.'", "'part'", "'ref'", "'~'", "'/'", "'+-'", "'yes'", "'true'", "'no'", "'false'", "'('", "'x'", "')'", "'-'", "'v'", "'['", "']'", "'<<'", "'>>'", "'?'", "'='"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_XML_TEXT=7;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalQuizDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQuizDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQuizDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQuizDSL.g"; }



     	private QuizDSLGrammarAccess grammarAccess;

        public InternalQuizDSLParser(TokenStream input, QuizDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Quiz";
       	}

       	@Override
       	protected QuizDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleQuiz"
    // InternalQuizDSL.g:64:1: entryRuleQuiz returns [EObject current=null] : iv_ruleQuiz= ruleQuiz EOF ;
    public final EObject entryRuleQuiz() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuiz = null;


        try {
            // InternalQuizDSL.g:64:45: (iv_ruleQuiz= ruleQuiz EOF )
            // InternalQuizDSL.g:65:2: iv_ruleQuiz= ruleQuiz EOF
            {
             newCompositeNode(grammarAccess.getQuizRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuiz=ruleQuiz();

            state._fsp--;

             current =iv_ruleQuiz; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuiz"


    // $ANTLR start "ruleQuiz"
    // InternalQuizDSL.g:71:1: ruleQuiz returns [EObject current=null] : ( () otherlv_1= 'quiz' ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) ) ) ;
    public final EObject ruleQuiz() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_title_3_0=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_parts_4_0 = null;

        EObject lv_parts_5_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:77:2: ( ( () otherlv_1= 'quiz' ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) ) ) )
            // InternalQuizDSL.g:78:2: ( () otherlv_1= 'quiz' ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) ) )
            {
            // InternalQuizDSL.g:78:2: ( () otherlv_1= 'quiz' ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) ) )
            // InternalQuizDSL.g:79:3: () otherlv_1= 'quiz' ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) )
            {
            // InternalQuizDSL.g:79:3: ()
            // InternalQuizDSL.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getQuizAccess().getQuizAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getQuizAccess().getQuizKeyword_1());
            		
            // InternalQuizDSL.g:90:3: ( ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* ) | ( (lv_parts_5_0= ruleAnonymousQuizPart ) ) )
            int alt4=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case RULE_STRING:
                    {
                    switch ( input.LA(3) ) {
                    case 14:
                        {
                        alt4=1;
                        }
                        break;
                    case EOF:
                        {
                        alt4=1;
                        }
                        break;
                    case RULE_STRING:
                    case RULE_INT:
                    case 17:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 26:
                    case 27:
                    case 28:
                    case 30:
                        {
                        alt4=2;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 2, input);

                        throw nvae;
                    }

                    }
                    break;
                case 30:
                    {
                    alt4=2;
                    }
                    break;
                case EOF:
                case 13:
                    {
                    alt4=1;
                    }
                    break;
                case 14:
                    {
                    alt4=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STRING:
                {
                switch ( input.LA(2) ) {
                case 14:
                    {
                    alt4=1;
                    }
                    break;
                case EOF:
                    {
                    alt4=1;
                    }
                    break;
                case RULE_STRING:
                case RULE_INT:
                case 17:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 26:
                case 27:
                case 28:
                case 30:
                    {
                    alt4=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;
                }

                }
                break;
            case 14:
                {
                alt4=1;
                }
                break;
            case EOF:
                {
                alt4=1;
                }
                break;
            case 15:
            case 30:
                {
                alt4=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalQuizDSL.g:91:4: ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* )
                    {
                    // InternalQuizDSL.g:91:4: ( ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )* )
                    // InternalQuizDSL.g:92:5: ( (lv_name_2_0= ruleQName ) )? ( (lv_title_3_0= RULE_STRING ) )? ( (lv_parts_4_0= ruleAbstractQuizPart ) )*
                    {
                    // InternalQuizDSL.g:92:5: ( (lv_name_2_0= ruleQName ) )?
                    int alt1=2;
                    int LA1_0 = input.LA(1);

                    if ( (LA1_0==RULE_ID) ) {
                        alt1=1;
                    }
                    switch (alt1) {
                        case 1 :
                            // InternalQuizDSL.g:93:6: (lv_name_2_0= ruleQName )
                            {
                            // InternalQuizDSL.g:93:6: (lv_name_2_0= ruleQName )
                            // InternalQuizDSL.g:94:7: lv_name_2_0= ruleQName
                            {

                            							newCompositeNode(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_2_0_0_0());
                            						
                            pushFollow(FOLLOW_4);
                            lv_name_2_0=ruleQName();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getQuizRule());
                            							}
                            							set(
                            								current,
                            								"name",
                            								lv_name_2_0,
                            								"no.hal.quiz.xtext.QuizDSL.QName");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    // InternalQuizDSL.g:111:5: ( (lv_title_3_0= RULE_STRING ) )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0==RULE_STRING) ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // InternalQuizDSL.g:112:6: (lv_title_3_0= RULE_STRING )
                            {
                            // InternalQuizDSL.g:112:6: (lv_title_3_0= RULE_STRING )
                            // InternalQuizDSL.g:113:7: lv_title_3_0= RULE_STRING
                            {
                            lv_title_3_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

                            							newLeafNode(lv_title_3_0, grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getQuizRule());
                            							}
                            							setWithLastConsumed(
                            								current,
                            								"title",
                            								lv_title_3_0,
                            								"org.eclipse.xtext.common.Terminals.STRING");
                            						

                            }


                            }
                            break;

                    }

                    // InternalQuizDSL.g:129:5: ( (lv_parts_4_0= ruleAbstractQuizPart ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==14) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalQuizDSL.g:130:6: (lv_parts_4_0= ruleAbstractQuizPart )
                    	    {
                    	    // InternalQuizDSL.g:130:6: (lv_parts_4_0= ruleAbstractQuizPart )
                    	    // InternalQuizDSL.g:131:7: lv_parts_4_0= ruleAbstractQuizPart
                    	    {

                    	    							newCompositeNode(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_2_0_2_0());
                    	    						
                    	    pushFollow(FOLLOW_5);
                    	    lv_parts_4_0=ruleAbstractQuizPart();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getQuizRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parts",
                    	    								lv_parts_4_0,
                    	    								"no.hal.quiz.xtext.QuizDSL.AbstractQuizPart");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:150:4: ( (lv_parts_5_0= ruleAnonymousQuizPart ) )
                    {
                    // InternalQuizDSL.g:150:4: ( (lv_parts_5_0= ruleAnonymousQuizPart ) )
                    // InternalQuizDSL.g:151:5: (lv_parts_5_0= ruleAnonymousQuizPart )
                    {
                    // InternalQuizDSL.g:151:5: (lv_parts_5_0= ruleAnonymousQuizPart )
                    // InternalQuizDSL.g:152:6: lv_parts_5_0= ruleAnonymousQuizPart
                    {

                    						newCompositeNode(grammarAccess.getQuizAccess().getPartsAnonymousQuizPartParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_parts_5_0=ruleAnonymousQuizPart();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getQuizRule());
                    						}
                    						add(
                    							current,
                    							"parts",
                    							lv_parts_5_0,
                    							"no.hal.quiz.xtext.QuizDSL.AnonymousQuizPart");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuiz"


    // $ANTLR start "entryRuleQName"
    // InternalQuizDSL.g:174:1: entryRuleQName returns [String current=null] : iv_ruleQName= ruleQName EOF ;
    public final String entryRuleQName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQName = null;


        try {
            // InternalQuizDSL.g:174:45: (iv_ruleQName= ruleQName EOF )
            // InternalQuizDSL.g:175:2: iv_ruleQName= ruleQName EOF
            {
             newCompositeNode(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQName=ruleQName();

            state._fsp--;

             current =iv_ruleQName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalQuizDSL.g:181:1: ruleQName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:187:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalQuizDSL.g:188:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalQuizDSL.g:188:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalQuizDSL.g:189:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalQuizDSL.g:196:3: (kw= '.' this_ID_2= RULE_ID )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==13) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalQuizDSL.g:197:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,13,FOLLOW_7); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_6); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleAbstractQuizPart"
    // InternalQuizDSL.g:214:1: entryRuleAbstractQuizPart returns [EObject current=null] : iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF ;
    public final EObject entryRuleAbstractQuizPart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractQuizPart = null;


        try {
            // InternalQuizDSL.g:214:57: (iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF )
            // InternalQuizDSL.g:215:2: iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF
            {
             newCompositeNode(grammarAccess.getAbstractQuizPartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractQuizPart=ruleAbstractQuizPart();

            state._fsp--;

             current =iv_ruleAbstractQuizPart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractQuizPart"


    // $ANTLR start "ruleAbstractQuizPart"
    // InternalQuizDSL.g:221:1: ruleAbstractQuizPart returns [EObject current=null] : (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef ) ;
    public final EObject ruleAbstractQuizPart() throws RecognitionException {
        EObject current = null;

        EObject this_QuizPart_0 = null;

        EObject this_QuizPartRef_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:227:2: ( (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef ) )
            // InternalQuizDSL.g:228:2: (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef )
            {
            // InternalQuizDSL.g:228:2: (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==RULE_ID) ) {
                    alt6=1;
                }
                else if ( (LA6_1==15) ) {
                    alt6=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalQuizDSL.g:229:3: this_QuizPart_0= ruleQuizPart
                    {

                    			newCompositeNode(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuizPart_0=ruleQuizPart();

                    state._fsp--;


                    			current = this_QuizPart_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:238:3: this_QuizPartRef_1= ruleQuizPartRef
                    {

                    			newCompositeNode(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuizPartRef_1=ruleQuizPartRef();

                    state._fsp--;


                    			current = this_QuizPartRef_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractQuizPart"


    // $ANTLR start "entryRuleQuizPart"
    // InternalQuizDSL.g:250:1: entryRuleQuizPart returns [EObject current=null] : iv_ruleQuizPart= ruleQuizPart EOF ;
    public final EObject entryRuleQuizPart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuizPart = null;


        try {
            // InternalQuizDSL.g:250:49: (iv_ruleQuizPart= ruleQuizPart EOF )
            // InternalQuizDSL.g:251:2: iv_ruleQuizPart= ruleQuizPart EOF
            {
             newCompositeNode(grammarAccess.getQuizPartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuizPart=ruleQuizPart();

            state._fsp--;

             current =iv_ruleQuizPart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuizPart"


    // $ANTLR start "ruleQuizPart"
    // InternalQuizDSL.g:257:1: ruleQuizPart returns [EObject current=null] : (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* ) ;
    public final EObject ruleQuizPart() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_title_2_0=null;
        EObject lv_questions_3_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:263:2: ( (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* ) )
            // InternalQuizDSL.g:264:2: (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* )
            {
            // InternalQuizDSL.g:264:2: (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* )
            // InternalQuizDSL.g:265:3: otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )*
            {
            otherlv_0=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getQuizPartAccess().getPartKeyword_0());
            		
            // InternalQuizDSL.g:269:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalQuizDSL.g:270:4: (lv_name_1_0= RULE_ID )
            {
            // InternalQuizDSL.g:270:4: (lv_name_1_0= RULE_ID )
            // InternalQuizDSL.g:271:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_1_0, grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalQuizDSL.g:287:3: ( (lv_title_2_0= RULE_STRING ) )
            // InternalQuizDSL.g:288:4: (lv_title_2_0= RULE_STRING )
            {
            // InternalQuizDSL.g:288:4: (lv_title_2_0= RULE_STRING )
            // InternalQuizDSL.g:289:5: lv_title_2_0= RULE_STRING
            {
            lv_title_2_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_title_2_0, grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRule());
            					}
            					setWithLastConsumed(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalQuizDSL.g:305:3: ( (lv_questions_3_0= ruleAbstractQA ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=RULE_STRING && LA7_0<=RULE_ID)||LA7_0==15||LA7_0==30) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalQuizDSL.g:306:4: (lv_questions_3_0= ruleAbstractQA )
            	    {
            	    // InternalQuizDSL.g:306:4: (lv_questions_3_0= ruleAbstractQA )
            	    // InternalQuizDSL.g:307:5: lv_questions_3_0= ruleAbstractQA
            	    {

            	    					newCompositeNode(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_questions_3_0=ruleAbstractQA();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getQuizPartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"questions",
            	    						lv_questions_3_0,
            	    						"no.hal.quiz.xtext.QuizDSL.AbstractQA");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuizPart"


    // $ANTLR start "entryRuleQuizPartRef"
    // InternalQuizDSL.g:328:1: entryRuleQuizPartRef returns [EObject current=null] : iv_ruleQuizPartRef= ruleQuizPartRef EOF ;
    public final EObject entryRuleQuizPartRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuizPartRef = null;


        try {
            // InternalQuizDSL.g:328:52: (iv_ruleQuizPartRef= ruleQuizPartRef EOF )
            // InternalQuizDSL.g:329:2: iv_ruleQuizPartRef= ruleQuizPartRef EOF
            {
             newCompositeNode(grammarAccess.getQuizPartRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuizPartRef=ruleQuizPartRef();

            state._fsp--;

             current =iv_ruleQuizPartRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuizPartRef"


    // $ANTLR start "ruleQuizPartRef"
    // InternalQuizDSL.g:335:1: ruleQuizPartRef returns [EObject current=null] : (otherlv_0= 'part' otherlv_1= 'ref' ( ( ruleQName ) ) ) ;
    public final EObject ruleQuizPartRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:341:2: ( (otherlv_0= 'part' otherlv_1= 'ref' ( ( ruleQName ) ) ) )
            // InternalQuizDSL.g:342:2: (otherlv_0= 'part' otherlv_1= 'ref' ( ( ruleQName ) ) )
            {
            // InternalQuizDSL.g:342:2: (otherlv_0= 'part' otherlv_1= 'ref' ( ( ruleQName ) ) )
            // InternalQuizDSL.g:343:3: otherlv_0= 'part' otherlv_1= 'ref' ( ( ruleQName ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getQuizPartRefAccess().getPartKeyword_0());
            		
            otherlv_1=(Token)match(input,15,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getQuizPartRefAccess().getRefKeyword_1());
            		
            // InternalQuizDSL.g:351:3: ( ( ruleQName ) )
            // InternalQuizDSL.g:352:4: ( ruleQName )
            {
            // InternalQuizDSL.g:352:4: ( ruleQName )
            // InternalQuizDSL.g:353:5: ruleQName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRefRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuizPartRef"


    // $ANTLR start "entryRuleAnonymousQuizPart"
    // InternalQuizDSL.g:371:1: entryRuleAnonymousQuizPart returns [EObject current=null] : iv_ruleAnonymousQuizPart= ruleAnonymousQuizPart EOF ;
    public final EObject entryRuleAnonymousQuizPart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnonymousQuizPart = null;


        try {
            // InternalQuizDSL.g:371:58: (iv_ruleAnonymousQuizPart= ruleAnonymousQuizPart EOF )
            // InternalQuizDSL.g:372:2: iv_ruleAnonymousQuizPart= ruleAnonymousQuizPart EOF
            {
             newCompositeNode(grammarAccess.getAnonymousQuizPartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnonymousQuizPart=ruleAnonymousQuizPart();

            state._fsp--;

             current =iv_ruleAnonymousQuizPart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnonymousQuizPart"


    // $ANTLR start "ruleAnonymousQuizPart"
    // InternalQuizDSL.g:378:1: ruleAnonymousQuizPart returns [EObject current=null] : ( () ( (lv_questions_1_0= ruleAbstractQA ) )* ) ;
    public final EObject ruleAnonymousQuizPart() throws RecognitionException {
        EObject current = null;

        EObject lv_questions_1_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:384:2: ( ( () ( (lv_questions_1_0= ruleAbstractQA ) )* ) )
            // InternalQuizDSL.g:385:2: ( () ( (lv_questions_1_0= ruleAbstractQA ) )* )
            {
            // InternalQuizDSL.g:385:2: ( () ( (lv_questions_1_0= ruleAbstractQA ) )* )
            // InternalQuizDSL.g:386:3: () ( (lv_questions_1_0= ruleAbstractQA ) )*
            {
            // InternalQuizDSL.g:386:3: ()
            // InternalQuizDSL.g:387:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAnonymousQuizPartAccess().getQuizPartAction_0(),
            					current);
            			

            }

            // InternalQuizDSL.g:393:3: ( (lv_questions_1_0= ruleAbstractQA ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_STRING && LA8_0<=RULE_ID)||LA8_0==15||LA8_0==30) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalQuizDSL.g:394:4: (lv_questions_1_0= ruleAbstractQA )
            	    {
            	    // InternalQuizDSL.g:394:4: (lv_questions_1_0= ruleAbstractQA )
            	    // InternalQuizDSL.g:395:5: lv_questions_1_0= ruleAbstractQA
            	    {

            	    					newCompositeNode(grammarAccess.getAnonymousQuizPartAccess().getQuestionsAbstractQAParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_questions_1_0=ruleAbstractQA();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAnonymousQuizPartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"questions",
            	    						lv_questions_1_0,
            	    						"no.hal.quiz.xtext.QuizDSL.AbstractQA");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnonymousQuizPart"


    // $ANTLR start "entryRuleAbstractQA"
    // InternalQuizDSL.g:416:1: entryRuleAbstractQA returns [EObject current=null] : iv_ruleAbstractQA= ruleAbstractQA EOF ;
    public final EObject entryRuleAbstractQA() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractQA = null;


        try {
            // InternalQuizDSL.g:416:51: (iv_ruleAbstractQA= ruleAbstractQA EOF )
            // InternalQuizDSL.g:417:2: iv_ruleAbstractQA= ruleAbstractQA EOF
            {
             newCompositeNode(grammarAccess.getAbstractQARule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractQA=ruleAbstractQA();

            state._fsp--;

             current =iv_ruleAbstractQA; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractQA"


    // $ANTLR start "ruleAbstractQA"
    // InternalQuizDSL.g:423:1: ruleAbstractQA returns [EObject current=null] : (this_QA_0= ruleQA | this_QARef_1= ruleQARef ) ;
    public final EObject ruleAbstractQA() throws RecognitionException {
        EObject current = null;

        EObject this_QA_0 = null;

        EObject this_QARef_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:429:2: ( (this_QA_0= ruleQA | this_QARef_1= ruleQARef ) )
            // InternalQuizDSL.g:430:2: (this_QA_0= ruleQA | this_QARef_1= ruleQARef )
            {
            // InternalQuizDSL.g:430:2: (this_QA_0= ruleQA | this_QARef_1= ruleQARef )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=RULE_STRING && LA9_0<=RULE_ID)||LA9_0==30) ) {
                alt9=1;
            }
            else if ( (LA9_0==15) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalQuizDSL.g:431:3: this_QA_0= ruleQA
                    {

                    			newCompositeNode(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_QA_0=ruleQA();

                    state._fsp--;


                    			current = this_QA_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:440:3: this_QARef_1= ruleQARef
                    {

                    			newCompositeNode(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_QARef_1=ruleQARef();

                    state._fsp--;


                    			current = this_QARef_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractQA"


    // $ANTLR start "entryRuleQARef"
    // InternalQuizDSL.g:452:1: entryRuleQARef returns [EObject current=null] : iv_ruleQARef= ruleQARef EOF ;
    public final EObject entryRuleQARef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQARef = null;


        try {
            // InternalQuizDSL.g:452:46: (iv_ruleQARef= ruleQARef EOF )
            // InternalQuizDSL.g:453:2: iv_ruleQARef= ruleQARef EOF
            {
             newCompositeNode(grammarAccess.getQARefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQARef=ruleQARef();

            state._fsp--;

             current =iv_ruleQARef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQARef"


    // $ANTLR start "ruleQARef"
    // InternalQuizDSL.g:459:1: ruleQARef returns [EObject current=null] : (otherlv_0= 'ref' ( ( ruleQName ) ) ) ;
    public final EObject ruleQARef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:465:2: ( (otherlv_0= 'ref' ( ( ruleQName ) ) ) )
            // InternalQuizDSL.g:466:2: (otherlv_0= 'ref' ( ( ruleQName ) ) )
            {
            // InternalQuizDSL.g:466:2: (otherlv_0= 'ref' ( ( ruleQName ) ) )
            // InternalQuizDSL.g:467:3: otherlv_0= 'ref' ( ( ruleQName ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getQARefAccess().getRefKeyword_0());
            		
            // InternalQuizDSL.g:471:3: ( ( ruleQName ) )
            // InternalQuizDSL.g:472:4: ( ruleQName )
            {
            // InternalQuizDSL.g:472:4: ( ruleQName )
            // InternalQuizDSL.g:473:5: ruleQName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQARefRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQARef"


    // $ANTLR start "entryRuleQA"
    // InternalQuizDSL.g:491:1: entryRuleQA returns [EObject current=null] : iv_ruleQA= ruleQA EOF ;
    public final EObject entryRuleQA() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQA = null;


        try {
            // InternalQuizDSL.g:491:43: (iv_ruleQA= ruleQA EOF )
            // InternalQuizDSL.g:492:2: iv_ruleQA= ruleQA EOF
            {
             newCompositeNode(grammarAccess.getQARule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQA=ruleQA();

            state._fsp--;

             current =iv_ruleQA; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQA"


    // $ANTLR start "ruleQA"
    // InternalQuizDSL.g:498:1: ruleQA returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) ( (lv_a_2_0= ruleAnswer ) ) ) ;
    public final EObject ruleQA() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_q_1_0 = null;

        EObject lv_a_2_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:504:2: ( ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) ( (lv_a_2_0= ruleAnswer ) ) ) )
            // InternalQuizDSL.g:505:2: ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) ( (lv_a_2_0= ruleAnswer ) ) )
            {
            // InternalQuizDSL.g:505:2: ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) ( (lv_a_2_0= ruleAnswer ) ) )
            // InternalQuizDSL.g:506:3: ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) ( (lv_a_2_0= ruleAnswer ) )
            {
            // InternalQuizDSL.g:506:3: ( (lv_name_0_0= RULE_ID ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalQuizDSL.g:507:4: (lv_name_0_0= RULE_ID )
                    {
                    // InternalQuizDSL.g:507:4: (lv_name_0_0= RULE_ID )
                    // InternalQuizDSL.g:508:5: lv_name_0_0= RULE_ID
                    {
                    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_11); 

                    					newLeafNode(lv_name_0_0, grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getQARule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_0_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalQuizDSL.g:524:3: ( (lv_q_1_0= ruleQuestion ) )
            // InternalQuizDSL.g:525:4: (lv_q_1_0= ruleQuestion )
            {
            // InternalQuizDSL.g:525:4: (lv_q_1_0= ruleQuestion )
            // InternalQuizDSL.g:526:5: lv_q_1_0= ruleQuestion
            {

            					newCompositeNode(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_12);
            lv_q_1_0=ruleQuestion();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQARule());
            					}
            					set(
            						current,
            						"q",
            						lv_q_1_0,
            						"no.hal.quiz.xtext.QuizDSL.Question");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQuizDSL.g:543:3: ( (lv_a_2_0= ruleAnswer ) )
            // InternalQuizDSL.g:544:4: (lv_a_2_0= ruleAnswer )
            {
            // InternalQuizDSL.g:544:4: (lv_a_2_0= ruleAnswer )
            // InternalQuizDSL.g:545:5: lv_a_2_0= ruleAnswer
            {

            					newCompositeNode(grammarAccess.getQAAccess().getAAnswerParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_a_2_0=ruleAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQARule());
            					}
            					set(
            						current,
            						"a",
            						lv_a_2_0,
            						"no.hal.quiz.xtext.QuizDSL.Answer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQA"


    // $ANTLR start "entryRuleQuestion"
    // InternalQuizDSL.g:566:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // InternalQuizDSL.g:566:49: (iv_ruleQuestion= ruleQuestion EOF )
            // InternalQuizDSL.g:567:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // InternalQuizDSL.g:573:1: ruleQuestion returns [EObject current=null] : (this_StringQuestion_0= ruleStringQuestion | this_XmlQuestion_1= ruleXmlQuestion ) ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        EObject this_StringQuestion_0 = null;

        EObject this_XmlQuestion_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:579:2: ( (this_StringQuestion_0= ruleStringQuestion | this_XmlQuestion_1= ruleXmlQuestion ) )
            // InternalQuizDSL.g:580:2: (this_StringQuestion_0= ruleStringQuestion | this_XmlQuestion_1= ruleXmlQuestion )
            {
            // InternalQuizDSL.g:580:2: (this_StringQuestion_0= ruleStringQuestion | this_XmlQuestion_1= ruleXmlQuestion )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_STRING) ) {
                alt11=1;
            }
            else if ( (LA11_0==30) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalQuizDSL.g:581:3: this_StringQuestion_0= ruleStringQuestion
                    {

                    			newCompositeNode(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringQuestion_0=ruleStringQuestion();

                    state._fsp--;


                    			current = this_StringQuestion_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:590:3: this_XmlQuestion_1= ruleXmlQuestion
                    {

                    			newCompositeNode(grammarAccess.getQuestionAccess().getXmlQuestionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_XmlQuestion_1=ruleXmlQuestion();

                    state._fsp--;


                    			current = this_XmlQuestion_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleStringQuestion"
    // InternalQuizDSL.g:602:1: entryRuleStringQuestion returns [EObject current=null] : iv_ruleStringQuestion= ruleStringQuestion EOF ;
    public final EObject entryRuleStringQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringQuestion = null;


        try {
            // InternalQuizDSL.g:602:55: (iv_ruleStringQuestion= ruleStringQuestion EOF )
            // InternalQuizDSL.g:603:2: iv_ruleStringQuestion= ruleStringQuestion EOF
            {
             newCompositeNode(grammarAccess.getStringQuestionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringQuestion=ruleStringQuestion();

            state._fsp--;

             current =iv_ruleStringQuestion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringQuestion"


    // $ANTLR start "ruleStringQuestion"
    // InternalQuizDSL.g:609:1: ruleStringQuestion returns [EObject current=null] : ( (lv_question_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringQuestion() throws RecognitionException {
        EObject current = null;

        Token lv_question_0_0=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:615:2: ( ( (lv_question_0_0= RULE_STRING ) ) )
            // InternalQuizDSL.g:616:2: ( (lv_question_0_0= RULE_STRING ) )
            {
            // InternalQuizDSL.g:616:2: ( (lv_question_0_0= RULE_STRING ) )
            // InternalQuizDSL.g:617:3: (lv_question_0_0= RULE_STRING )
            {
            // InternalQuizDSL.g:617:3: (lv_question_0_0= RULE_STRING )
            // InternalQuizDSL.g:618:4: lv_question_0_0= RULE_STRING
            {
            lv_question_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_question_0_0, grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getStringQuestionRule());
            				}
            				setWithLastConsumed(
            					current,
            					"question",
            					lv_question_0_0,
            					"org.eclipse.xtext.common.Terminals.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringQuestion"


    // $ANTLR start "entryRuleXmlQuestion"
    // InternalQuizDSL.g:637:1: entryRuleXmlQuestion returns [EObject current=null] : iv_ruleXmlQuestion= ruleXmlQuestion EOF ;
    public final EObject entryRuleXmlQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlQuestion = null;


        try {
            // InternalQuizDSL.g:637:52: (iv_ruleXmlQuestion= ruleXmlQuestion EOF )
            // InternalQuizDSL.g:638:2: iv_ruleXmlQuestion= ruleXmlQuestion EOF
            {
             newCompositeNode(grammarAccess.getXmlQuestionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlQuestion=ruleXmlQuestion();

            state._fsp--;

             current =iv_ruleXmlQuestion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlQuestion"


    // $ANTLR start "ruleXmlQuestion"
    // InternalQuizDSL.g:644:1: ruleXmlQuestion returns [EObject current=null] : ( (lv_xml_0_0= ruleXml ) ) ;
    public final EObject ruleXmlQuestion() throws RecognitionException {
        EObject current = null;

        EObject lv_xml_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:650:2: ( ( (lv_xml_0_0= ruleXml ) ) )
            // InternalQuizDSL.g:651:2: ( (lv_xml_0_0= ruleXml ) )
            {
            // InternalQuizDSL.g:651:2: ( (lv_xml_0_0= ruleXml ) )
            // InternalQuizDSL.g:652:3: (lv_xml_0_0= ruleXml )
            {
            // InternalQuizDSL.g:652:3: (lv_xml_0_0= ruleXml )
            // InternalQuizDSL.g:653:4: lv_xml_0_0= ruleXml
            {

            				newCompositeNode(grammarAccess.getXmlQuestionAccess().getXmlXmlParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_xml_0_0=ruleXml();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getXmlQuestionRule());
            				}
            				set(
            					current,
            					"xml",
            					lv_xml_0_0,
            					"no.hal.quiz.xtext.QuizDSL.Xml");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlQuestion"


    // $ANTLR start "entryRuleAnswer"
    // InternalQuizDSL.g:673:1: entryRuleAnswer returns [EObject current=null] : iv_ruleAnswer= ruleAnswer EOF ;
    public final EObject entryRuleAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnswer = null;


        try {
            // InternalQuizDSL.g:673:47: (iv_ruleAnswer= ruleAnswer EOF )
            // InternalQuizDSL.g:674:2: iv_ruleAnswer= ruleAnswer EOF
            {
             newCompositeNode(grammarAccess.getAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnswer=ruleAnswer();

            state._fsp--;

             current =iv_ruleAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnswer"


    // $ANTLR start "ruleAnswer"
    // InternalQuizDSL.g:680:1: ruleAnswer returns [EObject current=null] : (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer ) ;
    public final EObject ruleAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_OptionAnswer_0 = null;

        EObject this_OptionsAnswer_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:686:2: ( (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer ) )
            // InternalQuizDSL.g:687:2: (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer )
            {
            // InternalQuizDSL.g:687:2: (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_STRING||LA12_0==RULE_INT||LA12_0==17||(LA12_0>=19 && LA12_0<=22)||LA12_0==30) ) {
                alt12=1;
            }
            else if ( ((LA12_0>=23 && LA12_0<=24)||(LA12_0>=26 && LA12_0<=28)) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalQuizDSL.g:688:3: this_OptionAnswer_0= ruleOptionAnswer
                    {

                    			newCompositeNode(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OptionAnswer_0=ruleOptionAnswer();

                    state._fsp--;


                    			current = this_OptionAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:697:3: this_OptionsAnswer_1= ruleOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_OptionsAnswer_1=ruleOptionsAnswer();

                    state._fsp--;


                    			current = this_OptionsAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnswer"


    // $ANTLR start "entryRuleOptionAnswer"
    // InternalQuizDSL.g:709:1: entryRuleOptionAnswer returns [EObject current=null] : iv_ruleOptionAnswer= ruleOptionAnswer EOF ;
    public final EObject entryRuleOptionAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionAnswer = null;


        try {
            // InternalQuizDSL.g:709:53: (iv_ruleOptionAnswer= ruleOptionAnswer EOF )
            // InternalQuizDSL.g:710:2: iv_ruleOptionAnswer= ruleOptionAnswer EOF
            {
             newCompositeNode(grammarAccess.getOptionAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOptionAnswer=ruleOptionAnswer();

            state._fsp--;

             current =iv_ruleOptionAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionAnswer"


    // $ANTLR start "ruleOptionAnswer"
    // InternalQuizDSL.g:716:1: ruleOptionAnswer returns [EObject current=null] : (this_SimpleAnswer_0= ruleSimpleAnswer | this_XmlAnswer_1= ruleXmlAnswer ) ;
    public final EObject ruleOptionAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleAnswer_0 = null;

        EObject this_XmlAnswer_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:722:2: ( (this_SimpleAnswer_0= ruleSimpleAnswer | this_XmlAnswer_1= ruleXmlAnswer ) )
            // InternalQuizDSL.g:723:2: (this_SimpleAnswer_0= ruleSimpleAnswer | this_XmlAnswer_1= ruleXmlAnswer )
            {
            // InternalQuizDSL.g:723:2: (this_SimpleAnswer_0= ruleSimpleAnswer | this_XmlAnswer_1= ruleXmlAnswer )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_STRING||LA13_0==RULE_INT||LA13_0==17||(LA13_0>=19 && LA13_0<=22)) ) {
                alt13=1;
            }
            else if ( (LA13_0==30) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalQuizDSL.g:724:3: this_SimpleAnswer_0= ruleSimpleAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleAnswer_0=ruleSimpleAnswer();

                    state._fsp--;


                    			current = this_SimpleAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:733:3: this_XmlAnswer_1= ruleXmlAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionAnswerAccess().getXmlAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_XmlAnswer_1=ruleXmlAnswer();

                    state._fsp--;


                    			current = this_XmlAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionAnswer"


    // $ANTLR start "entryRuleSimpleAnswer"
    // InternalQuizDSL.g:745:1: entryRuleSimpleAnswer returns [EObject current=null] : iv_ruleSimpleAnswer= ruleSimpleAnswer EOF ;
    public final EObject entryRuleSimpleAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleAnswer = null;


        try {
            // InternalQuizDSL.g:745:53: (iv_ruleSimpleAnswer= ruleSimpleAnswer EOF )
            // InternalQuizDSL.g:746:2: iv_ruleSimpleAnswer= ruleSimpleAnswer EOF
            {
             newCompositeNode(grammarAccess.getSimpleAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleAnswer=ruleSimpleAnswer();

            state._fsp--;

             current =iv_ruleSimpleAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleAnswer"


    // $ANTLR start "ruleSimpleAnswer"
    // InternalQuizDSL.g:752:1: ruleSimpleAnswer returns [EObject current=null] : (this_StringAnswer_0= ruleStringAnswer | this_RegexAnswer_1= ruleRegexAnswer | this_NumberAnswer_2= ruleNumberAnswer | this_BooleanAnswer_3= ruleBooleanAnswer ) ;
    public final EObject ruleSimpleAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_StringAnswer_0 = null;

        EObject this_RegexAnswer_1 = null;

        EObject this_NumberAnswer_2 = null;

        EObject this_BooleanAnswer_3 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:758:2: ( (this_StringAnswer_0= ruleStringAnswer | this_RegexAnswer_1= ruleRegexAnswer | this_NumberAnswer_2= ruleNumberAnswer | this_BooleanAnswer_3= ruleBooleanAnswer ) )
            // InternalQuizDSL.g:759:2: (this_StringAnswer_0= ruleStringAnswer | this_RegexAnswer_1= ruleRegexAnswer | this_NumberAnswer_2= ruleNumberAnswer | this_BooleanAnswer_3= ruleBooleanAnswer )
            {
            // InternalQuizDSL.g:759:2: (this_StringAnswer_0= ruleStringAnswer | this_RegexAnswer_1= ruleRegexAnswer | this_NumberAnswer_2= ruleNumberAnswer | this_BooleanAnswer_3= ruleBooleanAnswer )
            int alt14=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt14=1;
                }
                break;
            case 17:
                {
                alt14=2;
                }
                break;
            case RULE_INT:
                {
                alt14=3;
                }
                break;
            case 19:
            case 20:
            case 21:
            case 22:
                {
                alt14=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalQuizDSL.g:760:3: this_StringAnswer_0= ruleStringAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringAnswer_0=ruleStringAnswer();

                    state._fsp--;


                    			current = this_StringAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:769:3: this_RegexAnswer_1= ruleRegexAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getRegexAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_RegexAnswer_1=ruleRegexAnswer();

                    state._fsp--;


                    			current = this_RegexAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalQuizDSL.g:778:3: this_NumberAnswer_2= ruleNumberAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_NumberAnswer_2=ruleNumberAnswer();

                    state._fsp--;


                    			current = this_NumberAnswer_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalQuizDSL.g:787:3: this_BooleanAnswer_3= ruleBooleanAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanAnswer_3=ruleBooleanAnswer();

                    state._fsp--;


                    			current = this_BooleanAnswer_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleAnswer"


    // $ANTLR start "entryRuleStringAnswer"
    // InternalQuizDSL.g:799:1: entryRuleStringAnswer returns [EObject current=null] : iv_ruleStringAnswer= ruleStringAnswer EOF ;
    public final EObject entryRuleStringAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringAnswer = null;


        try {
            // InternalQuizDSL.g:799:53: (iv_ruleStringAnswer= ruleStringAnswer EOF )
            // InternalQuizDSL.g:800:2: iv_ruleStringAnswer= ruleStringAnswer EOF
            {
             newCompositeNode(grammarAccess.getStringAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringAnswer=ruleStringAnswer();

            state._fsp--;

             current =iv_ruleStringAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringAnswer"


    // $ANTLR start "ruleStringAnswer"
    // InternalQuizDSL.g:806:1: ruleStringAnswer returns [EObject current=null] : ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_ignoreCase_1_0= '~' ) )? ) ;
    public final EObject ruleStringAnswer() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_ignoreCase_1_0=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:812:2: ( ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_ignoreCase_1_0= '~' ) )? ) )
            // InternalQuizDSL.g:813:2: ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_ignoreCase_1_0= '~' ) )? )
            {
            // InternalQuizDSL.g:813:2: ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_ignoreCase_1_0= '~' ) )? )
            // InternalQuizDSL.g:814:3: ( (lv_value_0_0= RULE_STRING ) ) ( (lv_ignoreCase_1_0= '~' ) )?
            {
            // InternalQuizDSL.g:814:3: ( (lv_value_0_0= RULE_STRING ) )
            // InternalQuizDSL.g:815:4: (lv_value_0_0= RULE_STRING )
            {
            // InternalQuizDSL.g:815:4: (lv_value_0_0= RULE_STRING )
            // InternalQuizDSL.g:816:5: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            					newLeafNode(lv_value_0_0, grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStringAnswerRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_0_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalQuizDSL.g:832:3: ( (lv_ignoreCase_1_0= '~' ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==16) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalQuizDSL.g:833:4: (lv_ignoreCase_1_0= '~' )
                    {
                    // InternalQuizDSL.g:833:4: (lv_ignoreCase_1_0= '~' )
                    // InternalQuizDSL.g:834:5: lv_ignoreCase_1_0= '~'
                    {
                    lv_ignoreCase_1_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_ignoreCase_1_0, grammarAccess.getStringAnswerAccess().getIgnoreCaseTildeKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getStringAnswerRule());
                    					}
                    					setWithLastConsumed(current, "ignoreCase", lv_ignoreCase_1_0 != null, "~");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringAnswer"


    // $ANTLR start "entryRuleRegexAnswer"
    // InternalQuizDSL.g:850:1: entryRuleRegexAnswer returns [EObject current=null] : iv_ruleRegexAnswer= ruleRegexAnswer EOF ;
    public final EObject entryRuleRegexAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRegexAnswer = null;


        try {
            // InternalQuizDSL.g:850:52: (iv_ruleRegexAnswer= ruleRegexAnswer EOF )
            // InternalQuizDSL.g:851:2: iv_ruleRegexAnswer= ruleRegexAnswer EOF
            {
             newCompositeNode(grammarAccess.getRegexAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRegexAnswer=ruleRegexAnswer();

            state._fsp--;

             current =iv_ruleRegexAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRegexAnswer"


    // $ANTLR start "ruleRegexAnswer"
    // InternalQuizDSL.g:857:1: ruleRegexAnswer returns [EObject current=null] : ( ( (lv_regexp_0_0= '/' ) ) ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= '/' ( (lv_ignoreCase_3_0= '~' ) )? ) ;
    public final EObject ruleRegexAnswer() throws RecognitionException {
        EObject current = null;

        Token lv_regexp_0_0=null;
        Token lv_value_1_0=null;
        Token otherlv_2=null;
        Token lv_ignoreCase_3_0=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:863:2: ( ( ( (lv_regexp_0_0= '/' ) ) ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= '/' ( (lv_ignoreCase_3_0= '~' ) )? ) )
            // InternalQuizDSL.g:864:2: ( ( (lv_regexp_0_0= '/' ) ) ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= '/' ( (lv_ignoreCase_3_0= '~' ) )? )
            {
            // InternalQuizDSL.g:864:2: ( ( (lv_regexp_0_0= '/' ) ) ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= '/' ( (lv_ignoreCase_3_0= '~' ) )? )
            // InternalQuizDSL.g:865:3: ( (lv_regexp_0_0= '/' ) ) ( (lv_value_1_0= RULE_STRING ) ) otherlv_2= '/' ( (lv_ignoreCase_3_0= '~' ) )?
            {
            // InternalQuizDSL.g:865:3: ( (lv_regexp_0_0= '/' ) )
            // InternalQuizDSL.g:866:4: (lv_regexp_0_0= '/' )
            {
            // InternalQuizDSL.g:866:4: (lv_regexp_0_0= '/' )
            // InternalQuizDSL.g:867:5: lv_regexp_0_0= '/'
            {
            lv_regexp_0_0=(Token)match(input,17,FOLLOW_8); 

            					newLeafNode(lv_regexp_0_0, grammarAccess.getRegexAnswerAccess().getRegexpSolidusKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRegexAnswerRule());
            					}
            					setWithLastConsumed(current, "regexp", lv_regexp_0_0 != null, "/");
            				

            }


            }

            // InternalQuizDSL.g:879:3: ( (lv_value_1_0= RULE_STRING ) )
            // InternalQuizDSL.g:880:4: (lv_value_1_0= RULE_STRING )
            {
            // InternalQuizDSL.g:880:4: (lv_value_1_0= RULE_STRING )
            // InternalQuizDSL.g:881:5: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

            					newLeafNode(lv_value_1_0, grammarAccess.getRegexAnswerAccess().getValueSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRegexAnswerRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_13); 

            			newLeafNode(otherlv_2, grammarAccess.getRegexAnswerAccess().getSolidusKeyword_2());
            		
            // InternalQuizDSL.g:901:3: ( (lv_ignoreCase_3_0= '~' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==16) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalQuizDSL.g:902:4: (lv_ignoreCase_3_0= '~' )
                    {
                    // InternalQuizDSL.g:902:4: (lv_ignoreCase_3_0= '~' )
                    // InternalQuizDSL.g:903:5: lv_ignoreCase_3_0= '~'
                    {
                    lv_ignoreCase_3_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_ignoreCase_3_0, grammarAccess.getRegexAnswerAccess().getIgnoreCaseTildeKeyword_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getRegexAnswerRule());
                    					}
                    					setWithLastConsumed(current, "ignoreCase", lv_ignoreCase_3_0 != null, "~");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRegexAnswer"


    // $ANTLR start "entryRuleNumberAnswer"
    // InternalQuizDSL.g:919:1: entryRuleNumberAnswer returns [EObject current=null] : iv_ruleNumberAnswer= ruleNumberAnswer EOF ;
    public final EObject entryRuleNumberAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberAnswer = null;


        try {
            // InternalQuizDSL.g:919:53: (iv_ruleNumberAnswer= ruleNumberAnswer EOF )
            // InternalQuizDSL.g:920:2: iv_ruleNumberAnswer= ruleNumberAnswer EOF
            {
             newCompositeNode(grammarAccess.getNumberAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNumberAnswer=ruleNumberAnswer();

            state._fsp--;

             current =iv_ruleNumberAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberAnswer"


    // $ANTLR start "ruleNumberAnswer"
    // InternalQuizDSL.g:926:1: ruleNumberAnswer returns [EObject current=null] : ( ( (lv_value_0_0= ruleEDoubleObject ) ) (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )? ) ;
    public final EObject ruleNumberAnswer() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_0_0 = null;

        AntlrDatatypeRuleToken lv_errorMargin_2_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:932:2: ( ( ( (lv_value_0_0= ruleEDoubleObject ) ) (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )? ) )
            // InternalQuizDSL.g:933:2: ( ( (lv_value_0_0= ruleEDoubleObject ) ) (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )? )
            {
            // InternalQuizDSL.g:933:2: ( ( (lv_value_0_0= ruleEDoubleObject ) ) (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )? )
            // InternalQuizDSL.g:934:3: ( (lv_value_0_0= ruleEDoubleObject ) ) (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )?
            {
            // InternalQuizDSL.g:934:3: ( (lv_value_0_0= ruleEDoubleObject ) )
            // InternalQuizDSL.g:935:4: (lv_value_0_0= ruleEDoubleObject )
            {
            // InternalQuizDSL.g:935:4: (lv_value_0_0= ruleEDoubleObject )
            // InternalQuizDSL.g:936:5: lv_value_0_0= ruleEDoubleObject
            {

            					newCompositeNode(grammarAccess.getNumberAnswerAccess().getValueEDoubleObjectParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_15);
            lv_value_0_0=ruleEDoubleObject();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNumberAnswerRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_0_0,
            						"no.hal.quiz.xtext.QuizDSL.EDoubleObject");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQuizDSL.g:953:3: (otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==18) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalQuizDSL.g:954:4: otherlv_1= '+-' ( (lv_errorMargin_2_0= ruleEDoubleObject ) )
                    {
                    otherlv_1=(Token)match(input,18,FOLLOW_16); 

                    				newLeafNode(otherlv_1, grammarAccess.getNumberAnswerAccess().getPlusSignHyphenMinusKeyword_1_0());
                    			
                    // InternalQuizDSL.g:958:4: ( (lv_errorMargin_2_0= ruleEDoubleObject ) )
                    // InternalQuizDSL.g:959:5: (lv_errorMargin_2_0= ruleEDoubleObject )
                    {
                    // InternalQuizDSL.g:959:5: (lv_errorMargin_2_0= ruleEDoubleObject )
                    // InternalQuizDSL.g:960:6: lv_errorMargin_2_0= ruleEDoubleObject
                    {

                    						newCompositeNode(grammarAccess.getNumberAnswerAccess().getErrorMarginEDoubleObjectParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_errorMargin_2_0=ruleEDoubleObject();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNumberAnswerRule());
                    						}
                    						set(
                    							current,
                    							"errorMargin",
                    							lv_errorMargin_2_0,
                    							"no.hal.quiz.xtext.QuizDSL.EDoubleObject");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberAnswer"


    // $ANTLR start "entryRuleEDoubleObject"
    // InternalQuizDSL.g:982:1: entryRuleEDoubleObject returns [String current=null] : iv_ruleEDoubleObject= ruleEDoubleObject EOF ;
    public final String entryRuleEDoubleObject() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDoubleObject = null;


        try {
            // InternalQuizDSL.g:982:53: (iv_ruleEDoubleObject= ruleEDoubleObject EOF )
            // InternalQuizDSL.g:983:2: iv_ruleEDoubleObject= ruleEDoubleObject EOF
            {
             newCompositeNode(grammarAccess.getEDoubleObjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDoubleObject=ruleEDoubleObject();

            state._fsp--;

             current =iv_ruleEDoubleObject.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDoubleObject"


    // $ANTLR start "ruleEDoubleObject"
    // InternalQuizDSL.g:989:1: ruleEDoubleObject returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDoubleObject() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:995:2: ( (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) )
            // InternalQuizDSL.g:996:2: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            {
            // InternalQuizDSL.g:996:2: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            // InternalQuizDSL.g:997:3: this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_6); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_0());
            		
            // InternalQuizDSL.g:1004:3: (kw= '.' this_INT_2= RULE_INT )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==13) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalQuizDSL.g:1005:4: kw= '.' this_INT_2= RULE_INT
                    {
                    kw=(Token)match(input,13,FOLLOW_16); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEDoubleObjectAccess().getFullStopKeyword_1_0());
                    			
                    this_INT_2=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_2);
                    			

                    				newLeafNode(this_INT_2, grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_1_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDoubleObject"


    // $ANTLR start "entryRuleBooleanAnswer"
    // InternalQuizDSL.g:1022:1: entryRuleBooleanAnswer returns [EObject current=null] : iv_ruleBooleanAnswer= ruleBooleanAnswer EOF ;
    public final EObject entryRuleBooleanAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanAnswer = null;


        try {
            // InternalQuizDSL.g:1022:54: (iv_ruleBooleanAnswer= ruleBooleanAnswer EOF )
            // InternalQuizDSL.g:1023:2: iv_ruleBooleanAnswer= ruleBooleanAnswer EOF
            {
             newCompositeNode(grammarAccess.getBooleanAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanAnswer=ruleBooleanAnswer();

            state._fsp--;

             current =iv_ruleBooleanAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanAnswer"


    // $ANTLR start "ruleBooleanAnswer"
    // InternalQuizDSL.g:1029:1: ruleBooleanAnswer returns [EObject current=null] : ( () ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) ) ) ;
    public final EObject ruleBooleanAnswer() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_1=null;
        Token lv_value_1_2=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:1035:2: ( ( () ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) ) ) )
            // InternalQuizDSL.g:1036:2: ( () ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) ) )
            {
            // InternalQuizDSL.g:1036:2: ( () ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) ) )
            // InternalQuizDSL.g:1037:3: () ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) )
            {
            // InternalQuizDSL.g:1037:3: ()
            // InternalQuizDSL.g:1038:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0(),
            					current);
            			

            }

            // InternalQuizDSL.g:1044:3: ( ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) ) | (otherlv_2= 'no' | otherlv_3= 'false' ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=19 && LA21_0<=20)) ) {
                alt21=1;
            }
            else if ( ((LA21_0>=21 && LA21_0<=22)) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalQuizDSL.g:1045:4: ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) )
                    {
                    // InternalQuizDSL.g:1045:4: ( ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) ) )
                    // InternalQuizDSL.g:1046:5: ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) )
                    {
                    // InternalQuizDSL.g:1046:5: ( (lv_value_1_1= 'yes' | lv_value_1_2= 'true' ) )
                    // InternalQuizDSL.g:1047:6: (lv_value_1_1= 'yes' | lv_value_1_2= 'true' )
                    {
                    // InternalQuizDSL.g:1047:6: (lv_value_1_1= 'yes' | lv_value_1_2= 'true' )
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==19) ) {
                        alt19=1;
                    }
                    else if ( (LA19_0==20) ) {
                        alt19=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 19, 0, input);

                        throw nvae;
                    }
                    switch (alt19) {
                        case 1 :
                            // InternalQuizDSL.g:1048:7: lv_value_1_1= 'yes'
                            {
                            lv_value_1_1=(Token)match(input,19,FOLLOW_2); 

                            							newLeafNode(lv_value_1_1, grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getBooleanAnswerRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_1_1 != null, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalQuizDSL.g:1059:7: lv_value_1_2= 'true'
                            {
                            lv_value_1_2=(Token)match(input,20,FOLLOW_2); 

                            							newLeafNode(lv_value_1_2, grammarAccess.getBooleanAnswerAccess().getValueTrueKeyword_1_0_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getBooleanAnswerRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_1_2 != null, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1073:4: (otherlv_2= 'no' | otherlv_3= 'false' )
                    {
                    // InternalQuizDSL.g:1073:4: (otherlv_2= 'no' | otherlv_3= 'false' )
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==21) ) {
                        alt20=1;
                    }
                    else if ( (LA20_0==22) ) {
                        alt20=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 0, input);

                        throw nvae;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalQuizDSL.g:1074:5: otherlv_2= 'no'
                            {
                            otherlv_2=(Token)match(input,21,FOLLOW_2); 

                            					newLeafNode(otherlv_2, grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalQuizDSL.g:1079:5: otherlv_3= 'false'
                            {
                            otherlv_3=(Token)match(input,22,FOLLOW_2); 

                            					newLeafNode(otherlv_3, grammarAccess.getBooleanAnswerAccess().getFalseKeyword_1_1_1());
                            				

                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanAnswer"


    // $ANTLR start "entryRuleXmlAnswer"
    // InternalQuizDSL.g:1089:1: entryRuleXmlAnswer returns [EObject current=null] : iv_ruleXmlAnswer= ruleXmlAnswer EOF ;
    public final EObject entryRuleXmlAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlAnswer = null;


        try {
            // InternalQuizDSL.g:1089:50: (iv_ruleXmlAnswer= ruleXmlAnswer EOF )
            // InternalQuizDSL.g:1090:2: iv_ruleXmlAnswer= ruleXmlAnswer EOF
            {
             newCompositeNode(grammarAccess.getXmlAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlAnswer=ruleXmlAnswer();

            state._fsp--;

             current =iv_ruleXmlAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlAnswer"


    // $ANTLR start "ruleXmlAnswer"
    // InternalQuizDSL.g:1096:1: ruleXmlAnswer returns [EObject current=null] : ( (lv_xml_0_0= ruleXml ) ) ;
    public final EObject ruleXmlAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_xml_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1102:2: ( ( (lv_xml_0_0= ruleXml ) ) )
            // InternalQuizDSL.g:1103:2: ( (lv_xml_0_0= ruleXml ) )
            {
            // InternalQuizDSL.g:1103:2: ( (lv_xml_0_0= ruleXml ) )
            // InternalQuizDSL.g:1104:3: (lv_xml_0_0= ruleXml )
            {
            // InternalQuizDSL.g:1104:3: (lv_xml_0_0= ruleXml )
            // InternalQuizDSL.g:1105:4: lv_xml_0_0= ruleXml
            {

            				newCompositeNode(grammarAccess.getXmlAnswerAccess().getXmlXmlParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_xml_0_0=ruleXml();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getXmlAnswerRule());
            				}
            				set(
            					current,
            					"xml",
            					lv_xml_0_0,
            					"no.hal.quiz.xtext.QuizDSL.Xml");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlAnswer"


    // $ANTLR start "entryRuleOptionsAnswer"
    // InternalQuizDSL.g:1125:1: entryRuleOptionsAnswer returns [EObject current=null] : iv_ruleOptionsAnswer= ruleOptionsAnswer EOF ;
    public final EObject entryRuleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionsAnswer = null;


        try {
            // InternalQuizDSL.g:1125:54: (iv_ruleOptionsAnswer= ruleOptionsAnswer EOF )
            // InternalQuizDSL.g:1126:2: iv_ruleOptionsAnswer= ruleOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOptionsAnswer=ruleOptionsAnswer();

            state._fsp--;

             current =iv_ruleOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionsAnswer"


    // $ANTLR start "ruleOptionsAnswer"
    // InternalQuizDSL.g:1132:1: ruleOptionsAnswer returns [EObject current=null] : (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer ) ;
    public final EObject ruleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_SingleOptionsAnswer_0 = null;

        EObject this_ManyOptionsAnswer_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1138:2: ( (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer ) )
            // InternalQuizDSL.g:1139:2: (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer )
            {
            // InternalQuizDSL.g:1139:2: (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=23 && LA22_0<=24)||(LA22_0>=26 && LA22_0<=27)) ) {
                alt22=1;
            }
            else if ( (LA22_0==28) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalQuizDSL.g:1140:3: this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SingleOptionsAnswer_0=ruleSingleOptionsAnswer();

                    state._fsp--;


                    			current = this_SingleOptionsAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1149:3: this_ManyOptionsAnswer_1= ruleManyOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyOptionsAnswer_1=ruleManyOptionsAnswer();

                    state._fsp--;


                    			current = this_ManyOptionsAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionsAnswer"


    // $ANTLR start "entryRuleSingleOptionsAnswer"
    // InternalQuizDSL.g:1161:1: entryRuleSingleOptionsAnswer returns [EObject current=null] : iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF ;
    public final EObject entryRuleSingleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleOptionsAnswer = null;


        try {
            // InternalQuizDSL.g:1161:60: (iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF )
            // InternalQuizDSL.g:1162:2: iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getSingleOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleOptionsAnswer=ruleSingleOptionsAnswer();

            state._fsp--;

             current =iv_ruleSingleOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleOptionsAnswer"


    // $ANTLR start "ruleSingleOptionsAnswer"
    // InternalQuizDSL.g:1168:1: ruleSingleOptionsAnswer returns [EObject current=null] : (this_SingleBoxOptionsAnswer_0= ruleSingleBoxOptionsAnswer | this_SingleListOptionsAnswer_1= ruleSingleListOptionsAnswer ) ;
    public final EObject ruleSingleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_SingleBoxOptionsAnswer_0 = null;

        EObject this_SingleListOptionsAnswer_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1174:2: ( (this_SingleBoxOptionsAnswer_0= ruleSingleBoxOptionsAnswer | this_SingleListOptionsAnswer_1= ruleSingleListOptionsAnswer ) )
            // InternalQuizDSL.g:1175:2: (this_SingleBoxOptionsAnswer_0= ruleSingleBoxOptionsAnswer | this_SingleListOptionsAnswer_1= ruleSingleListOptionsAnswer )
            {
            // InternalQuizDSL.g:1175:2: (this_SingleBoxOptionsAnswer_0= ruleSingleBoxOptionsAnswer | this_SingleListOptionsAnswer_1= ruleSingleListOptionsAnswer )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==23) ) {
                alt23=1;
            }
            else if ( (LA23_0==24||(LA23_0>=26 && LA23_0<=27)) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalQuizDSL.g:1176:3: this_SingleBoxOptionsAnswer_0= ruleSingleBoxOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getSingleOptionsAnswerAccess().getSingleBoxOptionsAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SingleBoxOptionsAnswer_0=ruleSingleBoxOptionsAnswer();

                    state._fsp--;


                    			current = this_SingleBoxOptionsAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1185:3: this_SingleListOptionsAnswer_1= ruleSingleListOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getSingleOptionsAnswerAccess().getSingleListOptionsAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SingleListOptionsAnswer_1=ruleSingleListOptionsAnswer();

                    state._fsp--;


                    			current = this_SingleListOptionsAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleOptionsAnswer"


    // $ANTLR start "entryRuleSingleBoxOptionsAnswer"
    // InternalQuizDSL.g:1197:1: entryRuleSingleBoxOptionsAnswer returns [EObject current=null] : iv_ruleSingleBoxOptionsAnswer= ruleSingleBoxOptionsAnswer EOF ;
    public final EObject entryRuleSingleBoxOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleBoxOptionsAnswer = null;


        try {
            // InternalQuizDSL.g:1197:63: (iv_ruleSingleBoxOptionsAnswer= ruleSingleBoxOptionsAnswer EOF )
            // InternalQuizDSL.g:1198:2: iv_ruleSingleBoxOptionsAnswer= ruleSingleBoxOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getSingleBoxOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleBoxOptionsAnswer=ruleSingleBoxOptionsAnswer();

            state._fsp--;

             current =iv_ruleSingleBoxOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleBoxOptionsAnswer"


    // $ANTLR start "ruleSingleBoxOptionsAnswer"
    // InternalQuizDSL.g:1204:1: ruleSingleBoxOptionsAnswer returns [EObject current=null] : ( (lv_options_0_0= ruleSingleBoxOption ) )+ ;
    public final EObject ruleSingleBoxOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_options_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1210:2: ( ( (lv_options_0_0= ruleSingleBoxOption ) )+ )
            // InternalQuizDSL.g:1211:2: ( (lv_options_0_0= ruleSingleBoxOption ) )+
            {
            // InternalQuizDSL.g:1211:2: ( (lv_options_0_0= ruleSingleBoxOption ) )+
            int cnt24=0;
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==23) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalQuizDSL.g:1212:3: (lv_options_0_0= ruleSingleBoxOption )
            	    {
            	    // InternalQuizDSL.g:1212:3: (lv_options_0_0= ruleSingleBoxOption )
            	    // InternalQuizDSL.g:1213:4: lv_options_0_0= ruleSingleBoxOption
            	    {

            	    				newCompositeNode(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsSingleBoxOptionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_17);
            	    lv_options_0_0=ruleSingleBoxOption();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getSingleBoxOptionsAnswerRule());
            	    				}
            	    				add(
            	    					current,
            	    					"options",
            	    					lv_options_0_0,
            	    					"no.hal.quiz.xtext.QuizDSL.SingleBoxOption");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt24 >= 1 ) break loop24;
                        EarlyExitException eee =
                            new EarlyExitException(24, input);
                        throw eee;
                }
                cnt24++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleBoxOptionsAnswer"


    // $ANTLR start "entryRuleSingleBoxOption"
    // InternalQuizDSL.g:1233:1: entryRuleSingleBoxOption returns [EObject current=null] : iv_ruleSingleBoxOption= ruleSingleBoxOption EOF ;
    public final EObject entryRuleSingleBoxOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleBoxOption = null;


        try {
            // InternalQuizDSL.g:1233:56: (iv_ruleSingleBoxOption= ruleSingleBoxOption EOF )
            // InternalQuizDSL.g:1234:2: iv_ruleSingleBoxOption= ruleSingleBoxOption EOF
            {
             newCompositeNode(grammarAccess.getSingleBoxOptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleBoxOption=ruleSingleBoxOption();

            state._fsp--;

             current =iv_ruleSingleBoxOption; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleBoxOption"


    // $ANTLR start "ruleSingleBoxOption"
    // InternalQuizDSL.g:1240:1: ruleSingleBoxOption returns [EObject current=null] : (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) ) ;
    public final EObject ruleSingleBoxOption() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_correct_1_0=null;
        Token otherlv_2=null;
        EObject lv_option_3_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1246:2: ( (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) ) )
            // InternalQuizDSL.g:1247:2: (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            {
            // InternalQuizDSL.g:1247:2: (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            // InternalQuizDSL.g:1248:3: otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getSingleBoxOptionAccess().getLeftParenthesisKeyword_0());
            		
            // InternalQuizDSL.g:1252:3: ( (lv_correct_1_0= 'x' ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==24) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalQuizDSL.g:1253:4: (lv_correct_1_0= 'x' )
                    {
                    // InternalQuizDSL.g:1253:4: (lv_correct_1_0= 'x' )
                    // InternalQuizDSL.g:1254:5: lv_correct_1_0= 'x'
                    {
                    lv_correct_1_0=(Token)match(input,24,FOLLOW_19); 

                    					newLeafNode(lv_correct_1_0, grammarAccess.getSingleBoxOptionAccess().getCorrectXKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSingleBoxOptionRule());
                    					}
                    					setWithLastConsumed(current, "correct", lv_correct_1_0 != null, "x");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,25,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getSingleBoxOptionAccess().getRightParenthesisKeyword_2());
            		
            // InternalQuizDSL.g:1270:3: ( (lv_option_3_0= ruleOptionAnswer ) )
            // InternalQuizDSL.g:1271:4: (lv_option_3_0= ruleOptionAnswer )
            {
            // InternalQuizDSL.g:1271:4: (lv_option_3_0= ruleOptionAnswer )
            // InternalQuizDSL.g:1272:5: lv_option_3_0= ruleOptionAnswer
            {

            					newCompositeNode(grammarAccess.getSingleBoxOptionAccess().getOptionOptionAnswerParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_option_3_0=ruleOptionAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSingleBoxOptionRule());
            					}
            					set(
            						current,
            						"option",
            						lv_option_3_0,
            						"no.hal.quiz.xtext.QuizDSL.OptionAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleBoxOption"


    // $ANTLR start "entryRuleSingleListOptionsAnswer"
    // InternalQuizDSL.g:1293:1: entryRuleSingleListOptionsAnswer returns [EObject current=null] : iv_ruleSingleListOptionsAnswer= ruleSingleListOptionsAnswer EOF ;
    public final EObject entryRuleSingleListOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleListOptionsAnswer = null;


        try {
            // InternalQuizDSL.g:1293:64: (iv_ruleSingleListOptionsAnswer= ruleSingleListOptionsAnswer EOF )
            // InternalQuizDSL.g:1294:2: iv_ruleSingleListOptionsAnswer= ruleSingleListOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getSingleListOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleListOptionsAnswer=ruleSingleListOptionsAnswer();

            state._fsp--;

             current =iv_ruleSingleListOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleListOptionsAnswer"


    // $ANTLR start "ruleSingleListOptionsAnswer"
    // InternalQuizDSL.g:1300:1: ruleSingleListOptionsAnswer returns [EObject current=null] : ( (lv_options_0_0= ruleSingleListOption ) )+ ;
    public final EObject ruleSingleListOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_options_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1306:2: ( ( (lv_options_0_0= ruleSingleListOption ) )+ )
            // InternalQuizDSL.g:1307:2: ( (lv_options_0_0= ruleSingleListOption ) )+
            {
            // InternalQuizDSL.g:1307:2: ( (lv_options_0_0= ruleSingleListOption ) )+
            int cnt26=0;
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==24||(LA26_0>=26 && LA26_0<=27)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalQuizDSL.g:1308:3: (lv_options_0_0= ruleSingleListOption )
            	    {
            	    // InternalQuizDSL.g:1308:3: (lv_options_0_0= ruleSingleListOption )
            	    // InternalQuizDSL.g:1309:4: lv_options_0_0= ruleSingleListOption
            	    {

            	    				newCompositeNode(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_21);
            	    lv_options_0_0=ruleSingleListOption();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getSingleListOptionsAnswerRule());
            	    				}
            	    				add(
            	    					current,
            	    					"options",
            	    					lv_options_0_0,
            	    					"no.hal.quiz.xtext.QuizDSL.SingleListOption");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt26 >= 1 ) break loop26;
                        EarlyExitException eee =
                            new EarlyExitException(26, input);
                        throw eee;
                }
                cnt26++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleListOptionsAnswer"


    // $ANTLR start "entryRuleSingleListOption"
    // InternalQuizDSL.g:1329:1: entryRuleSingleListOption returns [EObject current=null] : iv_ruleSingleListOption= ruleSingleListOption EOF ;
    public final EObject entryRuleSingleListOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleListOption = null;


        try {
            // InternalQuizDSL.g:1329:57: (iv_ruleSingleListOption= ruleSingleListOption EOF )
            // InternalQuizDSL.g:1330:2: iv_ruleSingleListOption= ruleSingleListOption EOF
            {
             newCompositeNode(grammarAccess.getSingleListOptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleListOption=ruleSingleListOption();

            state._fsp--;

             current =iv_ruleSingleListOption; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleListOption"


    // $ANTLR start "ruleSingleListOption"
    // InternalQuizDSL.g:1336:1: ruleSingleListOption returns [EObject current=null] : ( ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) ) ( (lv_option_3_0= ruleOptionAnswer ) ) ) ;
    public final EObject ruleSingleListOption() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_correct_2_0=null;
        EObject lv_option_3_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1342:2: ( ( ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) ) ( (lv_option_3_0= ruleOptionAnswer ) ) ) )
            // InternalQuizDSL.g:1343:2: ( ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) ) ( (lv_option_3_0= ruleOptionAnswer ) ) )
            {
            // InternalQuizDSL.g:1343:2: ( ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) ) ( (lv_option_3_0= ruleOptionAnswer ) ) )
            // InternalQuizDSL.g:1344:3: ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) ) ( (lv_option_3_0= ruleOptionAnswer ) )
            {
            // InternalQuizDSL.g:1344:3: ( (otherlv_0= '-' | otherlv_1= 'x' ) | ( (lv_correct_2_0= 'v' ) ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==24||LA28_0==26) ) {
                alt28=1;
            }
            else if ( (LA28_0==27) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalQuizDSL.g:1345:4: (otherlv_0= '-' | otherlv_1= 'x' )
                    {
                    // InternalQuizDSL.g:1345:4: (otherlv_0= '-' | otherlv_1= 'x' )
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( (LA27_0==26) ) {
                        alt27=1;
                    }
                    else if ( (LA27_0==24) ) {
                        alt27=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 27, 0, input);

                        throw nvae;
                    }
                    switch (alt27) {
                        case 1 :
                            // InternalQuizDSL.g:1346:5: otherlv_0= '-'
                            {
                            otherlv_0=(Token)match(input,26,FOLLOW_20); 

                            					newLeafNode(otherlv_0, grammarAccess.getSingleListOptionAccess().getHyphenMinusKeyword_0_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalQuizDSL.g:1351:5: otherlv_1= 'x'
                            {
                            otherlv_1=(Token)match(input,24,FOLLOW_20); 

                            					newLeafNode(otherlv_1, grammarAccess.getSingleListOptionAccess().getXKeyword_0_0_1());
                            				

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1357:4: ( (lv_correct_2_0= 'v' ) )
                    {
                    // InternalQuizDSL.g:1357:4: ( (lv_correct_2_0= 'v' ) )
                    // InternalQuizDSL.g:1358:5: (lv_correct_2_0= 'v' )
                    {
                    // InternalQuizDSL.g:1358:5: (lv_correct_2_0= 'v' )
                    // InternalQuizDSL.g:1359:6: lv_correct_2_0= 'v'
                    {
                    lv_correct_2_0=(Token)match(input,27,FOLLOW_20); 

                    						newLeafNode(lv_correct_2_0, grammarAccess.getSingleListOptionAccess().getCorrectVKeyword_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSingleListOptionRule());
                    						}
                    						setWithLastConsumed(current, "correct", lv_correct_2_0 != null, "v");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQuizDSL.g:1372:3: ( (lv_option_3_0= ruleOptionAnswer ) )
            // InternalQuizDSL.g:1373:4: (lv_option_3_0= ruleOptionAnswer )
            {
            // InternalQuizDSL.g:1373:4: (lv_option_3_0= ruleOptionAnswer )
            // InternalQuizDSL.g:1374:5: lv_option_3_0= ruleOptionAnswer
            {

            					newCompositeNode(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_option_3_0=ruleOptionAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSingleListOptionRule());
            					}
            					set(
            						current,
            						"option",
            						lv_option_3_0,
            						"no.hal.quiz.xtext.QuizDSL.OptionAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleListOption"


    // $ANTLR start "entryRuleManyOptionsAnswer"
    // InternalQuizDSL.g:1395:1: entryRuleManyOptionsAnswer returns [EObject current=null] : iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF ;
    public final EObject entryRuleManyOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyOptionsAnswer = null;


        try {
            // InternalQuizDSL.g:1395:58: (iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF )
            // InternalQuizDSL.g:1396:2: iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getManyOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyOptionsAnswer=ruleManyOptionsAnswer();

            state._fsp--;

             current =iv_ruleManyOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyOptionsAnswer"


    // $ANTLR start "ruleManyOptionsAnswer"
    // InternalQuizDSL.g:1402:1: ruleManyOptionsAnswer returns [EObject current=null] : ( (lv_options_0_0= ruleManyOption ) )+ ;
    public final EObject ruleManyOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_options_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1408:2: ( ( (lv_options_0_0= ruleManyOption ) )+ )
            // InternalQuizDSL.g:1409:2: ( (lv_options_0_0= ruleManyOption ) )+
            {
            // InternalQuizDSL.g:1409:2: ( (lv_options_0_0= ruleManyOption ) )+
            int cnt29=0;
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==28) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalQuizDSL.g:1410:3: (lv_options_0_0= ruleManyOption )
            	    {
            	    // InternalQuizDSL.g:1410:3: (lv_options_0_0= ruleManyOption )
            	    // InternalQuizDSL.g:1411:4: lv_options_0_0= ruleManyOption
            	    {

            	    				newCompositeNode(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_22);
            	    lv_options_0_0=ruleManyOption();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getManyOptionsAnswerRule());
            	    				}
            	    				add(
            	    					current,
            	    					"options",
            	    					lv_options_0_0,
            	    					"no.hal.quiz.xtext.QuizDSL.ManyOption");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt29 >= 1 ) break loop29;
                        EarlyExitException eee =
                            new EarlyExitException(29, input);
                        throw eee;
                }
                cnt29++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyOptionsAnswer"


    // $ANTLR start "entryRuleManyOption"
    // InternalQuizDSL.g:1431:1: entryRuleManyOption returns [EObject current=null] : iv_ruleManyOption= ruleManyOption EOF ;
    public final EObject entryRuleManyOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyOption = null;


        try {
            // InternalQuizDSL.g:1431:51: (iv_ruleManyOption= ruleManyOption EOF )
            // InternalQuizDSL.g:1432:2: iv_ruleManyOption= ruleManyOption EOF
            {
             newCompositeNode(grammarAccess.getManyOptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyOption=ruleManyOption();

            state._fsp--;

             current =iv_ruleManyOption; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyOption"


    // $ANTLR start "ruleManyOption"
    // InternalQuizDSL.g:1438:1: ruleManyOption returns [EObject current=null] : (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) ) ;
    public final EObject ruleManyOption() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_correct_1_0=null;
        Token otherlv_2=null;
        EObject lv_option_3_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1444:2: ( (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) ) )
            // InternalQuizDSL.g:1445:2: (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            {
            // InternalQuizDSL.g:1445:2: (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            // InternalQuizDSL.g:1446:3: otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) )
            {
            otherlv_0=(Token)match(input,28,FOLLOW_23); 

            			newLeafNode(otherlv_0, grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalQuizDSL.g:1450:3: ( (lv_correct_1_0= 'x' ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==24) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalQuizDSL.g:1451:4: (lv_correct_1_0= 'x' )
                    {
                    // InternalQuizDSL.g:1451:4: (lv_correct_1_0= 'x' )
                    // InternalQuizDSL.g:1452:5: lv_correct_1_0= 'x'
                    {
                    lv_correct_1_0=(Token)match(input,24,FOLLOW_24); 

                    					newLeafNode(lv_correct_1_0, grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getManyOptionRule());
                    					}
                    					setWithLastConsumed(current, "correct", lv_correct_1_0 != null, "x");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,29,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2());
            		
            // InternalQuizDSL.g:1468:3: ( (lv_option_3_0= ruleOptionAnswer ) )
            // InternalQuizDSL.g:1469:4: (lv_option_3_0= ruleOptionAnswer )
            {
            // InternalQuizDSL.g:1469:4: (lv_option_3_0= ruleOptionAnswer )
            // InternalQuizDSL.g:1470:5: lv_option_3_0= ruleOptionAnswer
            {

            					newCompositeNode(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_option_3_0=ruleOptionAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getManyOptionRule());
            					}
            					set(
            						current,
            						"option",
            						lv_option_3_0,
            						"no.hal.quiz.xtext.QuizDSL.OptionAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyOption"


    // $ANTLR start "entryRuleXml"
    // InternalQuizDSL.g:1491:1: entryRuleXml returns [EObject current=null] : iv_ruleXml= ruleXml EOF ;
    public final EObject entryRuleXml() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXml = null;


        try {
            // InternalQuizDSL.g:1491:44: (iv_ruleXml= ruleXml EOF )
            // InternalQuizDSL.g:1492:2: iv_ruleXml= ruleXml EOF
            {
             newCompositeNode(grammarAccess.getXmlRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXml=ruleXml();

            state._fsp--;

             current =iv_ruleXml; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXml"


    // $ANTLR start "ruleXml"
    // InternalQuizDSL.g:1498:1: ruleXml returns [EObject current=null] : (otherlv_0= '<<' ( (lv_element_1_0= ruleXmlElement ) ) otherlv_2= '>>' ) ;
    public final EObject ruleXml() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_element_1_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1504:2: ( (otherlv_0= '<<' ( (lv_element_1_0= ruleXmlElement ) ) otherlv_2= '>>' ) )
            // InternalQuizDSL.g:1505:2: (otherlv_0= '<<' ( (lv_element_1_0= ruleXmlElement ) ) otherlv_2= '>>' )
            {
            // InternalQuizDSL.g:1505:2: (otherlv_0= '<<' ( (lv_element_1_0= ruleXmlElement ) ) otherlv_2= '>>' )
            // InternalQuizDSL.g:1506:3: otherlv_0= '<<' ( (lv_element_1_0= ruleXmlElement ) ) otherlv_2= '>>'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_25); 

            			newLeafNode(otherlv_0, grammarAccess.getXmlAccess().getLessThanSignLessThanSignKeyword_0());
            		
            // InternalQuizDSL.g:1510:3: ( (lv_element_1_0= ruleXmlElement ) )
            // InternalQuizDSL.g:1511:4: (lv_element_1_0= ruleXmlElement )
            {
            // InternalQuizDSL.g:1511:4: (lv_element_1_0= ruleXmlElement )
            // InternalQuizDSL.g:1512:5: lv_element_1_0= ruleXmlElement
            {

            					newCompositeNode(grammarAccess.getXmlAccess().getElementXmlElementParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_26);
            lv_element_1_0=ruleXmlElement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getXmlRule());
            					}
            					set(
            						current,
            						"element",
            						lv_element_1_0,
            						"no.hal.quiz.xtext.QuizDSL.XmlElement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getXmlAccess().getGreaterThanSignGreaterThanSignKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXml"


    // $ANTLR start "entryRuleXmlContents"
    // InternalQuizDSL.g:1537:1: entryRuleXmlContents returns [EObject current=null] : iv_ruleXmlContents= ruleXmlContents EOF ;
    public final EObject entryRuleXmlContents() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlContents = null;


        try {
            // InternalQuizDSL.g:1537:52: (iv_ruleXmlContents= ruleXmlContents EOF )
            // InternalQuizDSL.g:1538:2: iv_ruleXmlContents= ruleXmlContents EOF
            {
             newCompositeNode(grammarAccess.getXmlContentsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlContents=ruleXmlContents();

            state._fsp--;

             current =iv_ruleXmlContents; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlContents"


    // $ANTLR start "ruleXmlContents"
    // InternalQuizDSL.g:1544:1: ruleXmlContents returns [EObject current=null] : ( ( (lv_element_0_0= ruleXmlElement ) ) ( (lv_post_1_0= RULE_XML_TEXT ) ) ) ;
    public final EObject ruleXmlContents() throws RecognitionException {
        EObject current = null;

        Token lv_post_1_0=null;
        EObject lv_element_0_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1550:2: ( ( ( (lv_element_0_0= ruleXmlElement ) ) ( (lv_post_1_0= RULE_XML_TEXT ) ) ) )
            // InternalQuizDSL.g:1551:2: ( ( (lv_element_0_0= ruleXmlElement ) ) ( (lv_post_1_0= RULE_XML_TEXT ) ) )
            {
            // InternalQuizDSL.g:1551:2: ( ( (lv_element_0_0= ruleXmlElement ) ) ( (lv_post_1_0= RULE_XML_TEXT ) ) )
            // InternalQuizDSL.g:1552:3: ( (lv_element_0_0= ruleXmlElement ) ) ( (lv_post_1_0= RULE_XML_TEXT ) )
            {
            // InternalQuizDSL.g:1552:3: ( (lv_element_0_0= ruleXmlElement ) )
            // InternalQuizDSL.g:1553:4: (lv_element_0_0= ruleXmlElement )
            {
            // InternalQuizDSL.g:1553:4: (lv_element_0_0= ruleXmlElement )
            // InternalQuizDSL.g:1554:5: lv_element_0_0= ruleXmlElement
            {

            					newCompositeNode(grammarAccess.getXmlContentsAccess().getElementXmlElementParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_27);
            lv_element_0_0=ruleXmlElement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getXmlContentsRule());
            					}
            					set(
            						current,
            						"element",
            						lv_element_0_0,
            						"no.hal.quiz.xtext.QuizDSL.XmlElement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQuizDSL.g:1571:3: ( (lv_post_1_0= RULE_XML_TEXT ) )
            // InternalQuizDSL.g:1572:4: (lv_post_1_0= RULE_XML_TEXT )
            {
            // InternalQuizDSL.g:1572:4: (lv_post_1_0= RULE_XML_TEXT )
            // InternalQuizDSL.g:1573:5: lv_post_1_0= RULE_XML_TEXT
            {
            lv_post_1_0=(Token)match(input,RULE_XML_TEXT,FOLLOW_2); 

            					newLeafNode(lv_post_1_0, grammarAccess.getXmlContentsAccess().getPostXML_TEXTTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXmlContentsRule());
            					}
            					setWithLastConsumed(
            						current,
            						"post",
            						lv_post_1_0,
            						"no.hal.quiz.xtext.QuizDSL.XML_TEXT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlContents"


    // $ANTLR start "entryRuleXmlElement"
    // InternalQuizDSL.g:1593:1: entryRuleXmlElement returns [EObject current=null] : iv_ruleXmlElement= ruleXmlElement EOF ;
    public final EObject entryRuleXmlElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlElement = null;


        try {
            // InternalQuizDSL.g:1593:51: (iv_ruleXmlElement= ruleXmlElement EOF )
            // InternalQuizDSL.g:1594:2: iv_ruleXmlElement= ruleXmlElement EOF
            {
             newCompositeNode(grammarAccess.getXmlElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlElement=ruleXmlElement();

            state._fsp--;

             current =iv_ruleXmlElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlElement"


    // $ANTLR start "ruleXmlElement"
    // InternalQuizDSL.g:1600:1: ruleXmlElement returns [EObject current=null] : (this_XmlPIAnswerElement_0= ruleXmlPIAnswerElement | this_XmlTagElement_1= ruleXmlTagElement ) ;
    public final EObject ruleXmlElement() throws RecognitionException {
        EObject current = null;

        EObject this_XmlPIAnswerElement_0 = null;

        EObject this_XmlTagElement_1 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1606:2: ( (this_XmlPIAnswerElement_0= ruleXmlPIAnswerElement | this_XmlTagElement_1= ruleXmlTagElement ) )
            // InternalQuizDSL.g:1607:2: (this_XmlPIAnswerElement_0= ruleXmlPIAnswerElement | this_XmlTagElement_1= ruleXmlTagElement )
            {
            // InternalQuizDSL.g:1607:2: (this_XmlPIAnswerElement_0= ruleXmlPIAnswerElement | this_XmlTagElement_1= ruleXmlTagElement )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==32) ) {
                alt31=1;
            }
            else if ( (LA31_0==RULE_ID) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalQuizDSL.g:1608:3: this_XmlPIAnswerElement_0= ruleXmlPIAnswerElement
                    {

                    			newCompositeNode(grammarAccess.getXmlElementAccess().getXmlPIAnswerElementParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_XmlPIAnswerElement_0=ruleXmlPIAnswerElement();

                    state._fsp--;


                    			current = this_XmlPIAnswerElement_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1617:3: this_XmlTagElement_1= ruleXmlTagElement
                    {

                    			newCompositeNode(grammarAccess.getXmlElementAccess().getXmlTagElementParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_XmlTagElement_1=ruleXmlTagElement();

                    state._fsp--;


                    			current = this_XmlTagElement_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlElement"


    // $ANTLR start "entryRuleXmlPIAnswerElement"
    // InternalQuizDSL.g:1629:1: entryRuleXmlPIAnswerElement returns [EObject current=null] : iv_ruleXmlPIAnswerElement= ruleXmlPIAnswerElement EOF ;
    public final EObject entryRuleXmlPIAnswerElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlPIAnswerElement = null;


        try {
            // InternalQuizDSL.g:1629:59: (iv_ruleXmlPIAnswerElement= ruleXmlPIAnswerElement EOF )
            // InternalQuizDSL.g:1630:2: iv_ruleXmlPIAnswerElement= ruleXmlPIAnswerElement EOF
            {
             newCompositeNode(grammarAccess.getXmlPIAnswerElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlPIAnswerElement=ruleXmlPIAnswerElement();

            state._fsp--;

             current =iv_ruleXmlPIAnswerElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlPIAnswerElement"


    // $ANTLR start "ruleXmlPIAnswerElement"
    // InternalQuizDSL.g:1636:1: ruleXmlPIAnswerElement returns [EObject current=null] : (otherlv_0= '?' ( (lv_answer_1_0= ruleSimpleAnswer ) ) otherlv_2= '?' ) ;
    public final EObject ruleXmlPIAnswerElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_answer_1_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1642:2: ( (otherlv_0= '?' ( (lv_answer_1_0= ruleSimpleAnswer ) ) otherlv_2= '?' ) )
            // InternalQuizDSL.g:1643:2: (otherlv_0= '?' ( (lv_answer_1_0= ruleSimpleAnswer ) ) otherlv_2= '?' )
            {
            // InternalQuizDSL.g:1643:2: (otherlv_0= '?' ( (lv_answer_1_0= ruleSimpleAnswer ) ) otherlv_2= '?' )
            // InternalQuizDSL.g:1644:3: otherlv_0= '?' ( (lv_answer_1_0= ruleSimpleAnswer ) ) otherlv_2= '?'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_28); 

            			newLeafNode(otherlv_0, grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_0());
            		
            // InternalQuizDSL.g:1648:3: ( (lv_answer_1_0= ruleSimpleAnswer ) )
            // InternalQuizDSL.g:1649:4: (lv_answer_1_0= ruleSimpleAnswer )
            {
            // InternalQuizDSL.g:1649:4: (lv_answer_1_0= ruleSimpleAnswer )
            // InternalQuizDSL.g:1650:5: lv_answer_1_0= ruleSimpleAnswer
            {

            					newCompositeNode(grammarAccess.getXmlPIAnswerElementAccess().getAnswerSimpleAnswerParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_29);
            lv_answer_1_0=ruleSimpleAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getXmlPIAnswerElementRule());
            					}
            					set(
            						current,
            						"answer",
            						lv_answer_1_0,
            						"no.hal.quiz.xtext.QuizDSL.SimpleAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlPIAnswerElement"


    // $ANTLR start "entryRuleXmlTagElement"
    // InternalQuizDSL.g:1675:1: entryRuleXmlTagElement returns [EObject current=null] : iv_ruleXmlTagElement= ruleXmlTagElement EOF ;
    public final EObject entryRuleXmlTagElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlTagElement = null;


        try {
            // InternalQuizDSL.g:1675:54: (iv_ruleXmlTagElement= ruleXmlTagElement EOF )
            // InternalQuizDSL.g:1676:2: iv_ruleXmlTagElement= ruleXmlTagElement EOF
            {
             newCompositeNode(grammarAccess.getXmlTagElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlTagElement=ruleXmlTagElement();

            state._fsp--;

             current =iv_ruleXmlTagElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlTagElement"


    // $ANTLR start "ruleXmlTagElement"
    // InternalQuizDSL.g:1682:1: ruleXmlTagElement returns [EObject current=null] : ( ( (lv_startTag_0_0= ruleXmlTag ) ) (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) ) ) ;
    public final EObject ruleXmlTagElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_pre_2_0=null;
        Token otherlv_4=null;
        Token lv_endTag_5_0=null;
        EObject lv_startTag_0_0 = null;

        EObject lv_contents_3_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1688:2: ( ( ( (lv_startTag_0_0= ruleXmlTag ) ) (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) ) ) )
            // InternalQuizDSL.g:1689:2: ( ( (lv_startTag_0_0= ruleXmlTag ) ) (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) ) )
            {
            // InternalQuizDSL.g:1689:2: ( ( (lv_startTag_0_0= ruleXmlTag ) ) (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) ) )
            // InternalQuizDSL.g:1690:3: ( (lv_startTag_0_0= ruleXmlTag ) ) (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) )
            {
            // InternalQuizDSL.g:1690:3: ( (lv_startTag_0_0= ruleXmlTag ) )
            // InternalQuizDSL.g:1691:4: (lv_startTag_0_0= ruleXmlTag )
            {
            // InternalQuizDSL.g:1691:4: (lv_startTag_0_0= ruleXmlTag )
            // InternalQuizDSL.g:1692:5: lv_startTag_0_0= ruleXmlTag
            {

            					newCompositeNode(grammarAccess.getXmlTagElementAccess().getStartTagXmlTagParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_30);
            lv_startTag_0_0=ruleXmlTag();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getXmlTagElementRule());
            					}
            					set(
            						current,
            						"startTag",
            						lv_startTag_0_0,
            						"no.hal.quiz.xtext.QuizDSL.XmlTag");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQuizDSL.g:1709:3: (otherlv_1= '/' | ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) ) )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==17) ) {
                alt34=1;
            }
            else if ( (LA34_0==RULE_XML_TEXT) ) {
                alt34=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // InternalQuizDSL.g:1710:4: otherlv_1= '/'
                    {
                    otherlv_1=(Token)match(input,17,FOLLOW_2); 

                    				newLeafNode(otherlv_1, grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1715:4: ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) )
                    {
                    // InternalQuizDSL.g:1715:4: ( ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? ) )
                    // InternalQuizDSL.g:1716:5: ( (lv_pre_2_0= RULE_XML_TEXT ) ) ( (lv_contents_3_0= ruleXmlContents ) )* (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? )
                    {
                    // InternalQuizDSL.g:1716:5: ( (lv_pre_2_0= RULE_XML_TEXT ) )
                    // InternalQuizDSL.g:1717:6: (lv_pre_2_0= RULE_XML_TEXT )
                    {
                    // InternalQuizDSL.g:1717:6: (lv_pre_2_0= RULE_XML_TEXT )
                    // InternalQuizDSL.g:1718:7: lv_pre_2_0= RULE_XML_TEXT
                    {
                    lv_pre_2_0=(Token)match(input,RULE_XML_TEXT,FOLLOW_31); 

                    							newLeafNode(lv_pre_2_0, grammarAccess.getXmlTagElementAccess().getPreXML_TEXTTerminalRuleCall_1_1_0_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getXmlTagElementRule());
                    							}
                    							setWithLastConsumed(
                    								current,
                    								"pre",
                    								lv_pre_2_0,
                    								"no.hal.quiz.xtext.QuizDSL.XML_TEXT");
                    						

                    }


                    }

                    // InternalQuizDSL.g:1734:5: ( (lv_contents_3_0= ruleXmlContents ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==RULE_ID||LA32_0==32) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalQuizDSL.g:1735:6: (lv_contents_3_0= ruleXmlContents )
                    	    {
                    	    // InternalQuizDSL.g:1735:6: (lv_contents_3_0= ruleXmlContents )
                    	    // InternalQuizDSL.g:1736:7: lv_contents_3_0= ruleXmlContents
                    	    {

                    	    							newCompositeNode(grammarAccess.getXmlTagElementAccess().getContentsXmlContentsParserRuleCall_1_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_31);
                    	    lv_contents_3_0=ruleXmlContents();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getXmlTagElementRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"contents",
                    	    								lv_contents_3_0,
                    	    								"no.hal.quiz.xtext.QuizDSL.XmlContents");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    // InternalQuizDSL.g:1753:5: (otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )? )
                    // InternalQuizDSL.g:1754:6: otherlv_4= '/' ( (lv_endTag_5_0= RULE_ID ) )?
                    {
                    otherlv_4=(Token)match(input,17,FOLLOW_32); 

                    						newLeafNode(otherlv_4, grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_1_2_0());
                    					
                    // InternalQuizDSL.g:1758:6: ( (lv_endTag_5_0= RULE_ID ) )?
                    int alt33=2;
                    int LA33_0 = input.LA(1);

                    if ( (LA33_0==RULE_ID) ) {
                        alt33=1;
                    }
                    switch (alt33) {
                        case 1 :
                            // InternalQuizDSL.g:1759:7: (lv_endTag_5_0= RULE_ID )
                            {
                            // InternalQuizDSL.g:1759:7: (lv_endTag_5_0= RULE_ID )
                            // InternalQuizDSL.g:1760:8: lv_endTag_5_0= RULE_ID
                            {
                            lv_endTag_5_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                            								newLeafNode(lv_endTag_5_0, grammarAccess.getXmlTagElementAccess().getEndTagIDTerminalRuleCall_1_1_2_1_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getXmlTagElementRule());
                            								}
                            								setWithLastConsumed(
                            									current,
                            									"endTag",
                            									lv_endTag_5_0,
                            									"org.eclipse.xtext.common.Terminals.ID");
                            							

                            }


                            }
                            break;

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlTagElement"


    // $ANTLR start "entryRuleXmlTag"
    // InternalQuizDSL.g:1783:1: entryRuleXmlTag returns [EObject current=null] : iv_ruleXmlTag= ruleXmlTag EOF ;
    public final EObject entryRuleXmlTag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlTag = null;


        try {
            // InternalQuizDSL.g:1783:47: (iv_ruleXmlTag= ruleXmlTag EOF )
            // InternalQuizDSL.g:1784:2: iv_ruleXmlTag= ruleXmlTag EOF
            {
             newCompositeNode(grammarAccess.getXmlTagRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlTag=ruleXmlTag();

            state._fsp--;

             current =iv_ruleXmlTag; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlTag"


    // $ANTLR start "ruleXmlTag"
    // InternalQuizDSL.g:1790:1: ruleXmlTag returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributes_1_0= ruleXmlAttribute ) )* ) ;
    public final EObject ruleXmlTag() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_attributes_1_0 = null;



        	enterRule();

        try {
            // InternalQuizDSL.g:1796:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributes_1_0= ruleXmlAttribute ) )* ) )
            // InternalQuizDSL.g:1797:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributes_1_0= ruleXmlAttribute ) )* )
            {
            // InternalQuizDSL.g:1797:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributes_1_0= ruleXmlAttribute ) )* )
            // InternalQuizDSL.g:1798:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributes_1_0= ruleXmlAttribute ) )*
            {
            // InternalQuizDSL.g:1798:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalQuizDSL.g:1799:4: (lv_name_0_0= RULE_ID )
            {
            // InternalQuizDSL.g:1799:4: (lv_name_0_0= RULE_ID )
            // InternalQuizDSL.g:1800:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_32); 

            					newLeafNode(lv_name_0_0, grammarAccess.getXmlTagAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXmlTagRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalQuizDSL.g:1816:3: ( (lv_attributes_1_0= ruleXmlAttribute ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_ID) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalQuizDSL.g:1817:4: (lv_attributes_1_0= ruleXmlAttribute )
            	    {
            	    // InternalQuizDSL.g:1817:4: (lv_attributes_1_0= ruleXmlAttribute )
            	    // InternalQuizDSL.g:1818:5: lv_attributes_1_0= ruleXmlAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getXmlTagAccess().getAttributesXmlAttributeParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_32);
            	    lv_attributes_1_0=ruleXmlAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXmlTagRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_1_0,
            	    						"no.hal.quiz.xtext.QuizDSL.XmlAttribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlTag"


    // $ANTLR start "entryRuleXmlAttribute"
    // InternalQuizDSL.g:1839:1: entryRuleXmlAttribute returns [EObject current=null] : iv_ruleXmlAttribute= ruleXmlAttribute EOF ;
    public final EObject entryRuleXmlAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlAttribute = null;


        try {
            // InternalQuizDSL.g:1839:53: (iv_ruleXmlAttribute= ruleXmlAttribute EOF )
            // InternalQuizDSL.g:1840:2: iv_ruleXmlAttribute= ruleXmlAttribute EOF
            {
             newCompositeNode(grammarAccess.getXmlAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlAttribute=ruleXmlAttribute();

            state._fsp--;

             current =iv_ruleXmlAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlAttribute"


    // $ANTLR start "ruleXmlAttribute"
    // InternalQuizDSL.g:1846:1: ruleXmlAttribute returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleXmlAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;


        	enterRule();

        try {
            // InternalQuizDSL.g:1852:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) ) )
            // InternalQuizDSL.g:1853:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) )
            {
            // InternalQuizDSL.g:1853:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) )
            // InternalQuizDSL.g:1854:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) )
            {
            // InternalQuizDSL.g:1854:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalQuizDSL.g:1855:4: (lv_name_0_0= RULE_ID )
            {
            // InternalQuizDSL.g:1855:4: (lv_name_0_0= RULE_ID )
            // InternalQuizDSL.g:1856:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            					newLeafNode(lv_name_0_0, grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXmlAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,33,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalQuizDSL.g:1876:3: ( (lv_value_2_0= RULE_STRING ) )
            // InternalQuizDSL.g:1877:4: (lv_value_2_0= RULE_STRING )
            {
            // InternalQuizDSL.g:1877:4: (lv_value_2_0= RULE_STRING )
            // InternalQuizDSL.g:1878:5: lv_value_2_0= RULE_STRING
            {
            lv_value_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_2_0, grammarAccess.getXmlAttributeAccess().getValueSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXmlAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlAttribute"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000004000C032L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004012L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000040008032L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000040000030L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x000000005DFA0070L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000407A0070L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000000000D800002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000005DFA0072L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000021000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000020L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x00000000007A0050L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000020080L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000100020020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000000L});

}