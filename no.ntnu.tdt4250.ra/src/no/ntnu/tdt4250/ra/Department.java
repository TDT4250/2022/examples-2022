/**
 */
package no.ntnu.tdt4250.ra;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.ra.Department#getStaff <em>Staff</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Department#getCourses <em>Courses</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Department#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.ra.RaPackage#getDepartment()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='allStaffMustBeAllocated'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 allStaffMustBeAllocated='self.courses-&gt;collect(c | c.allocations)-&gt;collect(a | a.lecturers)-&gt;asSet()-&gt;includesAll(self.staff)'"
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Staff</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.ra.Person}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' containment reference list.
	 * @see no.ntnu.tdt4250.ra.RaPackage#getDepartment_Staff()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getStaff();

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.ra.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see no.ntnu.tdt4250.ra.RaPackage#getDepartment_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.ntnu.tdt4250.ra.RaPackage#getDepartment_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.ra.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model personRequired="true" courseRequired="true" percentageRequired="true"
	 * @generated
	 */
	void allocatePersonToCourse(Person person, Course course, double percentage);

} // Department
