/**
 */
package no.ntnu.tdt4250.ra;

import java.time.LocalDate;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getCode <em>Code</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getAllocations <em>Allocations</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getNumberOfLecturers <em>Number Of Lecturers</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getStartingDate <em>Starting Date</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='maxLecturers'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 maxLecturers='self.numberOfLecturers &lt; 5 and self.numberOfLecturers &gt; 0 '"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute list.
	 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse_Name()
	 * @model required="true" upper="2"
	 * @generated
	 */
	EList<String> getName();

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse_Code()
	 * @model required="true"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.ra.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Allocations</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.ra.Allocation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocations</em>' containment reference list.
	 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse_Allocations()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Allocation> getAllocations();

	/**
	 * Returns the value of the '<em><b>Number Of Lecturers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Lecturers</em>' attribute.
	 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse_NumberOfLecturers()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/acceleo/query/1.0 derivation='self.allocations-&gt;size()'"
	 * @generated
	 */
	int getNumberOfLecturers();

	/**
	 * Returns the value of the '<em><b>Starting Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starting Date</em>' attribute.
	 * @see #setStartingDate(LocalDate)
	 * @see no.ntnu.tdt4250.ra.RaPackage#getCourse_StartingDate()
	 * @model dataType="no.ntnu.tdt4250.ra.CourseDate"
	 * @generated
	 */
	LocalDate getStartingDate();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.ra.Course#getStartingDate <em>Starting Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starting Date</em>' attribute.
	 * @see #getStartingDate()
	 * @generated
	 */
	void setStartingDate(LocalDate value);

} // Course
