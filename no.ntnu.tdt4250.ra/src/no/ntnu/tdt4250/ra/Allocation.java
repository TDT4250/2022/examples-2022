/**
 */
package no.ntnu.tdt4250.ra;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.ra.Allocation#getLecturers <em>Lecturers</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.Allocation#getPercentage <em>Percentage</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.ra.RaPackage#getAllocation()
 * @model
 * @generated
 */
public interface Allocation extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecturers</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecturers</em>' reference.
	 * @see #setLecturers(Person)
	 * @see no.ntnu.tdt4250.ra.RaPackage#getAllocation_Lecturers()
	 * @model required="true"
	 * @generated
	 */
	Person getLecturers();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.ra.Allocation#getLecturers <em>Lecturers</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecturers</em>' reference.
	 * @see #getLecturers()
	 * @generated
	 */
	void setLecturers(Person value);

	/**
	 * Returns the value of the '<em><b>Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Percentage</em>' attribute.
	 * @see #setPercentage(double)
	 * @see no.ntnu.tdt4250.ra.RaPackage#getAllocation_Percentage()
	 * @model required="true"
	 * @generated
	 */
	double getPercentage();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.ra.Allocation#getPercentage <em>Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Percentage</em>' attribute.
	 * @see #getPercentage()
	 * @generated
	 */
	void setPercentage(double value);

} // Allocation
