/**
 */
package no.ntnu.tdt4250.ra.impl;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import no.ntnu.tdt4250.ra.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RaFactoryImpl extends EFactoryImpl implements RaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RaFactory init() {
		try {
			RaFactory theRaFactory = (RaFactory)EPackage.Registry.INSTANCE.getEFactory(RaPackage.eNS_URI);
			if (theRaFactory != null) {
				return theRaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RaPackage.COURSE: return createCourse();
			case RaPackage.PERSON: return createPerson();
			case RaPackage.DEPARTMENT: return createDepartment();
			case RaPackage.ALLOCATION: return createAllocation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RaPackage.COURSE_DATE:
				return createCourseDateFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RaPackage.COURSE_DATE:
				return convertCourseDateToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department createDepartment() {
		DepartmentImpl department = new DepartmentImpl();
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Allocation createAllocation() {
		AllocationImpl allocation = new AllocationImpl();
		return allocation;
	}


	/**
	 * <!-- begin-user-doc -->
	 * This method converts String values form the CourseDate data type to instances
	 * of the LocalDate java class. This can be used when serializing the value as the
	 * rest of the Ecore object, but also then a value is read from e.g., text field.
	 * 
	 * NOTE HOWEVER THAT THIS CUSTOM DATATYPES WORK WELL ONLY IN THE RUNTIME ENVIRONMENT
	 * (I.E., THE SECOND INSTANCE OF ECLIPSE) 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LocalDate createCourseDateFromString(EDataType eDataType, String initialValue) {
		
		LocalDate date = LocalDate.now();
		
		try {
			date = LocalDate.parse(initialValue);
		} catch (DateTimeParseException e) {
			// In case the date is not valid, we set the current date as default
			date = LocalDate.now();
		}
		
		return date;
	}


	/**
	 * <!-- begin-user-doc -->
	 * This method converts instances of the LocalDate java class to String, 
	 * for them to be serialized as the rest of the Ecore model, or be displayed
	 * when a String is required.
	 * 
	 * The generated implementation is usually ok here, as any class typically implements
	 * a toString method that can be called for this purpose.
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCourseDateToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaPackage getRaPackage() {
		return (RaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RaPackage getPackage() {
		return RaPackage.eINSTANCE;
	}

} //RaFactoryImpl
