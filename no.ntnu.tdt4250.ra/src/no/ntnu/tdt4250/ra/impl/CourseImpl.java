/**
 */
package no.ntnu.tdt4250.ra.impl;

import java.time.LocalDate;
import java.util.Collection;
import no.ntnu.tdt4250.ra.Allocation;
import no.ntnu.tdt4250.ra.Course;
import no.ntnu.tdt4250.ra.RaPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.ra.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.impl.CourseImpl#getAllocations <em>Allocations</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.impl.CourseImpl#getNumberOfLecturers <em>Number Of Lecturers</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.ra.impl.CourseImpl#getStartingDate <em>Starting Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected EList<String> name;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAllocations() <em>Allocations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllocations()
	 * @generated
	 * @ordered
	 */
	protected EList<Allocation> allocations;

	/**
	 * The cached setting delegate for the '{@link #getNumberOfLecturers() <em>Number Of Lecturers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfLecturers()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate NUMBER_OF_LECTURERS__ESETTING_DELEGATE = ((EStructuralFeature.Internal)RaPackage.Literals.COURSE__NUMBER_OF_LECTURERS).getSettingDelegate();

	/**
	 * The default value of the '{@link #getStartingDate() <em>Starting Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingDate()
	 * @generated
	 * @ordered
	 */
	protected static final LocalDate STARTING_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartingDate() <em>Starting Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingDate()
	 * @generated
	 * @ordered
	 */
	protected LocalDate startingDate = STARTING_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RaPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getName() {
		if (name == null) {
			name = new EDataTypeUniqueEList<String>(String.class, this, RaPackage.COURSE__NAME);
		}
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RaPackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Allocation> getAllocations() {
		if (allocations == null) {
			allocations = new EObjectContainmentEList<Allocation>(Allocation.class, this, RaPackage.COURSE__ALLOCATIONS);
		}
		return allocations;
	}

/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfLecturers() {
		return (Integer)NUMBER_OF_LECTURERS__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated NOT
//	 */
//	public int getNumberOfLecturers() {
//		// TODO: implement this method to return the 'Number Of Lecturers' attribute
//		// Ensure that you remove @generated or mark it @generated NOT
//		
//		// Number of lecturers is the number of allocations, assuming
//		// no duplicates
//		
//		int value = 0;
//		
//		if(this.allocations != null) {
//			value = this.allocations.size();
//		}
//		
//		return value;
//	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalDate getStartingDate() {
		return startingDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartingDate(LocalDate newStartingDate) {
		LocalDate oldStartingDate = startingDate;
		startingDate = newStartingDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RaPackage.COURSE__STARTING_DATE, oldStartingDate, startingDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RaPackage.COURSE__ALLOCATIONS:
				return ((InternalEList<?>)getAllocations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RaPackage.COURSE__NAME:
				return getName();
			case RaPackage.COURSE__CODE:
				return getCode();
			case RaPackage.COURSE__ALLOCATIONS:
				return getAllocations();
			case RaPackage.COURSE__NUMBER_OF_LECTURERS:
				return getNumberOfLecturers();
			case RaPackage.COURSE__STARTING_DATE:
				return getStartingDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RaPackage.COURSE__NAME:
				getName().clear();
				getName().addAll((Collection<? extends String>)newValue);
				return;
			case RaPackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case RaPackage.COURSE__ALLOCATIONS:
				getAllocations().clear();
				getAllocations().addAll((Collection<? extends Allocation>)newValue);
				return;
			case RaPackage.COURSE__STARTING_DATE:
				setStartingDate((LocalDate)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RaPackage.COURSE__NAME:
				getName().clear();
				return;
			case RaPackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case RaPackage.COURSE__ALLOCATIONS:
				getAllocations().clear();
				return;
			case RaPackage.COURSE__STARTING_DATE:
				setStartingDate(STARTING_DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RaPackage.COURSE__NAME:
				return name != null && !name.isEmpty();
			case RaPackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case RaPackage.COURSE__ALLOCATIONS:
				return allocations != null && !allocations.isEmpty();
			case RaPackage.COURSE__NUMBER_OF_LECTURERS:
				return NUMBER_OF_LECTURERS__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case RaPackage.COURSE__STARTING_DATE:
				return STARTING_DATE_EDEFAULT == null ? startingDate != null : !STARTING_DATE_EDEFAULT.equals(startingDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", code: ");
		result.append(code);
		result.append(", startingDate: ");
		result.append(startingDate);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
