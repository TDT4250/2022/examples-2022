package no.ntnu.tdt4250.ra.examples;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emfcloud.jackson.resource.JsonResourceFactory;

import no.ntnu.tdt4250.ra.Allocation;
import no.ntnu.tdt4250.ra.Course;
import no.ntnu.tdt4250.ra.Department;
import no.ntnu.tdt4250.ra.Person;
import no.ntnu.tdt4250.ra.RaFactory;
import no.ntnu.tdt4250.ra.RaPackage;

public class EcoreProgrammingExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		RaFactory factory = RaFactory.eINSTANCE;

		// Create the Course instance
		Course c = factory.createCourse();
		c.setCode("TDT4250");
		c.getName().add("Advanced Software Design");
				
		// Create the Person instance
		Person leo = factory.createPerson();
		leo.setFirstName("Leonardo");
		leo.setLastName("Montecchi");
		
		// Create the Person instance
		Person j = factory.createPerson();
		j.setFirstName("John");

		// Create the Allocation instance
		Allocation a = factory.createAllocation();
		a.setLecturers(leo); // Set Person in the Allocation
		a.setPercentage(0.5);
		
		// Create the Allocation instance
		Allocation a2 = factory.createAllocation();
		a.setLecturers(j); // Set Person in the Allocation
		a.setPercentage(0.5);
		
		// Add allocation to the course
		c.getAllocations().add(a);
		c.getAllocations().add(a2);
		
		// Create Department
		Department dep = factory.createDepartment();
		dep.getCourses().add(c);
		dep.getStaff().add(leo);
		dep.getStaff().add(j);
		
		System.out.println(c);
//		
//		System.out.println("Lecturers: " + c.getNumberOfLecturers());
//				
		
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(RaPackage.eNS_URI, RaPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		
		// Replace this with full path on your machine, or use the relative path (commented below)
		String folder = "/home/montex/Lavoro/NTNU/Teaching/TDT4250/examples/examples2022/no.ntnu.tdt4250.ra.examples/src/no/ntnu/tdt4250/ra/examples/";
		
		// String folder = URI.createFileURI(".").toString();
		// Using the relative path as above, the file will be created in the *bin* folder
		// inside the project (you can search for it with the file explorer)
		Resource resource = rs.createResource(URI.createFileURI(folder + "newmodel.xmi"));
		
		resource.getContents().add(dep);
		
		try {
			resource.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Resource jsonResource = new JsonResourceFactory().createResource(URI.createFileURI(folder + "newmodel.raj"));
		jsonResource.getContents().add(dep);
		
		try {
			jsonResource.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
