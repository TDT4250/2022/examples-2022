package no.ntnu.tdt4250.ra.examples

import no.ntnu.tdt4250.ra.RaPackage
import no.ntnu.tdt4250.ra.Department
import no.ntnu.tdt4250.ra.RaFactory
import no.ntnu.tdt4250.ra.Person
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.common.util.URI
import java.lang.invoke.LambdaConversionException
import com.fasterxml.jackson.databind.deser.impl.CreatorCandidate.Param

class TransformationExample {
	
	extension static RaFactory factory = RaFactory.eINSTANCE
	
	def static void main(String[] args) {
		val rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(RaPackage.eNS_URI, RaPackage.eINSTANCE)
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl())
		
		// Replace this with full path on your machine, or use the relative path (commented below)
		val srcFolder = "/home/montex/Lavoro/NTNU/Teaching/TDT4250/examples/examples2022/no.ntnu.tdt4250.ra/model/"
		val dstFolder = "/home/montex/Lavoro/NTNU/Teaching/TDT4250/examples/examples2022/no.ntnu.tdt4250.ra.examples/src/no/ntnu/tdt4250/ra/examples/transformed/"

		// String folder = URI.createFileURI(".").toString();
		// Using the relative path as above, the file will be created in the *bin* folder
		// inside the project (you can search for it with the file explorer)
		
		// Loading base IDI model
		val resIDI = rs.createResource(URI.createFileURI(srcFolder + "IDI.xmi"))
		resIDI.load(null)
		var depIDI = resIDI.contents.get(0) as Department

		// Transformation example: Produce a view only of courses starting with TDT42
		val resource1 = rs.createResource(URI.createFileURI(dstFolder + "IDI-viewTDT42.xmi"))
		val depFiltered = depIDI.viewByCourseCode("TDT42")
		resource1.contents += depFiltered
		resource1.save(null)
		
		// Transformation example: Produce a view with only staff that is allocated to some course
		val resource2 = rs.createResource(URI.createFileURI(dstFolder + "IDI-allocated-only.xmi"))
		resource2.contents += depIDI.removeUnallocatedStaff
		resource2.save(null)
		
		// Transformation example: Move all the course allocations from Leonardo to Donald
		val resource3 = rs.createResource(URI.createFileURI(dstFolder + "IDI-delegated.xmi"))
		val leonardo = depIDI.staff.findFirst[firstName=="Leonardo" lastName=="Montecchi"]
		// This calls the "createPerson" method from the RaFactory extension
		val donald = createPerson => [
			firstName = "Donald"
			lastName = "Knuth"
		]
		resource3.contents += depIDI.delegateCourses(leonardo, donald)
		resource3.save(null)
	}
	
	def static viewByCourseCode(Department dep, String codePrefix) {
		
		val depCopy = EcoreUtil.copy(dep)
		
		depCopy.courses.removeIf(
			[s | !s.code.startsWith(codePrefix)]
		)
		
		depCopy.removeUnallocatedStaff
				
		depCopy;
	}
	
	
	def static removeUnallocatedStaff(Department dep) {
		dep.staff.removeIf(
			[s | dep.courses.
				filter[allocations.
					filter[lecturers == s].size > 0
				].isEmpty
			]
		)
		
		dep
	}
	
	def static delegateCourses(Department dep, Person from, Person to) {
		if (!dep.staff.contains(to)) {
			dep.staff += to
		}
	
		dep.courses.flatMap[
			allocations
		].filter[lecturers == from].forEach[
			lecturers = to 
			percentage = 0.2
		]
		
		dep
		
	}
}