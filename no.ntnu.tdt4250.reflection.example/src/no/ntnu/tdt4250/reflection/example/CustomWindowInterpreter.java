package no.ntnu.tdt4250.reflection.example;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CustomWindowInterpreter {

	private static final String UI_BASE_PACKAGE = "javax.swing.";
	private static final String LABEL_ATTRIBUTE = "title";
	
	public static void main(String[] args) {
		
		String baseFolder = "/home/montex/Lavoro/NTNU/Teaching/TDT4250/examples/examples2022/";
		String filePath = "no.ntnu.tdt4250.reflection.example/tdt4250-window.xml";
				
		File xmlFile = new File(baseFolder + "/" + filePath);
		
		try {
			renderWindow(xmlFile);
		} catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void renderWindow(File resource) 
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
	//Exceptions should be probably handled better
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbFactory.newDocumentBuilder();
		
		Document doc  = db.parse(resource);
		
		Element root = doc.getDocumentElement();
			
		Container rootComponent = instantiateComponent(root);
		
		processChildren(root, rootComponent);
		
		rootComponent.setLayout(new FlowLayout());
		rootComponent.setMinimumSize(new Dimension(300,200));
		rootComponent.setVisible(true);
		
	}
	
	private static Container instantiateComponent(Element el) 
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
			//Exceptions should be probably handled better
	{
		Class<?> theClass = Class.forName(UI_BASE_PACKAGE + el.getNodeName());
		Constructor<?> constructor;
		
		String label = el.getAttribute(LABEL_ATTRIBUTE);
		Container newComponent = null;
		
		if(label != "") {
			constructor = theClass.getConstructor(String.class);
			newComponent = (Container)constructor.newInstance(label);
		}else {
			constructor = theClass.getConstructor();
			newComponent = (Container)constructor.newInstance();
		}
		
		return newComponent;
	}
	
	private static void processChildren(Element xmlElement, Container uiComponent) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		NodeList children = xmlElement.getChildNodes();
		Node temp;
		for (int i = 0; i < children.getLength(); i++) {
			
			temp = children.item(i);
			if(temp.getNodeType() == Node.ELEMENT_NODE) {
								
				Container childComponent = instantiateComponent((Element)temp);
				uiComponent.add(childComponent);
				
				processChildren((Element)temp, childComponent);
			}
		}
	}
}
