package no.ntnu.idi.tdt4250.sm.xtext.examples;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import no.ntnu.idi.tdt4250.sm.xtext.StateMachineDSLStandaloneSetup;

public class XtextConversionExample {

	public static void main(String[] args) {
		
		StateMachineDSLStandaloneSetup.doSetup();
		
		ResourceSet rs = new ResourceSetImpl();
		
		String folder = "/home/montex/Lavoro/NTNU/Teaching/TDT4250/examples/examples2022/no.ntnu.idi.tdt4250.sm.xtext.examples/";
		Resource r1 = rs.getResource(URI.createFileURI(folder + "CoffeeMachine.xmi"), true);
		Resource r2 = rs.createResource(URI.createFileURI(folder + "CoffeeMachine.machine"));

		EObject root = r1.getContents().get(0);
		
		r2.getContents().add(root);
		
		try {
			r2.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
