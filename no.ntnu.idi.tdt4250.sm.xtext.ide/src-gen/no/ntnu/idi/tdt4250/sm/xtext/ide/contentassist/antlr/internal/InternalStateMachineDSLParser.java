package no.ntnu.idi.tdt4250.sm.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import no.ntnu.idi.tdt4250.sm.xtext.services.StateMachineDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateMachineDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'s'", "'->'", "'['", "']'", "'init'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalStateMachineDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateMachineDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateMachineDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateMachineDSL.g"; }


    	private StateMachineDSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(StateMachineDSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStateMachine"
    // InternalStateMachineDSL.g:53:1: entryRuleStateMachine : ruleStateMachine EOF ;
    public final void entryRuleStateMachine() throws RecognitionException {
        try {
            // InternalStateMachineDSL.g:54:1: ( ruleStateMachine EOF )
            // InternalStateMachineDSL.g:55:1: ruleStateMachine EOF
            {
             before(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getStateMachineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalStateMachineDSL.g:62:1: ruleStateMachine : ( ( rule__StateMachine__Group__0 ) ) ;
    public final void ruleStateMachine() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:66:2: ( ( ( rule__StateMachine__Group__0 ) ) )
            // InternalStateMachineDSL.g:67:2: ( ( rule__StateMachine__Group__0 ) )
            {
            // InternalStateMachineDSL.g:67:2: ( ( rule__StateMachine__Group__0 ) )
            // InternalStateMachineDSL.g:68:3: ( rule__StateMachine__Group__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup()); 
            // InternalStateMachineDSL.g:69:3: ( rule__StateMachine__Group__0 )
            // InternalStateMachineDSL.g:69:4: rule__StateMachine__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalStateMachineDSL.g:78:1: entryRuleState : ruleState EOF ;
    public final void entryRuleState() throws RecognitionException {
        try {
            // InternalStateMachineDSL.g:79:1: ( ruleState EOF )
            // InternalStateMachineDSL.g:80:1: ruleState EOF
            {
             before(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateMachineDSL.g:87:1: ruleState : ( ( rule__State__Group__0 ) ) ;
    public final void ruleState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:91:2: ( ( ( rule__State__Group__0 ) ) )
            // InternalStateMachineDSL.g:92:2: ( ( rule__State__Group__0 ) )
            {
            // InternalStateMachineDSL.g:92:2: ( ( rule__State__Group__0 ) )
            // InternalStateMachineDSL.g:93:3: ( rule__State__Group__0 )
            {
             before(grammarAccess.getStateAccess().getGroup()); 
            // InternalStateMachineDSL.g:94:3: ( rule__State__Group__0 )
            // InternalStateMachineDSL.g:94:4: rule__State__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalStateMachineDSL.g:103:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalStateMachineDSL.g:104:1: ( ruleTransition EOF )
            // InternalStateMachineDSL.g:105:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateMachineDSL.g:112:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:116:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalStateMachineDSL.g:117:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalStateMachineDSL.g:117:2: ( ( rule__Transition__Group__0 ) )
            // InternalStateMachineDSL.g:118:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalStateMachineDSL.g:119:3: ( rule__Transition__Group__0 )
            // InternalStateMachineDSL.g:119:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "rule__StateMachine__Group__0"
    // InternalStateMachineDSL.g:127:1: rule__StateMachine__Group__0 : rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 ;
    public final void rule__StateMachine__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:131:1: ( rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 )
            // InternalStateMachineDSL.g:132:2: rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__StateMachine__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0"


    // $ANTLR start "rule__StateMachine__Group__0__Impl"
    // InternalStateMachineDSL.g:139:1: rule__StateMachine__Group__0__Impl : ( ( rule__StateMachine__NameAssignment_0 ) ) ;
    public final void rule__StateMachine__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:143:1: ( ( ( rule__StateMachine__NameAssignment_0 ) ) )
            // InternalStateMachineDSL.g:144:1: ( ( rule__StateMachine__NameAssignment_0 ) )
            {
            // InternalStateMachineDSL.g:144:1: ( ( rule__StateMachine__NameAssignment_0 ) )
            // InternalStateMachineDSL.g:145:2: ( rule__StateMachine__NameAssignment_0 )
            {
             before(grammarAccess.getStateMachineAccess().getNameAssignment_0()); 
            // InternalStateMachineDSL.g:146:2: ( rule__StateMachine__NameAssignment_0 )
            // InternalStateMachineDSL.g:146:3: rule__StateMachine__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0__Impl"


    // $ANTLR start "rule__StateMachine__Group__1"
    // InternalStateMachineDSL.g:154:1: rule__StateMachine__Group__1 : rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 ;
    public final void rule__StateMachine__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:158:1: ( rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 )
            // InternalStateMachineDSL.g:159:2: rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1"


    // $ANTLR start "rule__StateMachine__Group__1__Impl"
    // InternalStateMachineDSL.g:166:1: rule__StateMachine__Group__1__Impl : ( ( ( rule__StateMachine__StatesAssignment_1 ) ) ( ( rule__StateMachine__StatesAssignment_1 )* ) ) ;
    public final void rule__StateMachine__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:170:1: ( ( ( ( rule__StateMachine__StatesAssignment_1 ) ) ( ( rule__StateMachine__StatesAssignment_1 )* ) ) )
            // InternalStateMachineDSL.g:171:1: ( ( ( rule__StateMachine__StatesAssignment_1 ) ) ( ( rule__StateMachine__StatesAssignment_1 )* ) )
            {
            // InternalStateMachineDSL.g:171:1: ( ( ( rule__StateMachine__StatesAssignment_1 ) ) ( ( rule__StateMachine__StatesAssignment_1 )* ) )
            // InternalStateMachineDSL.g:172:2: ( ( rule__StateMachine__StatesAssignment_1 ) ) ( ( rule__StateMachine__StatesAssignment_1 )* )
            {
            // InternalStateMachineDSL.g:172:2: ( ( rule__StateMachine__StatesAssignment_1 ) )
            // InternalStateMachineDSL.g:173:3: ( rule__StateMachine__StatesAssignment_1 )
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment_1()); 
            // InternalStateMachineDSL.g:174:3: ( rule__StateMachine__StatesAssignment_1 )
            // InternalStateMachineDSL.g:174:4: rule__StateMachine__StatesAssignment_1
            {
            pushFollow(FOLLOW_5);
            rule__StateMachine__StatesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getStatesAssignment_1()); 

            }

            // InternalStateMachineDSL.g:177:2: ( ( rule__StateMachine__StatesAssignment_1 )* )
            // InternalStateMachineDSL.g:178:3: ( rule__StateMachine__StatesAssignment_1 )*
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment_1()); 
            // InternalStateMachineDSL.g:179:3: ( rule__StateMachine__StatesAssignment_1 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalStateMachineDSL.g:179:4: rule__StateMachine__StatesAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__StateMachine__StatesAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getStatesAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1__Impl"


    // $ANTLR start "rule__StateMachine__Group__2"
    // InternalStateMachineDSL.g:188:1: rule__StateMachine__Group__2 : rule__StateMachine__Group__2__Impl ;
    public final void rule__StateMachine__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:192:1: ( rule__StateMachine__Group__2__Impl )
            // InternalStateMachineDSL.g:193:2: rule__StateMachine__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2"


    // $ANTLR start "rule__StateMachine__Group__2__Impl"
    // InternalStateMachineDSL.g:199:1: rule__StateMachine__Group__2__Impl : ( ( rule__StateMachine__TransitionsAssignment_2 )* ) ;
    public final void rule__StateMachine__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:203:1: ( ( ( rule__StateMachine__TransitionsAssignment_2 )* ) )
            // InternalStateMachineDSL.g:204:1: ( ( rule__StateMachine__TransitionsAssignment_2 )* )
            {
            // InternalStateMachineDSL.g:204:1: ( ( rule__StateMachine__TransitionsAssignment_2 )* )
            // InternalStateMachineDSL.g:205:2: ( rule__StateMachine__TransitionsAssignment_2 )*
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsAssignment_2()); 
            // InternalStateMachineDSL.g:206:2: ( rule__StateMachine__TransitionsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalStateMachineDSL.g:206:3: rule__StateMachine__TransitionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__StateMachine__TransitionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getTransitionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2__Impl"


    // $ANTLR start "rule__State__Group__0"
    // InternalStateMachineDSL.g:215:1: rule__State__Group__0 : rule__State__Group__0__Impl rule__State__Group__1 ;
    public final void rule__State__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:219:1: ( rule__State__Group__0__Impl rule__State__Group__1 )
            // InternalStateMachineDSL.g:220:2: rule__State__Group__0__Impl rule__State__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__State__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0"


    // $ANTLR start "rule__State__Group__0__Impl"
    // InternalStateMachineDSL.g:227:1: rule__State__Group__0__Impl : ( 's' ) ;
    public final void rule__State__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:231:1: ( ( 's' ) )
            // InternalStateMachineDSL.g:232:1: ( 's' )
            {
            // InternalStateMachineDSL.g:232:1: ( 's' )
            // InternalStateMachineDSL.g:233:2: 's'
            {
             before(grammarAccess.getStateAccess().getSKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getSKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0__Impl"


    // $ANTLR start "rule__State__Group__1"
    // InternalStateMachineDSL.g:242:1: rule__State__Group__1 : rule__State__Group__1__Impl rule__State__Group__2 ;
    public final void rule__State__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:246:1: ( rule__State__Group__1__Impl rule__State__Group__2 )
            // InternalStateMachineDSL.g:247:2: rule__State__Group__1__Impl rule__State__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__State__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1"


    // $ANTLR start "rule__State__Group__1__Impl"
    // InternalStateMachineDSL.g:254:1: rule__State__Group__1__Impl : ( ( rule__State__NameAssignment_1 ) ) ;
    public final void rule__State__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:258:1: ( ( ( rule__State__NameAssignment_1 ) ) )
            // InternalStateMachineDSL.g:259:1: ( ( rule__State__NameAssignment_1 ) )
            {
            // InternalStateMachineDSL.g:259:1: ( ( rule__State__NameAssignment_1 ) )
            // InternalStateMachineDSL.g:260:2: ( rule__State__NameAssignment_1 )
            {
             before(grammarAccess.getStateAccess().getNameAssignment_1()); 
            // InternalStateMachineDSL.g:261:2: ( rule__State__NameAssignment_1 )
            // InternalStateMachineDSL.g:261:3: rule__State__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__State__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1__Impl"


    // $ANTLR start "rule__State__Group__2"
    // InternalStateMachineDSL.g:269:1: rule__State__Group__2 : rule__State__Group__2__Impl ;
    public final void rule__State__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:273:1: ( rule__State__Group__2__Impl )
            // InternalStateMachineDSL.g:274:2: rule__State__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2"


    // $ANTLR start "rule__State__Group__2__Impl"
    // InternalStateMachineDSL.g:280:1: rule__State__Group__2__Impl : ( ( rule__State__InitialAssignment_2 )? ) ;
    public final void rule__State__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:284:1: ( ( ( rule__State__InitialAssignment_2 )? ) )
            // InternalStateMachineDSL.g:285:1: ( ( rule__State__InitialAssignment_2 )? )
            {
            // InternalStateMachineDSL.g:285:1: ( ( rule__State__InitialAssignment_2 )? )
            // InternalStateMachineDSL.g:286:2: ( rule__State__InitialAssignment_2 )?
            {
             before(grammarAccess.getStateAccess().getInitialAssignment_2()); 
            // InternalStateMachineDSL.g:287:2: ( rule__State__InitialAssignment_2 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalStateMachineDSL.g:287:3: rule__State__InitialAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__State__InitialAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStateAccess().getInitialAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalStateMachineDSL.g:296:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:300:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalStateMachineDSL.g:301:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalStateMachineDSL.g:308:1: rule__Transition__Group__0__Impl : ( ( rule__Transition__SourceAssignment_0 ) ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:312:1: ( ( ( rule__Transition__SourceAssignment_0 ) ) )
            // InternalStateMachineDSL.g:313:1: ( ( rule__Transition__SourceAssignment_0 ) )
            {
            // InternalStateMachineDSL.g:313:1: ( ( rule__Transition__SourceAssignment_0 ) )
            // InternalStateMachineDSL.g:314:2: ( rule__Transition__SourceAssignment_0 )
            {
             before(grammarAccess.getTransitionAccess().getSourceAssignment_0()); 
            // InternalStateMachineDSL.g:315:2: ( rule__Transition__SourceAssignment_0 )
            // InternalStateMachineDSL.g:315:3: rule__Transition__SourceAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__SourceAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getSourceAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalStateMachineDSL.g:323:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:327:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalStateMachineDSL.g:328:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalStateMachineDSL.g:335:1: rule__Transition__Group__1__Impl : ( ( rule__Transition__Group_1__0 )? ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:339:1: ( ( ( rule__Transition__Group_1__0 )? ) )
            // InternalStateMachineDSL.g:340:1: ( ( rule__Transition__Group_1__0 )? )
            {
            // InternalStateMachineDSL.g:340:1: ( ( rule__Transition__Group_1__0 )? )
            // InternalStateMachineDSL.g:341:2: ( rule__Transition__Group_1__0 )?
            {
             before(grammarAccess.getTransitionAccess().getGroup_1()); 
            // InternalStateMachineDSL.g:342:2: ( rule__Transition__Group_1__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalStateMachineDSL.g:342:3: rule__Transition__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transition__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalStateMachineDSL.g:350:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:354:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // InternalStateMachineDSL.g:355:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalStateMachineDSL.g:362:1: rule__Transition__Group__2__Impl : ( '->' ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:366:1: ( ( '->' ) )
            // InternalStateMachineDSL.g:367:1: ( '->' )
            {
            // InternalStateMachineDSL.g:367:1: ( '->' )
            // InternalStateMachineDSL.g:368:2: '->'
            {
             before(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // InternalStateMachineDSL.g:377:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:381:1: ( rule__Transition__Group__3__Impl )
            // InternalStateMachineDSL.g:382:2: rule__Transition__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // InternalStateMachineDSL.g:388:1: rule__Transition__Group__3__Impl : ( ( rule__Transition__TargetAssignment_3 ) ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:392:1: ( ( ( rule__Transition__TargetAssignment_3 ) ) )
            // InternalStateMachineDSL.g:393:1: ( ( rule__Transition__TargetAssignment_3 ) )
            {
            // InternalStateMachineDSL.g:393:1: ( ( rule__Transition__TargetAssignment_3 ) )
            // InternalStateMachineDSL.g:394:2: ( rule__Transition__TargetAssignment_3 )
            {
             before(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 
            // InternalStateMachineDSL.g:395:2: ( rule__Transition__TargetAssignment_3 )
            // InternalStateMachineDSL.g:395:3: rule__Transition__TargetAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Transition__TargetAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group_1__0"
    // InternalStateMachineDSL.g:404:1: rule__Transition__Group_1__0 : rule__Transition__Group_1__0__Impl rule__Transition__Group_1__1 ;
    public final void rule__Transition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:408:1: ( rule__Transition__Group_1__0__Impl rule__Transition__Group_1__1 )
            // InternalStateMachineDSL.g:409:2: rule__Transition__Group_1__0__Impl rule__Transition__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__Transition__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__0"


    // $ANTLR start "rule__Transition__Group_1__0__Impl"
    // InternalStateMachineDSL.g:416:1: rule__Transition__Group_1__0__Impl : ( '[' ) ;
    public final void rule__Transition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:420:1: ( ( '[' ) )
            // InternalStateMachineDSL.g:421:1: ( '[' )
            {
            // InternalStateMachineDSL.g:421:1: ( '[' )
            // InternalStateMachineDSL.g:422:2: '['
            {
             before(grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_1_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__0__Impl"


    // $ANTLR start "rule__Transition__Group_1__1"
    // InternalStateMachineDSL.g:431:1: rule__Transition__Group_1__1 : rule__Transition__Group_1__1__Impl rule__Transition__Group_1__2 ;
    public final void rule__Transition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:435:1: ( rule__Transition__Group_1__1__Impl rule__Transition__Group_1__2 )
            // InternalStateMachineDSL.g:436:2: rule__Transition__Group_1__1__Impl rule__Transition__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Transition__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__1"


    // $ANTLR start "rule__Transition__Group_1__1__Impl"
    // InternalStateMachineDSL.g:443:1: rule__Transition__Group_1__1__Impl : ( ( rule__Transition__InputAssignment_1_1 ) ) ;
    public final void rule__Transition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:447:1: ( ( ( rule__Transition__InputAssignment_1_1 ) ) )
            // InternalStateMachineDSL.g:448:1: ( ( rule__Transition__InputAssignment_1_1 ) )
            {
            // InternalStateMachineDSL.g:448:1: ( ( rule__Transition__InputAssignment_1_1 ) )
            // InternalStateMachineDSL.g:449:2: ( rule__Transition__InputAssignment_1_1 )
            {
             before(grammarAccess.getTransitionAccess().getInputAssignment_1_1()); 
            // InternalStateMachineDSL.g:450:2: ( rule__Transition__InputAssignment_1_1 )
            // InternalStateMachineDSL.g:450:3: rule__Transition__InputAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__InputAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getInputAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__1__Impl"


    // $ANTLR start "rule__Transition__Group_1__2"
    // InternalStateMachineDSL.g:458:1: rule__Transition__Group_1__2 : rule__Transition__Group_1__2__Impl ;
    public final void rule__Transition__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:462:1: ( rule__Transition__Group_1__2__Impl )
            // InternalStateMachineDSL.g:463:2: rule__Transition__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__2"


    // $ANTLR start "rule__Transition__Group_1__2__Impl"
    // InternalStateMachineDSL.g:469:1: rule__Transition__Group_1__2__Impl : ( ']' ) ;
    public final void rule__Transition__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:473:1: ( ( ']' ) )
            // InternalStateMachineDSL.g:474:1: ( ']' )
            {
            // InternalStateMachineDSL.g:474:1: ( ']' )
            // InternalStateMachineDSL.g:475:2: ']'
            {
             before(grammarAccess.getTransitionAccess().getRightSquareBracketKeyword_1_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getRightSquareBracketKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_1__2__Impl"


    // $ANTLR start "rule__StateMachine__NameAssignment_0"
    // InternalStateMachineDSL.g:485:1: rule__StateMachine__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__StateMachine__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:489:1: ( ( RULE_ID ) )
            // InternalStateMachineDSL.g:490:2: ( RULE_ID )
            {
            // InternalStateMachineDSL.g:490:2: ( RULE_ID )
            // InternalStateMachineDSL.g:491:3: RULE_ID
            {
             before(grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__NameAssignment_0"


    // $ANTLR start "rule__StateMachine__StatesAssignment_1"
    // InternalStateMachineDSL.g:500:1: rule__StateMachine__StatesAssignment_1 : ( ruleState ) ;
    public final void rule__StateMachine__StatesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:504:1: ( ( ruleState ) )
            // InternalStateMachineDSL.g:505:2: ( ruleState )
            {
            // InternalStateMachineDSL.g:505:2: ( ruleState )
            // InternalStateMachineDSL.g:506:3: ruleState
            {
             before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StatesAssignment_1"


    // $ANTLR start "rule__StateMachine__TransitionsAssignment_2"
    // InternalStateMachineDSL.g:515:1: rule__StateMachine__TransitionsAssignment_2 : ( ruleTransition ) ;
    public final void rule__StateMachine__TransitionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:519:1: ( ( ruleTransition ) )
            // InternalStateMachineDSL.g:520:2: ( ruleTransition )
            {
            // InternalStateMachineDSL.g:520:2: ( ruleTransition )
            // InternalStateMachineDSL.g:521:3: ruleTransition
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__TransitionsAssignment_2"


    // $ANTLR start "rule__State__NameAssignment_1"
    // InternalStateMachineDSL.g:530:1: rule__State__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__State__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:534:1: ( ( RULE_ID ) )
            // InternalStateMachineDSL.g:535:2: ( RULE_ID )
            {
            // InternalStateMachineDSL.g:535:2: ( RULE_ID )
            // InternalStateMachineDSL.g:536:3: RULE_ID
            {
             before(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__NameAssignment_1"


    // $ANTLR start "rule__State__InitialAssignment_2"
    // InternalStateMachineDSL.g:545:1: rule__State__InitialAssignment_2 : ( ( 'init' ) ) ;
    public final void rule__State__InitialAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:549:1: ( ( ( 'init' ) ) )
            // InternalStateMachineDSL.g:550:2: ( ( 'init' ) )
            {
            // InternalStateMachineDSL.g:550:2: ( ( 'init' ) )
            // InternalStateMachineDSL.g:551:3: ( 'init' )
            {
             before(grammarAccess.getStateAccess().getInitialInitKeyword_2_0()); 
            // InternalStateMachineDSL.g:552:3: ( 'init' )
            // InternalStateMachineDSL.g:553:4: 'init'
            {
             before(grammarAccess.getStateAccess().getInitialInitKeyword_2_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getInitialInitKeyword_2_0()); 

            }

             after(grammarAccess.getStateAccess().getInitialInitKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__InitialAssignment_2"


    // $ANTLR start "rule__Transition__SourceAssignment_0"
    // InternalStateMachineDSL.g:564:1: rule__Transition__SourceAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__SourceAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:568:1: ( ( ( RULE_ID ) ) )
            // InternalStateMachineDSL.g:569:2: ( ( RULE_ID ) )
            {
            // InternalStateMachineDSL.g:569:2: ( ( RULE_ID ) )
            // InternalStateMachineDSL.g:570:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0()); 
            // InternalStateMachineDSL.g:571:3: ( RULE_ID )
            // InternalStateMachineDSL.g:572:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getSourceStateIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getSourceStateIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__SourceAssignment_0"


    // $ANTLR start "rule__Transition__InputAssignment_1_1"
    // InternalStateMachineDSL.g:583:1: rule__Transition__InputAssignment_1_1 : ( RULE_STRING ) ;
    public final void rule__Transition__InputAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:587:1: ( ( RULE_STRING ) )
            // InternalStateMachineDSL.g:588:2: ( RULE_STRING )
            {
            // InternalStateMachineDSL.g:588:2: ( RULE_STRING )
            // InternalStateMachineDSL.g:589:3: RULE_STRING
            {
             before(grammarAccess.getTransitionAccess().getInputSTRINGTerminalRuleCall_1_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getInputSTRINGTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__InputAssignment_1_1"


    // $ANTLR start "rule__Transition__TargetAssignment_3"
    // InternalStateMachineDSL.g:598:1: rule__Transition__TargetAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__TargetAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineDSL.g:602:1: ( ( ( RULE_ID ) ) )
            // InternalStateMachineDSL.g:603:2: ( ( RULE_ID ) )
            {
            // InternalStateMachineDSL.g:603:2: ( ( RULE_ID ) )
            // InternalStateMachineDSL.g:604:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 
            // InternalStateMachineDSL.g:605:3: ( RULE_ID )
            // InternalStateMachineDSL.g:606:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__TargetAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000004000L});

}