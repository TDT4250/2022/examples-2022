/**
 */
package no.ntnu.tdt4250.ra.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import no.ntnu.tdt4250.ra.Allocation;
import no.ntnu.tdt4250.ra.Course;
import no.ntnu.tdt4250.ra.Person;
import no.ntnu.tdt4250.ra.RaFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.ra.Course#getNumberOfLecturers() <em>Number Of Lecturers</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CourseTest extends TestCase {

	/**
	 * The fixture for this Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Course fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CourseTest.class);
	}

	/**
	 * Constructs a new Course test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Course fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Course getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(RaFactory.eINSTANCE.createCourse());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link no.ntnu.tdt4250.ra.Course#getNumberOfLecturers() <em>Number Of Lecturers</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.ntnu.tdt4250.ra.Course#getNumberOfLecturers()
	 * @generated NOT
	 */
	public void testGetNumberOfLecturers() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		
		RaFactory factory = RaFactory.eINSTANCE;
		
		// Create the Course instance
		Course c = factory.createCourse();
		c.setCode("TDT4250");
		c.getName().add("Advanced Software Design");
		
		assertEquals(0, c.getNumberOfLecturers());
		
		// Create the Person instance
		Person leo = factory.createPerson();
		leo.setFirstName("Leonardo");
		leo.setLastName("Montecchi");
		
		// Create the Person instance
		Person j = factory.createPerson();
		j.setFirstName("John");

		// Create the Allocation instance
		Allocation a = factory.createAllocation();
		a.setLecturers(leo); // Set Person in the Allocation
		a.setPercentage(0.5);
		
		// Create the Allocation instance
		Allocation a2 = factory.createAllocation();
		a.setLecturers(j); // Set Person in the Allocation
		a.setPercentage(0.5);
		
		// Add allocation to the course
		c.getAllocations().add(a);
		c.getAllocations().add(a2);
		
		assertEquals(2, c.getNumberOfLecturers());
	}

} //CourseTest
