package tdt4250.ra.application.handlers;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import tdt4250.ra.application.parts.SamplePart;
public class OpenHandler {

	@Execute
	public void execute(Shell shell, IEclipseContext context){
		SamplePart part = context.get(SamplePart.class);
		
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);

		String fileName = dialog.open();
		
		if(fileName != null) {
			context.set(SamplePart.FILE_NAME_KEY, fileName);
			part.setLabel(fileName);
			
			TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain();
			ResourceSet rs = domain.getResourceSet();

			Resource res = rs.getResource(URI.createFileURI(fileName), true);
			
			EObject rootObject = res.getContents().get(0);

			part.changeDomainModel(rootObject);
		}
	}
}
