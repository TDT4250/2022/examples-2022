package tdt4250.ra.application.handlers;

import java.io.IOException;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import tdt4250.ra.application.parts.SamplePart;

public class SaveHandler {

	@CanExecute
	public boolean canExecute(EPartService partService) {
//		if (partService != null) {
//			return !partService.getDirtyParts().isEmpty();
//		}
//		return false;
		
		return true;
	}

	@Execute
	public void execute(Shell shell, IEclipseContext context) {
		SamplePart part = context.get(SamplePart.class);

		String fileName = (String)context.get(SamplePart.FILE_NAME_KEY);

		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFileName(fileName);
		fileName = dialog.open();
		
		if(fileName != null) {
			
			context.set(SamplePart.FILE_NAME_KEY, fileName);
			
			EObject modelToSave = part.getDomainModel();
			// Creating a copy of the model, in order to avoid managing EMF transactions
			modelToSave = EcoreUtil.copy(modelToSave);
			
			ResourceSet rs = new ResourceSetImpl();
			
			Resource res = rs.createResource(URI.createFileURI(fileName));
			res.getContents().add(modelToSave);
			
			try {
				res.save(null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}