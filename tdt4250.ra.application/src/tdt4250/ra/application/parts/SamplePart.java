package tdt4250.ra.application.parts;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecp.ui.view.ECPRendererException;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTView;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import tdt4250.ra.RaPackage;

public class SamplePart {
	
	public static String FILE_NAME_KEY = "EcoreFileBoundToModel";

	@Inject
	private MPart part;
	
	private EObject boundObject;
	private ECPSWTView theView;

	private EObject getDummyEObject() {
		// Replace this with your own model EClass to test the application with a custom model
		final EClass eClass = RaPackage.eINSTANCE.getDepartment();
		return EcoreUtil.create(eClass);
	}
	
	@PostConstruct
	public void createComposite(Composite parent) {

		boundObject = getDummyEObject();
		try {
			final Composite content = new Composite(parent, SWT.NONE);
			content.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			content.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).create());

			theView = ECPSWTViewRenderer.INSTANCE.render(content, boundObject);
			part.getContext().getParent().set(SamplePart.class, this);
			
			content.layout();
			
		} catch (final ECPRendererException e) {
			e.printStackTrace();
		}
		parent.layout();
	}
	
	public EObject getDomainModel() {
		return theView.getViewModelContext().getDomainModel();
	}
	
	public void changeDomainModel(EObject object) {
		theView.getViewModelContext().changeDomainModel(object);
		boundObject = object;
		part.setDirty(false);
		part.setLabel(FILE_NAME_KEY);
	}

	public void setLabel(String label) {
		part.setLabel(label);
	}
	
}
