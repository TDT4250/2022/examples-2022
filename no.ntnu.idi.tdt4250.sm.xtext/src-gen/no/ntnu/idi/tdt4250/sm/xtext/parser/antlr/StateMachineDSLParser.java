/*
 * generated by Xtext 2.27.0
 */
package no.ntnu.idi.tdt4250.sm.xtext.parser.antlr;

import com.google.inject.Inject;
import no.ntnu.idi.tdt4250.sm.xtext.parser.antlr.internal.InternalStateMachineDSLParser;
import no.ntnu.idi.tdt4250.sm.xtext.services.StateMachineDSLGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class StateMachineDSLParser extends AbstractAntlrParser {

	@Inject
	private StateMachineDSLGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalStateMachineDSLParser createParser(XtextTokenStream stream) {
		return new InternalStateMachineDSLParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "StateMachine";
	}

	public StateMachineDSLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(StateMachineDSLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
