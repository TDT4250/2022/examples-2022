package no.ntnu.idi.tdt4250.sm.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.ntnu.idi.tdt4250.sm.xtext.services.StateMachineDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateMachineDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'s'", "'init'", "'['", "']'", "'->'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalStateMachineDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateMachineDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateMachineDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateMachineDSL.g"; }



     	private StateMachineDSLGrammarAccess grammarAccess;

        public InternalStateMachineDSLParser(TokenStream input, StateMachineDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StateMachine";
       	}

       	@Override
       	protected StateMachineDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStateMachine"
    // InternalStateMachineDSL.g:64:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalStateMachineDSL.g:64:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalStateMachineDSL.g:65:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalStateMachineDSL.g:71:1: ruleStateMachine returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_states_1_0= ruleState ) )+ ( (lv_transitions_2_0= ruleTransition ) )* ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_states_1_0 = null;

        EObject lv_transitions_2_0 = null;



        	enterRule();

        try {
            // InternalStateMachineDSL.g:77:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_states_1_0= ruleState ) )+ ( (lv_transitions_2_0= ruleTransition ) )* ) )
            // InternalStateMachineDSL.g:78:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_states_1_0= ruleState ) )+ ( (lv_transitions_2_0= ruleTransition ) )* )
            {
            // InternalStateMachineDSL.g:78:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_states_1_0= ruleState ) )+ ( (lv_transitions_2_0= ruleTransition ) )* )
            // InternalStateMachineDSL.g:79:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_states_1_0= ruleState ) )+ ( (lv_transitions_2_0= ruleTransition ) )*
            {
            // InternalStateMachineDSL.g:79:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalStateMachineDSL.g:80:4: (lv_name_0_0= RULE_ID )
            {
            // InternalStateMachineDSL.g:80:4: (lv_name_0_0= RULE_ID )
            // InternalStateMachineDSL.g:81:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateMachineRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalStateMachineDSL.g:97:3: ( (lv_states_1_0= ruleState ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalStateMachineDSL.g:98:4: (lv_states_1_0= ruleState )
            	    {
            	    // InternalStateMachineDSL.g:98:4: (lv_states_1_0= ruleState )
            	    // InternalStateMachineDSL.g:99:5: lv_states_1_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_states_1_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"states",
            	    						lv_states_1_0,
            	    						"no.ntnu.idi.tdt4250.sm.xtext.StateMachineDSL.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            // InternalStateMachineDSL.g:116:3: ( (lv_transitions_2_0= ruleTransition ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalStateMachineDSL.g:117:4: (lv_transitions_2_0= ruleTransition )
            	    {
            	    // InternalStateMachineDSL.g:117:4: (lv_transitions_2_0= ruleTransition )
            	    // InternalStateMachineDSL.g:118:5: lv_transitions_2_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_transitions_2_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_2_0,
            	    						"no.ntnu.idi.tdt4250.sm.xtext.StateMachineDSL.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalStateMachineDSL.g:139:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalStateMachineDSL.g:139:46: (iv_ruleState= ruleState EOF )
            // InternalStateMachineDSL.g:140:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateMachineDSL.g:146:1: ruleState returns [EObject current=null] : (otherlv_0= 's' ( (lv_name_1_0= RULE_ID ) ) ( (lv_initial_2_0= 'init' ) )? ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_initial_2_0=null;


        	enterRule();

        try {
            // InternalStateMachineDSL.g:152:2: ( (otherlv_0= 's' ( (lv_name_1_0= RULE_ID ) ) ( (lv_initial_2_0= 'init' ) )? ) )
            // InternalStateMachineDSL.g:153:2: (otherlv_0= 's' ( (lv_name_1_0= RULE_ID ) ) ( (lv_initial_2_0= 'init' ) )? )
            {
            // InternalStateMachineDSL.g:153:2: (otherlv_0= 's' ( (lv_name_1_0= RULE_ID ) ) ( (lv_initial_2_0= 'init' ) )? )
            // InternalStateMachineDSL.g:154:3: otherlv_0= 's' ( (lv_name_1_0= RULE_ID ) ) ( (lv_initial_2_0= 'init' ) )?
            {
            otherlv_0=(Token)match(input,11,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getSKeyword_0());
            		
            // InternalStateMachineDSL.g:158:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateMachineDSL.g:159:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateMachineDSL.g:159:4: (lv_name_1_0= RULE_ID )
            // InternalStateMachineDSL.g:160:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalStateMachineDSL.g:176:3: ( (lv_initial_2_0= 'init' ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalStateMachineDSL.g:177:4: (lv_initial_2_0= 'init' )
                    {
                    // InternalStateMachineDSL.g:177:4: (lv_initial_2_0= 'init' )
                    // InternalStateMachineDSL.g:178:5: lv_initial_2_0= 'init'
                    {
                    lv_initial_2_0=(Token)match(input,12,FOLLOW_2); 

                    					newLeafNode(lv_initial_2_0, grammarAccess.getStateAccess().getInitialInitKeyword_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getStateRule());
                    					}
                    					setWithLastConsumed(current, "initial", lv_initial_2_0 != null, "init");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalStateMachineDSL.g:194:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalStateMachineDSL.g:194:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalStateMachineDSL.g:195:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateMachineDSL.g:201:1: ruleTransition returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )? otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_input_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalStateMachineDSL.g:207:2: ( ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )? otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) ) )
            // InternalStateMachineDSL.g:208:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )? otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) )
            {
            // InternalStateMachineDSL.g:208:2: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )? otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) )
            // InternalStateMachineDSL.g:209:3: ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )? otherlv_4= '->' ( (otherlv_5= RULE_ID ) )
            {
            // InternalStateMachineDSL.g:209:3: ( (otherlv_0= RULE_ID ) )
            // InternalStateMachineDSL.g:210:4: (otherlv_0= RULE_ID )
            {
            // InternalStateMachineDSL.g:210:4: (otherlv_0= RULE_ID )
            // InternalStateMachineDSL.g:211:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0());
            				

            }


            }

            // InternalStateMachineDSL.g:222:3: (otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalStateMachineDSL.g:223:4: otherlv_1= '[' ( (lv_input_2_0= RULE_STRING ) ) otherlv_3= ']'
                    {
                    otherlv_1=(Token)match(input,13,FOLLOW_9); 

                    				newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_1_0());
                    			
                    // InternalStateMachineDSL.g:227:4: ( (lv_input_2_0= RULE_STRING ) )
                    // InternalStateMachineDSL.g:228:5: (lv_input_2_0= RULE_STRING )
                    {
                    // InternalStateMachineDSL.g:228:5: (lv_input_2_0= RULE_STRING )
                    // InternalStateMachineDSL.g:229:6: lv_input_2_0= RULE_STRING
                    {
                    lv_input_2_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

                    						newLeafNode(lv_input_2_0, grammarAccess.getTransitionAccess().getInputSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransitionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"input",
                    							lv_input_2_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    otherlv_3=(Token)match(input,14,FOLLOW_11); 

                    				newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getRightSquareBracketKeyword_1_2());
                    			

                    }
                    break;

            }

            otherlv_4=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2());
            		
            // InternalStateMachineDSL.g:254:3: ( (otherlv_5= RULE_ID ) )
            // InternalStateMachineDSL.g:255:4: (otherlv_5= RULE_ID )
            {
            // InternalStateMachineDSL.g:255:4: (otherlv_5= RULE_ID )
            // InternalStateMachineDSL.g:256:5: otherlv_5= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_5=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000812L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});

}