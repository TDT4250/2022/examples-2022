/*
 * generated by Xtext 2.27.0
 */
package no.ntnu.idi.tdt4250.sm.xtext.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
public class StateMachineDSLUiModule extends AbstractStateMachineDSLUiModule {

	public StateMachineDSLUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
