package no.hal.quiz.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import no.hal.quiz.xtext.services.QuizDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQuizDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_XML_TEXT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'yes'", "'true'", "'no'", "'false'", "'-'", "'x'", "'/'", "'quiz'", "'.'", "'part'", "'ref'", "'+-'", "'('", "')'", "'['", "']'", "'<<'", "'>>'", "'?'", "'='", "'~'", "'v'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_XML_TEXT=7;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalQuizDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQuizDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQuizDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQuizDSL.g"; }


    	private QuizDSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(QuizDSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleQuiz"
    // InternalQuizDSL.g:53:1: entryRuleQuiz : ruleQuiz EOF ;
    public final void entryRuleQuiz() throws RecognitionException {
        try {
            // InternalQuizDSL.g:54:1: ( ruleQuiz EOF )
            // InternalQuizDSL.g:55:1: ruleQuiz EOF
            {
             before(grammarAccess.getQuizRule()); 
            pushFollow(FOLLOW_1);
            ruleQuiz();

            state._fsp--;

             after(grammarAccess.getQuizRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuiz"


    // $ANTLR start "ruleQuiz"
    // InternalQuizDSL.g:62:1: ruleQuiz : ( ( rule__Quiz__Group__0 ) ) ;
    public final void ruleQuiz() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:66:2: ( ( ( rule__Quiz__Group__0 ) ) )
            // InternalQuizDSL.g:67:2: ( ( rule__Quiz__Group__0 ) )
            {
            // InternalQuizDSL.g:67:2: ( ( rule__Quiz__Group__0 ) )
            // InternalQuizDSL.g:68:3: ( rule__Quiz__Group__0 )
            {
             before(grammarAccess.getQuizAccess().getGroup()); 
            // InternalQuizDSL.g:69:3: ( rule__Quiz__Group__0 )
            // InternalQuizDSL.g:69:4: rule__Quiz__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuiz"


    // $ANTLR start "entryRuleQName"
    // InternalQuizDSL.g:78:1: entryRuleQName : ruleQName EOF ;
    public final void entryRuleQName() throws RecognitionException {
        try {
            // InternalQuizDSL.g:79:1: ( ruleQName EOF )
            // InternalQuizDSL.g:80:1: ruleQName EOF
            {
             before(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalQuizDSL.g:87:1: ruleQName : ( ( rule__QName__Group__0 ) ) ;
    public final void ruleQName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:91:2: ( ( ( rule__QName__Group__0 ) ) )
            // InternalQuizDSL.g:92:2: ( ( rule__QName__Group__0 ) )
            {
            // InternalQuizDSL.g:92:2: ( ( rule__QName__Group__0 ) )
            // InternalQuizDSL.g:93:3: ( rule__QName__Group__0 )
            {
             before(grammarAccess.getQNameAccess().getGroup()); 
            // InternalQuizDSL.g:94:3: ( rule__QName__Group__0 )
            // InternalQuizDSL.g:94:4: rule__QName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleAbstractQuizPart"
    // InternalQuizDSL.g:103:1: entryRuleAbstractQuizPart : ruleAbstractQuizPart EOF ;
    public final void entryRuleAbstractQuizPart() throws RecognitionException {
        try {
            // InternalQuizDSL.g:104:1: ( ruleAbstractQuizPart EOF )
            // InternalQuizDSL.g:105:1: ruleAbstractQuizPart EOF
            {
             before(grammarAccess.getAbstractQuizPartRule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractQuizPart();

            state._fsp--;

             after(grammarAccess.getAbstractQuizPartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractQuizPart"


    // $ANTLR start "ruleAbstractQuizPart"
    // InternalQuizDSL.g:112:1: ruleAbstractQuizPart : ( ( rule__AbstractQuizPart__Alternatives ) ) ;
    public final void ruleAbstractQuizPart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:116:2: ( ( ( rule__AbstractQuizPart__Alternatives ) ) )
            // InternalQuizDSL.g:117:2: ( ( rule__AbstractQuizPart__Alternatives ) )
            {
            // InternalQuizDSL.g:117:2: ( ( rule__AbstractQuizPart__Alternatives ) )
            // InternalQuizDSL.g:118:3: ( rule__AbstractQuizPart__Alternatives )
            {
             before(grammarAccess.getAbstractQuizPartAccess().getAlternatives()); 
            // InternalQuizDSL.g:119:3: ( rule__AbstractQuizPart__Alternatives )
            // InternalQuizDSL.g:119:4: rule__AbstractQuizPart__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractQuizPart__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractQuizPartAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractQuizPart"


    // $ANTLR start "entryRuleQuizPart"
    // InternalQuizDSL.g:128:1: entryRuleQuizPart : ruleQuizPart EOF ;
    public final void entryRuleQuizPart() throws RecognitionException {
        try {
            // InternalQuizDSL.g:129:1: ( ruleQuizPart EOF )
            // InternalQuizDSL.g:130:1: ruleQuizPart EOF
            {
             before(grammarAccess.getQuizPartRule()); 
            pushFollow(FOLLOW_1);
            ruleQuizPart();

            state._fsp--;

             after(grammarAccess.getQuizPartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuizPart"


    // $ANTLR start "ruleQuizPart"
    // InternalQuizDSL.g:137:1: ruleQuizPart : ( ( rule__QuizPart__Group__0 ) ) ;
    public final void ruleQuizPart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:141:2: ( ( ( rule__QuizPart__Group__0 ) ) )
            // InternalQuizDSL.g:142:2: ( ( rule__QuizPart__Group__0 ) )
            {
            // InternalQuizDSL.g:142:2: ( ( rule__QuizPart__Group__0 ) )
            // InternalQuizDSL.g:143:3: ( rule__QuizPart__Group__0 )
            {
             before(grammarAccess.getQuizPartAccess().getGroup()); 
            // InternalQuizDSL.g:144:3: ( rule__QuizPart__Group__0 )
            // InternalQuizDSL.g:144:4: rule__QuizPart__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuizPart"


    // $ANTLR start "entryRuleQuizPartRef"
    // InternalQuizDSL.g:153:1: entryRuleQuizPartRef : ruleQuizPartRef EOF ;
    public final void entryRuleQuizPartRef() throws RecognitionException {
        try {
            // InternalQuizDSL.g:154:1: ( ruleQuizPartRef EOF )
            // InternalQuizDSL.g:155:1: ruleQuizPartRef EOF
            {
             before(grammarAccess.getQuizPartRefRule()); 
            pushFollow(FOLLOW_1);
            ruleQuizPartRef();

            state._fsp--;

             after(grammarAccess.getQuizPartRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuizPartRef"


    // $ANTLR start "ruleQuizPartRef"
    // InternalQuizDSL.g:162:1: ruleQuizPartRef : ( ( rule__QuizPartRef__Group__0 ) ) ;
    public final void ruleQuizPartRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:166:2: ( ( ( rule__QuizPartRef__Group__0 ) ) )
            // InternalQuizDSL.g:167:2: ( ( rule__QuizPartRef__Group__0 ) )
            {
            // InternalQuizDSL.g:167:2: ( ( rule__QuizPartRef__Group__0 ) )
            // InternalQuizDSL.g:168:3: ( rule__QuizPartRef__Group__0 )
            {
             before(grammarAccess.getQuizPartRefAccess().getGroup()); 
            // InternalQuizDSL.g:169:3: ( rule__QuizPartRef__Group__0 )
            // InternalQuizDSL.g:169:4: rule__QuizPartRef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartRefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuizPartRef"


    // $ANTLR start "entryRuleAnonymousQuizPart"
    // InternalQuizDSL.g:178:1: entryRuleAnonymousQuizPart : ruleAnonymousQuizPart EOF ;
    public final void entryRuleAnonymousQuizPart() throws RecognitionException {
        try {
            // InternalQuizDSL.g:179:1: ( ruleAnonymousQuizPart EOF )
            // InternalQuizDSL.g:180:1: ruleAnonymousQuizPart EOF
            {
             before(grammarAccess.getAnonymousQuizPartRule()); 
            pushFollow(FOLLOW_1);
            ruleAnonymousQuizPart();

            state._fsp--;

             after(grammarAccess.getAnonymousQuizPartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnonymousQuizPart"


    // $ANTLR start "ruleAnonymousQuizPart"
    // InternalQuizDSL.g:187:1: ruleAnonymousQuizPart : ( ( rule__AnonymousQuizPart__Group__0 ) ) ;
    public final void ruleAnonymousQuizPart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:191:2: ( ( ( rule__AnonymousQuizPart__Group__0 ) ) )
            // InternalQuizDSL.g:192:2: ( ( rule__AnonymousQuizPart__Group__0 ) )
            {
            // InternalQuizDSL.g:192:2: ( ( rule__AnonymousQuizPart__Group__0 ) )
            // InternalQuizDSL.g:193:3: ( rule__AnonymousQuizPart__Group__0 )
            {
             before(grammarAccess.getAnonymousQuizPartAccess().getGroup()); 
            // InternalQuizDSL.g:194:3: ( rule__AnonymousQuizPart__Group__0 )
            // InternalQuizDSL.g:194:4: rule__AnonymousQuizPart__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AnonymousQuizPart__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnonymousQuizPartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnonymousQuizPart"


    // $ANTLR start "entryRuleAbstractQA"
    // InternalQuizDSL.g:203:1: entryRuleAbstractQA : ruleAbstractQA EOF ;
    public final void entryRuleAbstractQA() throws RecognitionException {
        try {
            // InternalQuizDSL.g:204:1: ( ruleAbstractQA EOF )
            // InternalQuizDSL.g:205:1: ruleAbstractQA EOF
            {
             before(grammarAccess.getAbstractQARule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractQA();

            state._fsp--;

             after(grammarAccess.getAbstractQARule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractQA"


    // $ANTLR start "ruleAbstractQA"
    // InternalQuizDSL.g:212:1: ruleAbstractQA : ( ( rule__AbstractQA__Alternatives ) ) ;
    public final void ruleAbstractQA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:216:2: ( ( ( rule__AbstractQA__Alternatives ) ) )
            // InternalQuizDSL.g:217:2: ( ( rule__AbstractQA__Alternatives ) )
            {
            // InternalQuizDSL.g:217:2: ( ( rule__AbstractQA__Alternatives ) )
            // InternalQuizDSL.g:218:3: ( rule__AbstractQA__Alternatives )
            {
             before(grammarAccess.getAbstractQAAccess().getAlternatives()); 
            // InternalQuizDSL.g:219:3: ( rule__AbstractQA__Alternatives )
            // InternalQuizDSL.g:219:4: rule__AbstractQA__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractQA__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractQAAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractQA"


    // $ANTLR start "entryRuleQARef"
    // InternalQuizDSL.g:228:1: entryRuleQARef : ruleQARef EOF ;
    public final void entryRuleQARef() throws RecognitionException {
        try {
            // InternalQuizDSL.g:229:1: ( ruleQARef EOF )
            // InternalQuizDSL.g:230:1: ruleQARef EOF
            {
             before(grammarAccess.getQARefRule()); 
            pushFollow(FOLLOW_1);
            ruleQARef();

            state._fsp--;

             after(grammarAccess.getQARefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQARef"


    // $ANTLR start "ruleQARef"
    // InternalQuizDSL.g:237:1: ruleQARef : ( ( rule__QARef__Group__0 ) ) ;
    public final void ruleQARef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:241:2: ( ( ( rule__QARef__Group__0 ) ) )
            // InternalQuizDSL.g:242:2: ( ( rule__QARef__Group__0 ) )
            {
            // InternalQuizDSL.g:242:2: ( ( rule__QARef__Group__0 ) )
            // InternalQuizDSL.g:243:3: ( rule__QARef__Group__0 )
            {
             before(grammarAccess.getQARefAccess().getGroup()); 
            // InternalQuizDSL.g:244:3: ( rule__QARef__Group__0 )
            // InternalQuizDSL.g:244:4: rule__QARef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QARef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQARefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQARef"


    // $ANTLR start "entryRuleQA"
    // InternalQuizDSL.g:253:1: entryRuleQA : ruleQA EOF ;
    public final void entryRuleQA() throws RecognitionException {
        try {
            // InternalQuizDSL.g:254:1: ( ruleQA EOF )
            // InternalQuizDSL.g:255:1: ruleQA EOF
            {
             before(grammarAccess.getQARule()); 
            pushFollow(FOLLOW_1);
            ruleQA();

            state._fsp--;

             after(grammarAccess.getQARule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQA"


    // $ANTLR start "ruleQA"
    // InternalQuizDSL.g:262:1: ruleQA : ( ( rule__QA__Group__0 ) ) ;
    public final void ruleQA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:266:2: ( ( ( rule__QA__Group__0 ) ) )
            // InternalQuizDSL.g:267:2: ( ( rule__QA__Group__0 ) )
            {
            // InternalQuizDSL.g:267:2: ( ( rule__QA__Group__0 ) )
            // InternalQuizDSL.g:268:3: ( rule__QA__Group__0 )
            {
             before(grammarAccess.getQAAccess().getGroup()); 
            // InternalQuizDSL.g:269:3: ( rule__QA__Group__0 )
            // InternalQuizDSL.g:269:4: rule__QA__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QA__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQA"


    // $ANTLR start "entryRuleQuestion"
    // InternalQuizDSL.g:278:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // InternalQuizDSL.g:279:1: ( ruleQuestion EOF )
            // InternalQuizDSL.g:280:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_1);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // InternalQuizDSL.g:287:1: ruleQuestion : ( ( rule__Question__Alternatives ) ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:291:2: ( ( ( rule__Question__Alternatives ) ) )
            // InternalQuizDSL.g:292:2: ( ( rule__Question__Alternatives ) )
            {
            // InternalQuizDSL.g:292:2: ( ( rule__Question__Alternatives ) )
            // InternalQuizDSL.g:293:3: ( rule__Question__Alternatives )
            {
             before(grammarAccess.getQuestionAccess().getAlternatives()); 
            // InternalQuizDSL.g:294:3: ( rule__Question__Alternatives )
            // InternalQuizDSL.g:294:4: rule__Question__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Question__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleStringQuestion"
    // InternalQuizDSL.g:303:1: entryRuleStringQuestion : ruleStringQuestion EOF ;
    public final void entryRuleStringQuestion() throws RecognitionException {
        try {
            // InternalQuizDSL.g:304:1: ( ruleStringQuestion EOF )
            // InternalQuizDSL.g:305:1: ruleStringQuestion EOF
            {
             before(grammarAccess.getStringQuestionRule()); 
            pushFollow(FOLLOW_1);
            ruleStringQuestion();

            state._fsp--;

             after(grammarAccess.getStringQuestionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringQuestion"


    // $ANTLR start "ruleStringQuestion"
    // InternalQuizDSL.g:312:1: ruleStringQuestion : ( ( rule__StringQuestion__QuestionAssignment ) ) ;
    public final void ruleStringQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:316:2: ( ( ( rule__StringQuestion__QuestionAssignment ) ) )
            // InternalQuizDSL.g:317:2: ( ( rule__StringQuestion__QuestionAssignment ) )
            {
            // InternalQuizDSL.g:317:2: ( ( rule__StringQuestion__QuestionAssignment ) )
            // InternalQuizDSL.g:318:3: ( rule__StringQuestion__QuestionAssignment )
            {
             before(grammarAccess.getStringQuestionAccess().getQuestionAssignment()); 
            // InternalQuizDSL.g:319:3: ( rule__StringQuestion__QuestionAssignment )
            // InternalQuizDSL.g:319:4: rule__StringQuestion__QuestionAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StringQuestion__QuestionAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringQuestionAccess().getQuestionAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringQuestion"


    // $ANTLR start "entryRuleXmlQuestion"
    // InternalQuizDSL.g:328:1: entryRuleXmlQuestion : ruleXmlQuestion EOF ;
    public final void entryRuleXmlQuestion() throws RecognitionException {
        try {
            // InternalQuizDSL.g:329:1: ( ruleXmlQuestion EOF )
            // InternalQuizDSL.g:330:1: ruleXmlQuestion EOF
            {
             before(grammarAccess.getXmlQuestionRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlQuestion();

            state._fsp--;

             after(grammarAccess.getXmlQuestionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlQuestion"


    // $ANTLR start "ruleXmlQuestion"
    // InternalQuizDSL.g:337:1: ruleXmlQuestion : ( ( rule__XmlQuestion__XmlAssignment ) ) ;
    public final void ruleXmlQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:341:2: ( ( ( rule__XmlQuestion__XmlAssignment ) ) )
            // InternalQuizDSL.g:342:2: ( ( rule__XmlQuestion__XmlAssignment ) )
            {
            // InternalQuizDSL.g:342:2: ( ( rule__XmlQuestion__XmlAssignment ) )
            // InternalQuizDSL.g:343:3: ( rule__XmlQuestion__XmlAssignment )
            {
             before(grammarAccess.getXmlQuestionAccess().getXmlAssignment()); 
            // InternalQuizDSL.g:344:3: ( rule__XmlQuestion__XmlAssignment )
            // InternalQuizDSL.g:344:4: rule__XmlQuestion__XmlAssignment
            {
            pushFollow(FOLLOW_2);
            rule__XmlQuestion__XmlAssignment();

            state._fsp--;


            }

             after(grammarAccess.getXmlQuestionAccess().getXmlAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlQuestion"


    // $ANTLR start "entryRuleAnswer"
    // InternalQuizDSL.g:353:1: entryRuleAnswer : ruleAnswer EOF ;
    public final void entryRuleAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:354:1: ( ruleAnswer EOF )
            // InternalQuizDSL.g:355:1: ruleAnswer EOF
            {
             before(grammarAccess.getAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleAnswer();

            state._fsp--;

             after(grammarAccess.getAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnswer"


    // $ANTLR start "ruleAnswer"
    // InternalQuizDSL.g:362:1: ruleAnswer : ( ( rule__Answer__Alternatives ) ) ;
    public final void ruleAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:366:2: ( ( ( rule__Answer__Alternatives ) ) )
            // InternalQuizDSL.g:367:2: ( ( rule__Answer__Alternatives ) )
            {
            // InternalQuizDSL.g:367:2: ( ( rule__Answer__Alternatives ) )
            // InternalQuizDSL.g:368:3: ( rule__Answer__Alternatives )
            {
             before(grammarAccess.getAnswerAccess().getAlternatives()); 
            // InternalQuizDSL.g:369:3: ( rule__Answer__Alternatives )
            // InternalQuizDSL.g:369:4: rule__Answer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Answer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnswer"


    // $ANTLR start "entryRuleOptionAnswer"
    // InternalQuizDSL.g:378:1: entryRuleOptionAnswer : ruleOptionAnswer EOF ;
    public final void entryRuleOptionAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:379:1: ( ruleOptionAnswer EOF )
            // InternalQuizDSL.g:380:1: ruleOptionAnswer EOF
            {
             before(grammarAccess.getOptionAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getOptionAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptionAnswer"


    // $ANTLR start "ruleOptionAnswer"
    // InternalQuizDSL.g:387:1: ruleOptionAnswer : ( ( rule__OptionAnswer__Alternatives ) ) ;
    public final void ruleOptionAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:391:2: ( ( ( rule__OptionAnswer__Alternatives ) ) )
            // InternalQuizDSL.g:392:2: ( ( rule__OptionAnswer__Alternatives ) )
            {
            // InternalQuizDSL.g:392:2: ( ( rule__OptionAnswer__Alternatives ) )
            // InternalQuizDSL.g:393:3: ( rule__OptionAnswer__Alternatives )
            {
             before(grammarAccess.getOptionAnswerAccess().getAlternatives()); 
            // InternalQuizDSL.g:394:3: ( rule__OptionAnswer__Alternatives )
            // InternalQuizDSL.g:394:4: rule__OptionAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OptionAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOptionAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptionAnswer"


    // $ANTLR start "entryRuleSimpleAnswer"
    // InternalQuizDSL.g:403:1: entryRuleSimpleAnswer : ruleSimpleAnswer EOF ;
    public final void entryRuleSimpleAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:404:1: ( ruleSimpleAnswer EOF )
            // InternalQuizDSL.g:405:1: ruleSimpleAnswer EOF
            {
             before(grammarAccess.getSimpleAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleAnswer();

            state._fsp--;

             after(grammarAccess.getSimpleAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleAnswer"


    // $ANTLR start "ruleSimpleAnswer"
    // InternalQuizDSL.g:412:1: ruleSimpleAnswer : ( ( rule__SimpleAnswer__Alternatives ) ) ;
    public final void ruleSimpleAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:416:2: ( ( ( rule__SimpleAnswer__Alternatives ) ) )
            // InternalQuizDSL.g:417:2: ( ( rule__SimpleAnswer__Alternatives ) )
            {
            // InternalQuizDSL.g:417:2: ( ( rule__SimpleAnswer__Alternatives ) )
            // InternalQuizDSL.g:418:3: ( rule__SimpleAnswer__Alternatives )
            {
             before(grammarAccess.getSimpleAnswerAccess().getAlternatives()); 
            // InternalQuizDSL.g:419:3: ( rule__SimpleAnswer__Alternatives )
            // InternalQuizDSL.g:419:4: rule__SimpleAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SimpleAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSimpleAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleAnswer"


    // $ANTLR start "entryRuleStringAnswer"
    // InternalQuizDSL.g:428:1: entryRuleStringAnswer : ruleStringAnswer EOF ;
    public final void entryRuleStringAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:429:1: ( ruleStringAnswer EOF )
            // InternalQuizDSL.g:430:1: ruleStringAnswer EOF
            {
             before(grammarAccess.getStringAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleStringAnswer();

            state._fsp--;

             after(grammarAccess.getStringAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringAnswer"


    // $ANTLR start "ruleStringAnswer"
    // InternalQuizDSL.g:437:1: ruleStringAnswer : ( ( rule__StringAnswer__Group__0 ) ) ;
    public final void ruleStringAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:441:2: ( ( ( rule__StringAnswer__Group__0 ) ) )
            // InternalQuizDSL.g:442:2: ( ( rule__StringAnswer__Group__0 ) )
            {
            // InternalQuizDSL.g:442:2: ( ( rule__StringAnswer__Group__0 ) )
            // InternalQuizDSL.g:443:3: ( rule__StringAnswer__Group__0 )
            {
             before(grammarAccess.getStringAnswerAccess().getGroup()); 
            // InternalQuizDSL.g:444:3: ( rule__StringAnswer__Group__0 )
            // InternalQuizDSL.g:444:4: rule__StringAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStringAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringAnswer"


    // $ANTLR start "entryRuleRegexAnswer"
    // InternalQuizDSL.g:453:1: entryRuleRegexAnswer : ruleRegexAnswer EOF ;
    public final void entryRuleRegexAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:454:1: ( ruleRegexAnswer EOF )
            // InternalQuizDSL.g:455:1: ruleRegexAnswer EOF
            {
             before(grammarAccess.getRegexAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleRegexAnswer();

            state._fsp--;

             after(grammarAccess.getRegexAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRegexAnswer"


    // $ANTLR start "ruleRegexAnswer"
    // InternalQuizDSL.g:462:1: ruleRegexAnswer : ( ( rule__RegexAnswer__Group__0 ) ) ;
    public final void ruleRegexAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:466:2: ( ( ( rule__RegexAnswer__Group__0 ) ) )
            // InternalQuizDSL.g:467:2: ( ( rule__RegexAnswer__Group__0 ) )
            {
            // InternalQuizDSL.g:467:2: ( ( rule__RegexAnswer__Group__0 ) )
            // InternalQuizDSL.g:468:3: ( rule__RegexAnswer__Group__0 )
            {
             before(grammarAccess.getRegexAnswerAccess().getGroup()); 
            // InternalQuizDSL.g:469:3: ( rule__RegexAnswer__Group__0 )
            // InternalQuizDSL.g:469:4: rule__RegexAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RegexAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRegexAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRegexAnswer"


    // $ANTLR start "entryRuleNumberAnswer"
    // InternalQuizDSL.g:478:1: entryRuleNumberAnswer : ruleNumberAnswer EOF ;
    public final void entryRuleNumberAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:479:1: ( ruleNumberAnswer EOF )
            // InternalQuizDSL.g:480:1: ruleNumberAnswer EOF
            {
             before(grammarAccess.getNumberAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleNumberAnswer();

            state._fsp--;

             after(grammarAccess.getNumberAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberAnswer"


    // $ANTLR start "ruleNumberAnswer"
    // InternalQuizDSL.g:487:1: ruleNumberAnswer : ( ( rule__NumberAnswer__Group__0 ) ) ;
    public final void ruleNumberAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:491:2: ( ( ( rule__NumberAnswer__Group__0 ) ) )
            // InternalQuizDSL.g:492:2: ( ( rule__NumberAnswer__Group__0 ) )
            {
            // InternalQuizDSL.g:492:2: ( ( rule__NumberAnswer__Group__0 ) )
            // InternalQuizDSL.g:493:3: ( rule__NumberAnswer__Group__0 )
            {
             before(grammarAccess.getNumberAnswerAccess().getGroup()); 
            // InternalQuizDSL.g:494:3: ( rule__NumberAnswer__Group__0 )
            // InternalQuizDSL.g:494:4: rule__NumberAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNumberAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberAnswer"


    // $ANTLR start "entryRuleEDoubleObject"
    // InternalQuizDSL.g:503:1: entryRuleEDoubleObject : ruleEDoubleObject EOF ;
    public final void entryRuleEDoubleObject() throws RecognitionException {
        try {
            // InternalQuizDSL.g:504:1: ( ruleEDoubleObject EOF )
            // InternalQuizDSL.g:505:1: ruleEDoubleObject EOF
            {
             before(grammarAccess.getEDoubleObjectRule()); 
            pushFollow(FOLLOW_1);
            ruleEDoubleObject();

            state._fsp--;

             after(grammarAccess.getEDoubleObjectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEDoubleObject"


    // $ANTLR start "ruleEDoubleObject"
    // InternalQuizDSL.g:512:1: ruleEDoubleObject : ( ( rule__EDoubleObject__Group__0 ) ) ;
    public final void ruleEDoubleObject() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:516:2: ( ( ( rule__EDoubleObject__Group__0 ) ) )
            // InternalQuizDSL.g:517:2: ( ( rule__EDoubleObject__Group__0 ) )
            {
            // InternalQuizDSL.g:517:2: ( ( rule__EDoubleObject__Group__0 ) )
            // InternalQuizDSL.g:518:3: ( rule__EDoubleObject__Group__0 )
            {
             before(grammarAccess.getEDoubleObjectAccess().getGroup()); 
            // InternalQuizDSL.g:519:3: ( rule__EDoubleObject__Group__0 )
            // InternalQuizDSL.g:519:4: rule__EDoubleObject__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EDoubleObject__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEDoubleObjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEDoubleObject"


    // $ANTLR start "entryRuleBooleanAnswer"
    // InternalQuizDSL.g:528:1: entryRuleBooleanAnswer : ruleBooleanAnswer EOF ;
    public final void entryRuleBooleanAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:529:1: ( ruleBooleanAnswer EOF )
            // InternalQuizDSL.g:530:1: ruleBooleanAnswer EOF
            {
             before(grammarAccess.getBooleanAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanAnswer();

            state._fsp--;

             after(grammarAccess.getBooleanAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanAnswer"


    // $ANTLR start "ruleBooleanAnswer"
    // InternalQuizDSL.g:537:1: ruleBooleanAnswer : ( ( rule__BooleanAnswer__Group__0 ) ) ;
    public final void ruleBooleanAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:541:2: ( ( ( rule__BooleanAnswer__Group__0 ) ) )
            // InternalQuizDSL.g:542:2: ( ( rule__BooleanAnswer__Group__0 ) )
            {
            // InternalQuizDSL.g:542:2: ( ( rule__BooleanAnswer__Group__0 ) )
            // InternalQuizDSL.g:543:3: ( rule__BooleanAnswer__Group__0 )
            {
             before(grammarAccess.getBooleanAnswerAccess().getGroup()); 
            // InternalQuizDSL.g:544:3: ( rule__BooleanAnswer__Group__0 )
            // InternalQuizDSL.g:544:4: rule__BooleanAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanAnswer"


    // $ANTLR start "entryRuleXmlAnswer"
    // InternalQuizDSL.g:553:1: entryRuleXmlAnswer : ruleXmlAnswer EOF ;
    public final void entryRuleXmlAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:554:1: ( ruleXmlAnswer EOF )
            // InternalQuizDSL.g:555:1: ruleXmlAnswer EOF
            {
             before(grammarAccess.getXmlAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlAnswer();

            state._fsp--;

             after(grammarAccess.getXmlAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlAnswer"


    // $ANTLR start "ruleXmlAnswer"
    // InternalQuizDSL.g:562:1: ruleXmlAnswer : ( ( rule__XmlAnswer__XmlAssignment ) ) ;
    public final void ruleXmlAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:566:2: ( ( ( rule__XmlAnswer__XmlAssignment ) ) )
            // InternalQuizDSL.g:567:2: ( ( rule__XmlAnswer__XmlAssignment ) )
            {
            // InternalQuizDSL.g:567:2: ( ( rule__XmlAnswer__XmlAssignment ) )
            // InternalQuizDSL.g:568:3: ( rule__XmlAnswer__XmlAssignment )
            {
             before(grammarAccess.getXmlAnswerAccess().getXmlAssignment()); 
            // InternalQuizDSL.g:569:3: ( rule__XmlAnswer__XmlAssignment )
            // InternalQuizDSL.g:569:4: rule__XmlAnswer__XmlAssignment
            {
            pushFollow(FOLLOW_2);
            rule__XmlAnswer__XmlAssignment();

            state._fsp--;


            }

             after(grammarAccess.getXmlAnswerAccess().getXmlAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlAnswer"


    // $ANTLR start "entryRuleOptionsAnswer"
    // InternalQuizDSL.g:578:1: entryRuleOptionsAnswer : ruleOptionsAnswer EOF ;
    public final void entryRuleOptionsAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:579:1: ( ruleOptionsAnswer EOF )
            // InternalQuizDSL.g:580:1: ruleOptionsAnswer EOF
            {
             before(grammarAccess.getOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptionsAnswer"


    // $ANTLR start "ruleOptionsAnswer"
    // InternalQuizDSL.g:587:1: ruleOptionsAnswer : ( ( rule__OptionsAnswer__Alternatives ) ) ;
    public final void ruleOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:591:2: ( ( ( rule__OptionsAnswer__Alternatives ) ) )
            // InternalQuizDSL.g:592:2: ( ( rule__OptionsAnswer__Alternatives ) )
            {
            // InternalQuizDSL.g:592:2: ( ( rule__OptionsAnswer__Alternatives ) )
            // InternalQuizDSL.g:593:3: ( rule__OptionsAnswer__Alternatives )
            {
             before(grammarAccess.getOptionsAnswerAccess().getAlternatives()); 
            // InternalQuizDSL.g:594:3: ( rule__OptionsAnswer__Alternatives )
            // InternalQuizDSL.g:594:4: rule__OptionsAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OptionsAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptionsAnswer"


    // $ANTLR start "entryRuleSingleOptionsAnswer"
    // InternalQuizDSL.g:603:1: entryRuleSingleOptionsAnswer : ruleSingleOptionsAnswer EOF ;
    public final void entryRuleSingleOptionsAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:604:1: ( ruleSingleOptionsAnswer EOF )
            // InternalQuizDSL.g:605:1: ruleSingleOptionsAnswer EOF
            {
             before(grammarAccess.getSingleOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getSingleOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleOptionsAnswer"


    // $ANTLR start "ruleSingleOptionsAnswer"
    // InternalQuizDSL.g:612:1: ruleSingleOptionsAnswer : ( ( rule__SingleOptionsAnswer__Alternatives ) ) ;
    public final void ruleSingleOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:616:2: ( ( ( rule__SingleOptionsAnswer__Alternatives ) ) )
            // InternalQuizDSL.g:617:2: ( ( rule__SingleOptionsAnswer__Alternatives ) )
            {
            // InternalQuizDSL.g:617:2: ( ( rule__SingleOptionsAnswer__Alternatives ) )
            // InternalQuizDSL.g:618:3: ( rule__SingleOptionsAnswer__Alternatives )
            {
             before(grammarAccess.getSingleOptionsAnswerAccess().getAlternatives()); 
            // InternalQuizDSL.g:619:3: ( rule__SingleOptionsAnswer__Alternatives )
            // InternalQuizDSL.g:619:4: rule__SingleOptionsAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SingleOptionsAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSingleOptionsAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleOptionsAnswer"


    // $ANTLR start "entryRuleSingleBoxOptionsAnswer"
    // InternalQuizDSL.g:628:1: entryRuleSingleBoxOptionsAnswer : ruleSingleBoxOptionsAnswer EOF ;
    public final void entryRuleSingleBoxOptionsAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:629:1: ( ruleSingleBoxOptionsAnswer EOF )
            // InternalQuizDSL.g:630:1: ruleSingleBoxOptionsAnswer EOF
            {
             before(grammarAccess.getSingleBoxOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleBoxOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getSingleBoxOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleBoxOptionsAnswer"


    // $ANTLR start "ruleSingleBoxOptionsAnswer"
    // InternalQuizDSL.g:637:1: ruleSingleBoxOptionsAnswer : ( ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* ) ) ;
    public final void ruleSingleBoxOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:641:2: ( ( ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* ) ) )
            // InternalQuizDSL.g:642:2: ( ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* ) )
            {
            // InternalQuizDSL.g:642:2: ( ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* ) )
            // InternalQuizDSL.g:643:3: ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* )
            {
            // InternalQuizDSL.g:643:3: ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment ) )
            // InternalQuizDSL.g:644:4: ( rule__SingleBoxOptionsAnswer__OptionsAssignment )
            {
             before(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:645:4: ( rule__SingleBoxOptionsAnswer__OptionsAssignment )
            // InternalQuizDSL.g:645:5: rule__SingleBoxOptionsAnswer__OptionsAssignment
            {
            pushFollow(FOLLOW_3);
            rule__SingleBoxOptionsAnswer__OptionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsAssignment()); 

            }

            // InternalQuizDSL.g:648:3: ( ( rule__SingleBoxOptionsAnswer__OptionsAssignment )* )
            // InternalQuizDSL.g:649:4: ( rule__SingleBoxOptionsAnswer__OptionsAssignment )*
            {
             before(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:650:4: ( rule__SingleBoxOptionsAnswer__OptionsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==24) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalQuizDSL.g:650:5: rule__SingleBoxOptionsAnswer__OptionsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__SingleBoxOptionsAnswer__OptionsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleBoxOptionsAnswer"


    // $ANTLR start "entryRuleSingleBoxOption"
    // InternalQuizDSL.g:660:1: entryRuleSingleBoxOption : ruleSingleBoxOption EOF ;
    public final void entryRuleSingleBoxOption() throws RecognitionException {
        try {
            // InternalQuizDSL.g:661:1: ( ruleSingleBoxOption EOF )
            // InternalQuizDSL.g:662:1: ruleSingleBoxOption EOF
            {
             before(grammarAccess.getSingleBoxOptionRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleBoxOption();

            state._fsp--;

             after(grammarAccess.getSingleBoxOptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleBoxOption"


    // $ANTLR start "ruleSingleBoxOption"
    // InternalQuizDSL.g:669:1: ruleSingleBoxOption : ( ( rule__SingleBoxOption__Group__0 ) ) ;
    public final void ruleSingleBoxOption() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:673:2: ( ( ( rule__SingleBoxOption__Group__0 ) ) )
            // InternalQuizDSL.g:674:2: ( ( rule__SingleBoxOption__Group__0 ) )
            {
            // InternalQuizDSL.g:674:2: ( ( rule__SingleBoxOption__Group__0 ) )
            // InternalQuizDSL.g:675:3: ( rule__SingleBoxOption__Group__0 )
            {
             before(grammarAccess.getSingleBoxOptionAccess().getGroup()); 
            // InternalQuizDSL.g:676:3: ( rule__SingleBoxOption__Group__0 )
            // InternalQuizDSL.g:676:4: rule__SingleBoxOption__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSingleBoxOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleBoxOption"


    // $ANTLR start "entryRuleSingleListOptionsAnswer"
    // InternalQuizDSL.g:685:1: entryRuleSingleListOptionsAnswer : ruleSingleListOptionsAnswer EOF ;
    public final void entryRuleSingleListOptionsAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:686:1: ( ruleSingleListOptionsAnswer EOF )
            // InternalQuizDSL.g:687:1: ruleSingleListOptionsAnswer EOF
            {
             before(grammarAccess.getSingleListOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleListOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getSingleListOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleListOptionsAnswer"


    // $ANTLR start "ruleSingleListOptionsAnswer"
    // InternalQuizDSL.g:694:1: ruleSingleListOptionsAnswer : ( ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* ) ) ;
    public final void ruleSingleListOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:698:2: ( ( ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* ) ) )
            // InternalQuizDSL.g:699:2: ( ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* ) )
            {
            // InternalQuizDSL.g:699:2: ( ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* ) )
            // InternalQuizDSL.g:700:3: ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* )
            {
            // InternalQuizDSL.g:700:3: ( ( rule__SingleListOptionsAnswer__OptionsAssignment ) )
            // InternalQuizDSL.g:701:4: ( rule__SingleListOptionsAnswer__OptionsAssignment )
            {
             before(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:702:4: ( rule__SingleListOptionsAnswer__OptionsAssignment )
            // InternalQuizDSL.g:702:5: rule__SingleListOptionsAnswer__OptionsAssignment
            {
            pushFollow(FOLLOW_4);
            rule__SingleListOptionsAnswer__OptionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsAssignment()); 

            }

            // InternalQuizDSL.g:705:3: ( ( rule__SingleListOptionsAnswer__OptionsAssignment )* )
            // InternalQuizDSL.g:706:4: ( rule__SingleListOptionsAnswer__OptionsAssignment )*
            {
             before(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:707:4: ( rule__SingleListOptionsAnswer__OptionsAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=16 && LA2_0<=17)||LA2_0==33) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalQuizDSL.g:707:5: rule__SingleListOptionsAnswer__OptionsAssignment
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__SingleListOptionsAnswer__OptionsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleListOptionsAnswer"


    // $ANTLR start "entryRuleSingleListOption"
    // InternalQuizDSL.g:717:1: entryRuleSingleListOption : ruleSingleListOption EOF ;
    public final void entryRuleSingleListOption() throws RecognitionException {
        try {
            // InternalQuizDSL.g:718:1: ( ruleSingleListOption EOF )
            // InternalQuizDSL.g:719:1: ruleSingleListOption EOF
            {
             before(grammarAccess.getSingleListOptionRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleListOption();

            state._fsp--;

             after(grammarAccess.getSingleListOptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleListOption"


    // $ANTLR start "ruleSingleListOption"
    // InternalQuizDSL.g:726:1: ruleSingleListOption : ( ( rule__SingleListOption__Group__0 ) ) ;
    public final void ruleSingleListOption() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:730:2: ( ( ( rule__SingleListOption__Group__0 ) ) )
            // InternalQuizDSL.g:731:2: ( ( rule__SingleListOption__Group__0 ) )
            {
            // InternalQuizDSL.g:731:2: ( ( rule__SingleListOption__Group__0 ) )
            // InternalQuizDSL.g:732:3: ( rule__SingleListOption__Group__0 )
            {
             before(grammarAccess.getSingleListOptionAccess().getGroup()); 
            // InternalQuizDSL.g:733:3: ( rule__SingleListOption__Group__0 )
            // InternalQuizDSL.g:733:4: rule__SingleListOption__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleListOption"


    // $ANTLR start "entryRuleManyOptionsAnswer"
    // InternalQuizDSL.g:742:1: entryRuleManyOptionsAnswer : ruleManyOptionsAnswer EOF ;
    public final void entryRuleManyOptionsAnswer() throws RecognitionException {
        try {
            // InternalQuizDSL.g:743:1: ( ruleManyOptionsAnswer EOF )
            // InternalQuizDSL.g:744:1: ruleManyOptionsAnswer EOF
            {
             before(grammarAccess.getManyOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleManyOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getManyOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyOptionsAnswer"


    // $ANTLR start "ruleManyOptionsAnswer"
    // InternalQuizDSL.g:751:1: ruleManyOptionsAnswer : ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) ) ;
    public final void ruleManyOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:755:2: ( ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) ) )
            // InternalQuizDSL.g:756:2: ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) )
            {
            // InternalQuizDSL.g:756:2: ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) )
            // InternalQuizDSL.g:757:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* )
            {
            // InternalQuizDSL.g:757:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment ) )
            // InternalQuizDSL.g:758:4: ( rule__ManyOptionsAnswer__OptionsAssignment )
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:759:4: ( rule__ManyOptionsAnswer__OptionsAssignment )
            // InternalQuizDSL.g:759:5: rule__ManyOptionsAnswer__OptionsAssignment
            {
            pushFollow(FOLLOW_5);
            rule__ManyOptionsAnswer__OptionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 

            }

            // InternalQuizDSL.g:762:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment )* )
            // InternalQuizDSL.g:763:4: ( rule__ManyOptionsAnswer__OptionsAssignment )*
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalQuizDSL.g:764:4: ( rule__ManyOptionsAnswer__OptionsAssignment )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==26) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalQuizDSL.g:764:5: rule__ManyOptionsAnswer__OptionsAssignment
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__ManyOptionsAnswer__OptionsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyOptionsAnswer"


    // $ANTLR start "entryRuleManyOption"
    // InternalQuizDSL.g:774:1: entryRuleManyOption : ruleManyOption EOF ;
    public final void entryRuleManyOption() throws RecognitionException {
        try {
            // InternalQuizDSL.g:775:1: ( ruleManyOption EOF )
            // InternalQuizDSL.g:776:1: ruleManyOption EOF
            {
             before(grammarAccess.getManyOptionRule()); 
            pushFollow(FOLLOW_1);
            ruleManyOption();

            state._fsp--;

             after(grammarAccess.getManyOptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyOption"


    // $ANTLR start "ruleManyOption"
    // InternalQuizDSL.g:783:1: ruleManyOption : ( ( rule__ManyOption__Group__0 ) ) ;
    public final void ruleManyOption() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:787:2: ( ( ( rule__ManyOption__Group__0 ) ) )
            // InternalQuizDSL.g:788:2: ( ( rule__ManyOption__Group__0 ) )
            {
            // InternalQuizDSL.g:788:2: ( ( rule__ManyOption__Group__0 ) )
            // InternalQuizDSL.g:789:3: ( rule__ManyOption__Group__0 )
            {
             before(grammarAccess.getManyOptionAccess().getGroup()); 
            // InternalQuizDSL.g:790:3: ( rule__ManyOption__Group__0 )
            // InternalQuizDSL.g:790:4: rule__ManyOption__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyOption"


    // $ANTLR start "entryRuleXml"
    // InternalQuizDSL.g:799:1: entryRuleXml : ruleXml EOF ;
    public final void entryRuleXml() throws RecognitionException {
        try {
            // InternalQuizDSL.g:800:1: ( ruleXml EOF )
            // InternalQuizDSL.g:801:1: ruleXml EOF
            {
             before(grammarAccess.getXmlRule()); 
            pushFollow(FOLLOW_1);
            ruleXml();

            state._fsp--;

             after(grammarAccess.getXmlRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXml"


    // $ANTLR start "ruleXml"
    // InternalQuizDSL.g:808:1: ruleXml : ( ( rule__Xml__Group__0 ) ) ;
    public final void ruleXml() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:812:2: ( ( ( rule__Xml__Group__0 ) ) )
            // InternalQuizDSL.g:813:2: ( ( rule__Xml__Group__0 ) )
            {
            // InternalQuizDSL.g:813:2: ( ( rule__Xml__Group__0 ) )
            // InternalQuizDSL.g:814:3: ( rule__Xml__Group__0 )
            {
             before(grammarAccess.getXmlAccess().getGroup()); 
            // InternalQuizDSL.g:815:3: ( rule__Xml__Group__0 )
            // InternalQuizDSL.g:815:4: rule__Xml__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Xml__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXml"


    // $ANTLR start "entryRuleXmlContents"
    // InternalQuizDSL.g:824:1: entryRuleXmlContents : ruleXmlContents EOF ;
    public final void entryRuleXmlContents() throws RecognitionException {
        try {
            // InternalQuizDSL.g:825:1: ( ruleXmlContents EOF )
            // InternalQuizDSL.g:826:1: ruleXmlContents EOF
            {
             before(grammarAccess.getXmlContentsRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlContents();

            state._fsp--;

             after(grammarAccess.getXmlContentsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlContents"


    // $ANTLR start "ruleXmlContents"
    // InternalQuizDSL.g:833:1: ruleXmlContents : ( ( rule__XmlContents__Group__0 ) ) ;
    public final void ruleXmlContents() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:837:2: ( ( ( rule__XmlContents__Group__0 ) ) )
            // InternalQuizDSL.g:838:2: ( ( rule__XmlContents__Group__0 ) )
            {
            // InternalQuizDSL.g:838:2: ( ( rule__XmlContents__Group__0 ) )
            // InternalQuizDSL.g:839:3: ( rule__XmlContents__Group__0 )
            {
             before(grammarAccess.getXmlContentsAccess().getGroup()); 
            // InternalQuizDSL.g:840:3: ( rule__XmlContents__Group__0 )
            // InternalQuizDSL.g:840:4: rule__XmlContents__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlContents__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlContentsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlContents"


    // $ANTLR start "entryRuleXmlElement"
    // InternalQuizDSL.g:849:1: entryRuleXmlElement : ruleXmlElement EOF ;
    public final void entryRuleXmlElement() throws RecognitionException {
        try {
            // InternalQuizDSL.g:850:1: ( ruleXmlElement EOF )
            // InternalQuizDSL.g:851:1: ruleXmlElement EOF
            {
             before(grammarAccess.getXmlElementRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlElement();

            state._fsp--;

             after(grammarAccess.getXmlElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlElement"


    // $ANTLR start "ruleXmlElement"
    // InternalQuizDSL.g:858:1: ruleXmlElement : ( ( rule__XmlElement__Alternatives ) ) ;
    public final void ruleXmlElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:862:2: ( ( ( rule__XmlElement__Alternatives ) ) )
            // InternalQuizDSL.g:863:2: ( ( rule__XmlElement__Alternatives ) )
            {
            // InternalQuizDSL.g:863:2: ( ( rule__XmlElement__Alternatives ) )
            // InternalQuizDSL.g:864:3: ( rule__XmlElement__Alternatives )
            {
             before(grammarAccess.getXmlElementAccess().getAlternatives()); 
            // InternalQuizDSL.g:865:3: ( rule__XmlElement__Alternatives )
            // InternalQuizDSL.g:865:4: rule__XmlElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlElement"


    // $ANTLR start "entryRuleXmlPIAnswerElement"
    // InternalQuizDSL.g:874:1: entryRuleXmlPIAnswerElement : ruleXmlPIAnswerElement EOF ;
    public final void entryRuleXmlPIAnswerElement() throws RecognitionException {
        try {
            // InternalQuizDSL.g:875:1: ( ruleXmlPIAnswerElement EOF )
            // InternalQuizDSL.g:876:1: ruleXmlPIAnswerElement EOF
            {
             before(grammarAccess.getXmlPIAnswerElementRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlPIAnswerElement();

            state._fsp--;

             after(grammarAccess.getXmlPIAnswerElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlPIAnswerElement"


    // $ANTLR start "ruleXmlPIAnswerElement"
    // InternalQuizDSL.g:883:1: ruleXmlPIAnswerElement : ( ( rule__XmlPIAnswerElement__Group__0 ) ) ;
    public final void ruleXmlPIAnswerElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:887:2: ( ( ( rule__XmlPIAnswerElement__Group__0 ) ) )
            // InternalQuizDSL.g:888:2: ( ( rule__XmlPIAnswerElement__Group__0 ) )
            {
            // InternalQuizDSL.g:888:2: ( ( rule__XmlPIAnswerElement__Group__0 ) )
            // InternalQuizDSL.g:889:3: ( rule__XmlPIAnswerElement__Group__0 )
            {
             before(grammarAccess.getXmlPIAnswerElementAccess().getGroup()); 
            // InternalQuizDSL.g:890:3: ( rule__XmlPIAnswerElement__Group__0 )
            // InternalQuizDSL.g:890:4: rule__XmlPIAnswerElement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlPIAnswerElement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlPIAnswerElementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlPIAnswerElement"


    // $ANTLR start "entryRuleXmlTagElement"
    // InternalQuizDSL.g:899:1: entryRuleXmlTagElement : ruleXmlTagElement EOF ;
    public final void entryRuleXmlTagElement() throws RecognitionException {
        try {
            // InternalQuizDSL.g:900:1: ( ruleXmlTagElement EOF )
            // InternalQuizDSL.g:901:1: ruleXmlTagElement EOF
            {
             before(grammarAccess.getXmlTagElementRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlTagElement();

            state._fsp--;

             after(grammarAccess.getXmlTagElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlTagElement"


    // $ANTLR start "ruleXmlTagElement"
    // InternalQuizDSL.g:908:1: ruleXmlTagElement : ( ( rule__XmlTagElement__Group__0 ) ) ;
    public final void ruleXmlTagElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:912:2: ( ( ( rule__XmlTagElement__Group__0 ) ) )
            // InternalQuizDSL.g:913:2: ( ( rule__XmlTagElement__Group__0 ) )
            {
            // InternalQuizDSL.g:913:2: ( ( rule__XmlTagElement__Group__0 ) )
            // InternalQuizDSL.g:914:3: ( rule__XmlTagElement__Group__0 )
            {
             before(grammarAccess.getXmlTagElementAccess().getGroup()); 
            // InternalQuizDSL.g:915:3: ( rule__XmlTagElement__Group__0 )
            // InternalQuizDSL.g:915:4: rule__XmlTagElement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagElementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlTagElement"


    // $ANTLR start "entryRuleXmlTag"
    // InternalQuizDSL.g:924:1: entryRuleXmlTag : ruleXmlTag EOF ;
    public final void entryRuleXmlTag() throws RecognitionException {
        try {
            // InternalQuizDSL.g:925:1: ( ruleXmlTag EOF )
            // InternalQuizDSL.g:926:1: ruleXmlTag EOF
            {
             before(grammarAccess.getXmlTagRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlTag();

            state._fsp--;

             after(grammarAccess.getXmlTagRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlTag"


    // $ANTLR start "ruleXmlTag"
    // InternalQuizDSL.g:933:1: ruleXmlTag : ( ( rule__XmlTag__Group__0 ) ) ;
    public final void ruleXmlTag() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:937:2: ( ( ( rule__XmlTag__Group__0 ) ) )
            // InternalQuizDSL.g:938:2: ( ( rule__XmlTag__Group__0 ) )
            {
            // InternalQuizDSL.g:938:2: ( ( rule__XmlTag__Group__0 ) )
            // InternalQuizDSL.g:939:3: ( rule__XmlTag__Group__0 )
            {
             before(grammarAccess.getXmlTagAccess().getGroup()); 
            // InternalQuizDSL.g:940:3: ( rule__XmlTag__Group__0 )
            // InternalQuizDSL.g:940:4: rule__XmlTag__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTag__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlTag"


    // $ANTLR start "entryRuleXmlAttribute"
    // InternalQuizDSL.g:949:1: entryRuleXmlAttribute : ruleXmlAttribute EOF ;
    public final void entryRuleXmlAttribute() throws RecognitionException {
        try {
            // InternalQuizDSL.g:950:1: ( ruleXmlAttribute EOF )
            // InternalQuizDSL.g:951:1: ruleXmlAttribute EOF
            {
             before(grammarAccess.getXmlAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlAttribute();

            state._fsp--;

             after(grammarAccess.getXmlAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlAttribute"


    // $ANTLR start "ruleXmlAttribute"
    // InternalQuizDSL.g:958:1: ruleXmlAttribute : ( ( rule__XmlAttribute__Group__0 ) ) ;
    public final void ruleXmlAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:962:2: ( ( ( rule__XmlAttribute__Group__0 ) ) )
            // InternalQuizDSL.g:963:2: ( ( rule__XmlAttribute__Group__0 ) )
            {
            // InternalQuizDSL.g:963:2: ( ( rule__XmlAttribute__Group__0 ) )
            // InternalQuizDSL.g:964:3: ( rule__XmlAttribute__Group__0 )
            {
             before(grammarAccess.getXmlAttributeAccess().getGroup()); 
            // InternalQuizDSL.g:965:3: ( rule__XmlAttribute__Group__0 )
            // InternalQuizDSL.g:965:4: rule__XmlAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlAttribute"


    // $ANTLR start "rule__Quiz__Alternatives_2"
    // InternalQuizDSL.g:973:1: rule__Quiz__Alternatives_2 : ( ( ( rule__Quiz__Group_2_0__0 ) ) | ( ( rule__Quiz__PartsAssignment_2_1 ) ) );
    public final void rule__Quiz__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:977:1: ( ( ( rule__Quiz__Group_2_0__0 ) ) | ( ( rule__Quiz__PartsAssignment_2_1 ) ) )
            int alt4=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case 20:
                    {
                    alt4=1;
                    }
                    break;
                case RULE_STRING:
                    {
                    switch ( input.LA(3) ) {
                    case 21:
                        {
                        alt4=1;
                        }
                        break;
                    case EOF:
                        {
                        alt4=1;
                        }
                        break;
                    case RULE_INT:
                    case RULE_STRING:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 24:
                    case 26:
                    case 28:
                    case 33:
                        {
                        alt4=2;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 2, input);

                        throw nvae;
                    }

                    }
                    break;
                case 21:
                    {
                    alt4=1;
                    }
                    break;
                case 28:
                    {
                    alt4=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STRING:
                {
                switch ( input.LA(2) ) {
                case 21:
                    {
                    alt4=1;
                    }
                    break;
                case EOF:
                    {
                    alt4=1;
                    }
                    break;
                case RULE_INT:
                case RULE_STRING:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 24:
                case 26:
                case 28:
                case 33:
                    {
                    alt4=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;
                }

                }
                break;
            case 21:
                {
                alt4=1;
                }
                break;
            case EOF:
                {
                alt4=1;
                }
                break;
            case 22:
            case 28:
                {
                alt4=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalQuizDSL.g:978:2: ( ( rule__Quiz__Group_2_0__0 ) )
                    {
                    // InternalQuizDSL.g:978:2: ( ( rule__Quiz__Group_2_0__0 ) )
                    // InternalQuizDSL.g:979:3: ( rule__Quiz__Group_2_0__0 )
                    {
                     before(grammarAccess.getQuizAccess().getGroup_2_0()); 
                    // InternalQuizDSL.g:980:3: ( rule__Quiz__Group_2_0__0 )
                    // InternalQuizDSL.g:980:4: rule__Quiz__Group_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Quiz__Group_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getQuizAccess().getGroup_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:984:2: ( ( rule__Quiz__PartsAssignment_2_1 ) )
                    {
                    // InternalQuizDSL.g:984:2: ( ( rule__Quiz__PartsAssignment_2_1 ) )
                    // InternalQuizDSL.g:985:3: ( rule__Quiz__PartsAssignment_2_1 )
                    {
                     before(grammarAccess.getQuizAccess().getPartsAssignment_2_1()); 
                    // InternalQuizDSL.g:986:3: ( rule__Quiz__PartsAssignment_2_1 )
                    // InternalQuizDSL.g:986:4: rule__Quiz__PartsAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Quiz__PartsAssignment_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getQuizAccess().getPartsAssignment_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Alternatives_2"


    // $ANTLR start "rule__AbstractQuizPart__Alternatives"
    // InternalQuizDSL.g:994:1: rule__AbstractQuizPart__Alternatives : ( ( ruleQuizPart ) | ( ruleQuizPartRef ) );
    public final void rule__AbstractQuizPart__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:998:1: ( ( ruleQuizPart ) | ( ruleQuizPartRef ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==21) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==22) ) {
                    alt5=2;
                }
                else if ( (LA5_1==RULE_ID) ) {
                    alt5=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalQuizDSL.g:999:2: ( ruleQuizPart )
                    {
                    // InternalQuizDSL.g:999:2: ( ruleQuizPart )
                    // InternalQuizDSL.g:1000:3: ruleQuizPart
                    {
                     before(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleQuizPart();

                    state._fsp--;

                     after(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1005:2: ( ruleQuizPartRef )
                    {
                    // InternalQuizDSL.g:1005:2: ( ruleQuizPartRef )
                    // InternalQuizDSL.g:1006:3: ruleQuizPartRef
                    {
                     before(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleQuizPartRef();

                    state._fsp--;

                     after(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractQuizPart__Alternatives"


    // $ANTLR start "rule__AbstractQA__Alternatives"
    // InternalQuizDSL.g:1015:1: rule__AbstractQA__Alternatives : ( ( ruleQA ) | ( ruleQARef ) );
    public final void rule__AbstractQA__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1019:1: ( ( ruleQA ) | ( ruleQARef ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID||LA6_0==RULE_STRING||LA6_0==28) ) {
                alt6=1;
            }
            else if ( (LA6_0==22) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalQuizDSL.g:1020:2: ( ruleQA )
                    {
                    // InternalQuizDSL.g:1020:2: ( ruleQA )
                    // InternalQuizDSL.g:1021:3: ruleQA
                    {
                     before(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleQA();

                    state._fsp--;

                     after(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1026:2: ( ruleQARef )
                    {
                    // InternalQuizDSL.g:1026:2: ( ruleQARef )
                    // InternalQuizDSL.g:1027:3: ruleQARef
                    {
                     before(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleQARef();

                    state._fsp--;

                     after(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractQA__Alternatives"


    // $ANTLR start "rule__Question__Alternatives"
    // InternalQuizDSL.g:1036:1: rule__Question__Alternatives : ( ( ruleStringQuestion ) | ( ruleXmlQuestion ) );
    public final void rule__Question__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1040:1: ( ( ruleStringQuestion ) | ( ruleXmlQuestion ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==28) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalQuizDSL.g:1041:2: ( ruleStringQuestion )
                    {
                    // InternalQuizDSL.g:1041:2: ( ruleStringQuestion )
                    // InternalQuizDSL.g:1042:3: ruleStringQuestion
                    {
                     before(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStringQuestion();

                    state._fsp--;

                     after(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1047:2: ( ruleXmlQuestion )
                    {
                    // InternalQuizDSL.g:1047:2: ( ruleXmlQuestion )
                    // InternalQuizDSL.g:1048:3: ruleXmlQuestion
                    {
                     before(grammarAccess.getQuestionAccess().getXmlQuestionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlQuestion();

                    state._fsp--;

                     after(grammarAccess.getQuestionAccess().getXmlQuestionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Alternatives"


    // $ANTLR start "rule__Answer__Alternatives"
    // InternalQuizDSL.g:1057:1: rule__Answer__Alternatives : ( ( ruleOptionAnswer ) | ( ruleOptionsAnswer ) );
    public final void rule__Answer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1061:1: ( ( ruleOptionAnswer ) | ( ruleOptionsAnswer ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( ((LA8_0>=RULE_INT && LA8_0<=RULE_STRING)||(LA8_0>=12 && LA8_0<=15)||LA8_0==18||LA8_0==28) ) {
                alt8=1;
            }
            else if ( ((LA8_0>=16 && LA8_0<=17)||LA8_0==24||LA8_0==26||LA8_0==33) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalQuizDSL.g:1062:2: ( ruleOptionAnswer )
                    {
                    // InternalQuizDSL.g:1062:2: ( ruleOptionAnswer )
                    // InternalQuizDSL.g:1063:3: ruleOptionAnswer
                    {
                     before(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionAnswer();

                    state._fsp--;

                     after(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1068:2: ( ruleOptionsAnswer )
                    {
                    // InternalQuizDSL.g:1068:2: ( ruleOptionsAnswer )
                    // InternalQuizDSL.g:1069:3: ruleOptionsAnswer
                    {
                     before(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Answer__Alternatives"


    // $ANTLR start "rule__OptionAnswer__Alternatives"
    // InternalQuizDSL.g:1078:1: rule__OptionAnswer__Alternatives : ( ( ruleSimpleAnswer ) | ( ruleXmlAnswer ) );
    public final void rule__OptionAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1082:1: ( ( ruleSimpleAnswer ) | ( ruleXmlAnswer ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=RULE_INT && LA9_0<=RULE_STRING)||(LA9_0>=12 && LA9_0<=15)||LA9_0==18) ) {
                alt9=1;
            }
            else if ( (LA9_0==28) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalQuizDSL.g:1083:2: ( ruleSimpleAnswer )
                    {
                    // InternalQuizDSL.g:1083:2: ( ruleSimpleAnswer )
                    // InternalQuizDSL.g:1084:3: ruleSimpleAnswer
                    {
                     before(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSimpleAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1089:2: ( ruleXmlAnswer )
                    {
                    // InternalQuizDSL.g:1089:2: ( ruleXmlAnswer )
                    // InternalQuizDSL.g:1090:3: ruleXmlAnswer
                    {
                     before(grammarAccess.getOptionAnswerAccess().getXmlAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionAnswerAccess().getXmlAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionAnswer__Alternatives"


    // $ANTLR start "rule__SimpleAnswer__Alternatives"
    // InternalQuizDSL.g:1099:1: rule__SimpleAnswer__Alternatives : ( ( ruleStringAnswer ) | ( ruleRegexAnswer ) | ( ruleNumberAnswer ) | ( ruleBooleanAnswer ) );
    public final void rule__SimpleAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1103:1: ( ( ruleStringAnswer ) | ( ruleRegexAnswer ) | ( ruleNumberAnswer ) | ( ruleBooleanAnswer ) )
            int alt10=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt10=1;
                }
                break;
            case 18:
                {
                alt10=2;
                }
                break;
            case RULE_INT:
                {
                alt10=3;
                }
                break;
            case 12:
            case 13:
            case 14:
            case 15:
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalQuizDSL.g:1104:2: ( ruleStringAnswer )
                    {
                    // InternalQuizDSL.g:1104:2: ( ruleStringAnswer )
                    // InternalQuizDSL.g:1105:3: ruleStringAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStringAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1110:2: ( ruleRegexAnswer )
                    {
                    // InternalQuizDSL.g:1110:2: ( ruleRegexAnswer )
                    // InternalQuizDSL.g:1111:3: ruleRegexAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getRegexAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleRegexAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getRegexAnswerParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalQuizDSL.g:1116:2: ( ruleNumberAnswer )
                    {
                    // InternalQuizDSL.g:1116:2: ( ruleNumberAnswer )
                    // InternalQuizDSL.g:1117:3: ruleNumberAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNumberAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalQuizDSL.g:1122:2: ( ruleBooleanAnswer )
                    {
                    // InternalQuizDSL.g:1122:2: ( ruleBooleanAnswer )
                    // InternalQuizDSL.g:1123:3: ruleBooleanAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleAnswer__Alternatives"


    // $ANTLR start "rule__BooleanAnswer__Alternatives_1"
    // InternalQuizDSL.g:1132:1: rule__BooleanAnswer__Alternatives_1 : ( ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) ) | ( ( rule__BooleanAnswer__Alternatives_1_1 ) ) );
    public final void rule__BooleanAnswer__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1136:1: ( ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) ) | ( ( rule__BooleanAnswer__Alternatives_1_1 ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( ((LA11_0>=12 && LA11_0<=13)) ) {
                alt11=1;
            }
            else if ( ((LA11_0>=14 && LA11_0<=15)) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalQuizDSL.g:1137:2: ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) )
                    {
                    // InternalQuizDSL.g:1137:2: ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) )
                    // InternalQuizDSL.g:1138:3: ( rule__BooleanAnswer__ValueAssignment_1_0 )
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getValueAssignment_1_0()); 
                    // InternalQuizDSL.g:1139:3: ( rule__BooleanAnswer__ValueAssignment_1_0 )
                    // InternalQuizDSL.g:1139:4: rule__BooleanAnswer__ValueAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanAnswer__ValueAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanAnswerAccess().getValueAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1143:2: ( ( rule__BooleanAnswer__Alternatives_1_1 ) )
                    {
                    // InternalQuizDSL.g:1143:2: ( ( rule__BooleanAnswer__Alternatives_1_1 ) )
                    // InternalQuizDSL.g:1144:3: ( rule__BooleanAnswer__Alternatives_1_1 )
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getAlternatives_1_1()); 
                    // InternalQuizDSL.g:1145:3: ( rule__BooleanAnswer__Alternatives_1_1 )
                    // InternalQuizDSL.g:1145:4: rule__BooleanAnswer__Alternatives_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanAnswer__Alternatives_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanAnswerAccess().getAlternatives_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Alternatives_1"


    // $ANTLR start "rule__BooleanAnswer__ValueAlternatives_1_0_0"
    // InternalQuizDSL.g:1153:1: rule__BooleanAnswer__ValueAlternatives_1_0_0 : ( ( 'yes' ) | ( 'true' ) );
    public final void rule__BooleanAnswer__ValueAlternatives_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1157:1: ( ( 'yes' ) | ( 'true' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==12) ) {
                alt12=1;
            }
            else if ( (LA12_0==13) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalQuizDSL.g:1158:2: ( 'yes' )
                    {
                    // InternalQuizDSL.g:1158:2: ( 'yes' )
                    // InternalQuizDSL.g:1159:3: 'yes'
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1164:2: ( 'true' )
                    {
                    // InternalQuizDSL.g:1164:2: ( 'true' )
                    // InternalQuizDSL.g:1165:3: 'true'
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getValueTrueKeyword_1_0_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getBooleanAnswerAccess().getValueTrueKeyword_1_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__ValueAlternatives_1_0_0"


    // $ANTLR start "rule__BooleanAnswer__Alternatives_1_1"
    // InternalQuizDSL.g:1174:1: rule__BooleanAnswer__Alternatives_1_1 : ( ( 'no' ) | ( 'false' ) );
    public final void rule__BooleanAnswer__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1178:1: ( ( 'no' ) | ( 'false' ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==14) ) {
                alt13=1;
            }
            else if ( (LA13_0==15) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalQuizDSL.g:1179:2: ( 'no' )
                    {
                    // InternalQuizDSL.g:1179:2: ( 'no' )
                    // InternalQuizDSL.g:1180:3: 'no'
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1_0()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1185:2: ( 'false' )
                    {
                    // InternalQuizDSL.g:1185:2: ( 'false' )
                    // InternalQuizDSL.g:1186:3: 'false'
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getFalseKeyword_1_1_1()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getBooleanAnswerAccess().getFalseKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Alternatives_1_1"


    // $ANTLR start "rule__OptionsAnswer__Alternatives"
    // InternalQuizDSL.g:1195:1: rule__OptionsAnswer__Alternatives : ( ( ruleSingleOptionsAnswer ) | ( ruleManyOptionsAnswer ) );
    public final void rule__OptionsAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1199:1: ( ( ruleSingleOptionsAnswer ) | ( ruleManyOptionsAnswer ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=16 && LA14_0<=17)||LA14_0==24||LA14_0==33) ) {
                alt14=1;
            }
            else if ( (LA14_0==26) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalQuizDSL.g:1200:2: ( ruleSingleOptionsAnswer )
                    {
                    // InternalQuizDSL.g:1200:2: ( ruleSingleOptionsAnswer )
                    // InternalQuizDSL.g:1201:3: ruleSingleOptionsAnswer
                    {
                     before(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSingleOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1206:2: ( ruleManyOptionsAnswer )
                    {
                    // InternalQuizDSL.g:1206:2: ( ruleManyOptionsAnswer )
                    // InternalQuizDSL.g:1207:3: ruleManyOptionsAnswer
                    {
                     before(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleManyOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionsAnswer__Alternatives"


    // $ANTLR start "rule__SingleOptionsAnswer__Alternatives"
    // InternalQuizDSL.g:1216:1: rule__SingleOptionsAnswer__Alternatives : ( ( ruleSingleBoxOptionsAnswer ) | ( ruleSingleListOptionsAnswer ) );
    public final void rule__SingleOptionsAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1220:1: ( ( ruleSingleBoxOptionsAnswer ) | ( ruleSingleListOptionsAnswer ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==24) ) {
                alt15=1;
            }
            else if ( ((LA15_0>=16 && LA15_0<=17)||LA15_0==33) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalQuizDSL.g:1221:2: ( ruleSingleBoxOptionsAnswer )
                    {
                    // InternalQuizDSL.g:1221:2: ( ruleSingleBoxOptionsAnswer )
                    // InternalQuizDSL.g:1222:3: ruleSingleBoxOptionsAnswer
                    {
                     before(grammarAccess.getSingleOptionsAnswerAccess().getSingleBoxOptionsAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSingleBoxOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getSingleOptionsAnswerAccess().getSingleBoxOptionsAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1227:2: ( ruleSingleListOptionsAnswer )
                    {
                    // InternalQuizDSL.g:1227:2: ( ruleSingleListOptionsAnswer )
                    // InternalQuizDSL.g:1228:3: ruleSingleListOptionsAnswer
                    {
                     before(grammarAccess.getSingleOptionsAnswerAccess().getSingleListOptionsAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSingleListOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getSingleOptionsAnswerAccess().getSingleListOptionsAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleOptionsAnswer__Alternatives"


    // $ANTLR start "rule__SingleListOption__Alternatives_0"
    // InternalQuizDSL.g:1237:1: rule__SingleListOption__Alternatives_0 : ( ( ( rule__SingleListOption__Alternatives_0_0 ) ) | ( ( rule__SingleListOption__CorrectAssignment_0_1 ) ) );
    public final void rule__SingleListOption__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1241:1: ( ( ( rule__SingleListOption__Alternatives_0_0 ) ) | ( ( rule__SingleListOption__CorrectAssignment_0_1 ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=16 && LA16_0<=17)) ) {
                alt16=1;
            }
            else if ( (LA16_0==33) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalQuizDSL.g:1242:2: ( ( rule__SingleListOption__Alternatives_0_0 ) )
                    {
                    // InternalQuizDSL.g:1242:2: ( ( rule__SingleListOption__Alternatives_0_0 ) )
                    // InternalQuizDSL.g:1243:3: ( rule__SingleListOption__Alternatives_0_0 )
                    {
                     before(grammarAccess.getSingleListOptionAccess().getAlternatives_0_0()); 
                    // InternalQuizDSL.g:1244:3: ( rule__SingleListOption__Alternatives_0_0 )
                    // InternalQuizDSL.g:1244:4: rule__SingleListOption__Alternatives_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SingleListOption__Alternatives_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSingleListOptionAccess().getAlternatives_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1248:2: ( ( rule__SingleListOption__CorrectAssignment_0_1 ) )
                    {
                    // InternalQuizDSL.g:1248:2: ( ( rule__SingleListOption__CorrectAssignment_0_1 ) )
                    // InternalQuizDSL.g:1249:3: ( rule__SingleListOption__CorrectAssignment_0_1 )
                    {
                     before(grammarAccess.getSingleListOptionAccess().getCorrectAssignment_0_1()); 
                    // InternalQuizDSL.g:1250:3: ( rule__SingleListOption__CorrectAssignment_0_1 )
                    // InternalQuizDSL.g:1250:4: rule__SingleListOption__CorrectAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SingleListOption__CorrectAssignment_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSingleListOptionAccess().getCorrectAssignment_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Alternatives_0"


    // $ANTLR start "rule__SingleListOption__Alternatives_0_0"
    // InternalQuizDSL.g:1258:1: rule__SingleListOption__Alternatives_0_0 : ( ( '-' ) | ( 'x' ) );
    public final void rule__SingleListOption__Alternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1262:1: ( ( '-' ) | ( 'x' ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==16) ) {
                alt17=1;
            }
            else if ( (LA17_0==17) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalQuizDSL.g:1263:2: ( '-' )
                    {
                    // InternalQuizDSL.g:1263:2: ( '-' )
                    // InternalQuizDSL.g:1264:3: '-'
                    {
                     before(grammarAccess.getSingleListOptionAccess().getHyphenMinusKeyword_0_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getSingleListOptionAccess().getHyphenMinusKeyword_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1269:2: ( 'x' )
                    {
                    // InternalQuizDSL.g:1269:2: ( 'x' )
                    // InternalQuizDSL.g:1270:3: 'x'
                    {
                     before(grammarAccess.getSingleListOptionAccess().getXKeyword_0_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getSingleListOptionAccess().getXKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Alternatives_0_0"


    // $ANTLR start "rule__XmlElement__Alternatives"
    // InternalQuizDSL.g:1279:1: rule__XmlElement__Alternatives : ( ( ruleXmlPIAnswerElement ) | ( ruleXmlTagElement ) );
    public final void rule__XmlElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1283:1: ( ( ruleXmlPIAnswerElement ) | ( ruleXmlTagElement ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==30) ) {
                alt18=1;
            }
            else if ( (LA18_0==RULE_ID) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalQuizDSL.g:1284:2: ( ruleXmlPIAnswerElement )
                    {
                    // InternalQuizDSL.g:1284:2: ( ruleXmlPIAnswerElement )
                    // InternalQuizDSL.g:1285:3: ruleXmlPIAnswerElement
                    {
                     before(grammarAccess.getXmlElementAccess().getXmlPIAnswerElementParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlPIAnswerElement();

                    state._fsp--;

                     after(grammarAccess.getXmlElementAccess().getXmlPIAnswerElementParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1290:2: ( ruleXmlTagElement )
                    {
                    // InternalQuizDSL.g:1290:2: ( ruleXmlTagElement )
                    // InternalQuizDSL.g:1291:3: ruleXmlTagElement
                    {
                     before(grammarAccess.getXmlElementAccess().getXmlTagElementParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlTagElement();

                    state._fsp--;

                     after(grammarAccess.getXmlElementAccess().getXmlTagElementParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Alternatives"


    // $ANTLR start "rule__XmlTagElement__Alternatives_1"
    // InternalQuizDSL.g:1300:1: rule__XmlTagElement__Alternatives_1 : ( ( '/' ) | ( ( rule__XmlTagElement__Group_1_1__0 ) ) );
    public final void rule__XmlTagElement__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1304:1: ( ( '/' ) | ( ( rule__XmlTagElement__Group_1_1__0 ) ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==18) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_XML_TEXT) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalQuizDSL.g:1305:2: ( '/' )
                    {
                    // InternalQuizDSL.g:1305:2: ( '/' )
                    // InternalQuizDSL.g:1306:3: '/'
                    {
                     before(grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_0()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQuizDSL.g:1311:2: ( ( rule__XmlTagElement__Group_1_1__0 ) )
                    {
                    // InternalQuizDSL.g:1311:2: ( ( rule__XmlTagElement__Group_1_1__0 ) )
                    // InternalQuizDSL.g:1312:3: ( rule__XmlTagElement__Group_1_1__0 )
                    {
                     before(grammarAccess.getXmlTagElementAccess().getGroup_1_1()); 
                    // InternalQuizDSL.g:1313:3: ( rule__XmlTagElement__Group_1_1__0 )
                    // InternalQuizDSL.g:1313:4: rule__XmlTagElement__Group_1_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlTagElement__Group_1_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getXmlTagElementAccess().getGroup_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Alternatives_1"


    // $ANTLR start "rule__Quiz__Group__0"
    // InternalQuizDSL.g:1321:1: rule__Quiz__Group__0 : rule__Quiz__Group__0__Impl rule__Quiz__Group__1 ;
    public final void rule__Quiz__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1325:1: ( rule__Quiz__Group__0__Impl rule__Quiz__Group__1 )
            // InternalQuizDSL.g:1326:2: rule__Quiz__Group__0__Impl rule__Quiz__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Quiz__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__0"


    // $ANTLR start "rule__Quiz__Group__0__Impl"
    // InternalQuizDSL.g:1333:1: rule__Quiz__Group__0__Impl : ( () ) ;
    public final void rule__Quiz__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1337:1: ( ( () ) )
            // InternalQuizDSL.g:1338:1: ( () )
            {
            // InternalQuizDSL.g:1338:1: ( () )
            // InternalQuizDSL.g:1339:2: ()
            {
             before(grammarAccess.getQuizAccess().getQuizAction_0()); 
            // InternalQuizDSL.g:1340:2: ()
            // InternalQuizDSL.g:1340:3: 
            {
            }

             after(grammarAccess.getQuizAccess().getQuizAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__0__Impl"


    // $ANTLR start "rule__Quiz__Group__1"
    // InternalQuizDSL.g:1348:1: rule__Quiz__Group__1 : rule__Quiz__Group__1__Impl rule__Quiz__Group__2 ;
    public final void rule__Quiz__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1352:1: ( rule__Quiz__Group__1__Impl rule__Quiz__Group__2 )
            // InternalQuizDSL.g:1353:2: rule__Quiz__Group__1__Impl rule__Quiz__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Quiz__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__1"


    // $ANTLR start "rule__Quiz__Group__1__Impl"
    // InternalQuizDSL.g:1360:1: rule__Quiz__Group__1__Impl : ( 'quiz' ) ;
    public final void rule__Quiz__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1364:1: ( ( 'quiz' ) )
            // InternalQuizDSL.g:1365:1: ( 'quiz' )
            {
            // InternalQuizDSL.g:1365:1: ( 'quiz' )
            // InternalQuizDSL.g:1366:2: 'quiz'
            {
             before(grammarAccess.getQuizAccess().getQuizKeyword_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getQuizAccess().getQuizKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__1__Impl"


    // $ANTLR start "rule__Quiz__Group__2"
    // InternalQuizDSL.g:1375:1: rule__Quiz__Group__2 : rule__Quiz__Group__2__Impl ;
    public final void rule__Quiz__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1379:1: ( rule__Quiz__Group__2__Impl )
            // InternalQuizDSL.g:1380:2: rule__Quiz__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__2"


    // $ANTLR start "rule__Quiz__Group__2__Impl"
    // InternalQuizDSL.g:1386:1: rule__Quiz__Group__2__Impl : ( ( rule__Quiz__Alternatives_2 ) ) ;
    public final void rule__Quiz__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1390:1: ( ( ( rule__Quiz__Alternatives_2 ) ) )
            // InternalQuizDSL.g:1391:1: ( ( rule__Quiz__Alternatives_2 ) )
            {
            // InternalQuizDSL.g:1391:1: ( ( rule__Quiz__Alternatives_2 ) )
            // InternalQuizDSL.g:1392:2: ( rule__Quiz__Alternatives_2 )
            {
             before(grammarAccess.getQuizAccess().getAlternatives_2()); 
            // InternalQuizDSL.g:1393:2: ( rule__Quiz__Alternatives_2 )
            // InternalQuizDSL.g:1393:3: rule__Quiz__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getQuizAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__2__Impl"


    // $ANTLR start "rule__Quiz__Group_2_0__0"
    // InternalQuizDSL.g:1402:1: rule__Quiz__Group_2_0__0 : rule__Quiz__Group_2_0__0__Impl rule__Quiz__Group_2_0__1 ;
    public final void rule__Quiz__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1406:1: ( rule__Quiz__Group_2_0__0__Impl rule__Quiz__Group_2_0__1 )
            // InternalQuizDSL.g:1407:2: rule__Quiz__Group_2_0__0__Impl rule__Quiz__Group_2_0__1
            {
            pushFollow(FOLLOW_8);
            rule__Quiz__Group_2_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__0"


    // $ANTLR start "rule__Quiz__Group_2_0__0__Impl"
    // InternalQuizDSL.g:1414:1: rule__Quiz__Group_2_0__0__Impl : ( ( rule__Quiz__NameAssignment_2_0_0 )? ) ;
    public final void rule__Quiz__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1418:1: ( ( ( rule__Quiz__NameAssignment_2_0_0 )? ) )
            // InternalQuizDSL.g:1419:1: ( ( rule__Quiz__NameAssignment_2_0_0 )? )
            {
            // InternalQuizDSL.g:1419:1: ( ( rule__Quiz__NameAssignment_2_0_0 )? )
            // InternalQuizDSL.g:1420:2: ( rule__Quiz__NameAssignment_2_0_0 )?
            {
             before(grammarAccess.getQuizAccess().getNameAssignment_2_0_0()); 
            // InternalQuizDSL.g:1421:2: ( rule__Quiz__NameAssignment_2_0_0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_ID) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalQuizDSL.g:1421:3: rule__Quiz__NameAssignment_2_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Quiz__NameAssignment_2_0_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuizAccess().getNameAssignment_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__0__Impl"


    // $ANTLR start "rule__Quiz__Group_2_0__1"
    // InternalQuizDSL.g:1429:1: rule__Quiz__Group_2_0__1 : rule__Quiz__Group_2_0__1__Impl rule__Quiz__Group_2_0__2 ;
    public final void rule__Quiz__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1433:1: ( rule__Quiz__Group_2_0__1__Impl rule__Quiz__Group_2_0__2 )
            // InternalQuizDSL.g:1434:2: rule__Quiz__Group_2_0__1__Impl rule__Quiz__Group_2_0__2
            {
            pushFollow(FOLLOW_8);
            rule__Quiz__Group_2_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group_2_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__1"


    // $ANTLR start "rule__Quiz__Group_2_0__1__Impl"
    // InternalQuizDSL.g:1441:1: rule__Quiz__Group_2_0__1__Impl : ( ( rule__Quiz__TitleAssignment_2_0_1 )? ) ;
    public final void rule__Quiz__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1445:1: ( ( ( rule__Quiz__TitleAssignment_2_0_1 )? ) )
            // InternalQuizDSL.g:1446:1: ( ( rule__Quiz__TitleAssignment_2_0_1 )? )
            {
            // InternalQuizDSL.g:1446:1: ( ( rule__Quiz__TitleAssignment_2_0_1 )? )
            // InternalQuizDSL.g:1447:2: ( rule__Quiz__TitleAssignment_2_0_1 )?
            {
             before(grammarAccess.getQuizAccess().getTitleAssignment_2_0_1()); 
            // InternalQuizDSL.g:1448:2: ( rule__Quiz__TitleAssignment_2_0_1 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_STRING) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalQuizDSL.g:1448:3: rule__Quiz__TitleAssignment_2_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Quiz__TitleAssignment_2_0_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuizAccess().getTitleAssignment_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__1__Impl"


    // $ANTLR start "rule__Quiz__Group_2_0__2"
    // InternalQuizDSL.g:1456:1: rule__Quiz__Group_2_0__2 : rule__Quiz__Group_2_0__2__Impl ;
    public final void rule__Quiz__Group_2_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1460:1: ( rule__Quiz__Group_2_0__2__Impl )
            // InternalQuizDSL.g:1461:2: rule__Quiz__Group_2_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Group_2_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__2"


    // $ANTLR start "rule__Quiz__Group_2_0__2__Impl"
    // InternalQuizDSL.g:1467:1: rule__Quiz__Group_2_0__2__Impl : ( ( rule__Quiz__PartsAssignment_2_0_2 )* ) ;
    public final void rule__Quiz__Group_2_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1471:1: ( ( ( rule__Quiz__PartsAssignment_2_0_2 )* ) )
            // InternalQuizDSL.g:1472:1: ( ( rule__Quiz__PartsAssignment_2_0_2 )* )
            {
            // InternalQuizDSL.g:1472:1: ( ( rule__Quiz__PartsAssignment_2_0_2 )* )
            // InternalQuizDSL.g:1473:2: ( rule__Quiz__PartsAssignment_2_0_2 )*
            {
             before(grammarAccess.getQuizAccess().getPartsAssignment_2_0_2()); 
            // InternalQuizDSL.g:1474:2: ( rule__Quiz__PartsAssignment_2_0_2 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==21) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalQuizDSL.g:1474:3: rule__Quiz__PartsAssignment_2_0_2
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Quiz__PartsAssignment_2_0_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getQuizAccess().getPartsAssignment_2_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group_2_0__2__Impl"


    // $ANTLR start "rule__QName__Group__0"
    // InternalQuizDSL.g:1483:1: rule__QName__Group__0 : rule__QName__Group__0__Impl rule__QName__Group__1 ;
    public final void rule__QName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1487:1: ( rule__QName__Group__0__Impl rule__QName__Group__1 )
            // InternalQuizDSL.g:1488:2: rule__QName__Group__0__Impl rule__QName__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__QName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__0"


    // $ANTLR start "rule__QName__Group__0__Impl"
    // InternalQuizDSL.g:1495:1: rule__QName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1499:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:1500:1: ( RULE_ID )
            {
            // InternalQuizDSL.g:1500:1: ( RULE_ID )
            // InternalQuizDSL.g:1501:2: RULE_ID
            {
             before(grammarAccess.getQNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__0__Impl"


    // $ANTLR start "rule__QName__Group__1"
    // InternalQuizDSL.g:1510:1: rule__QName__Group__1 : rule__QName__Group__1__Impl ;
    public final void rule__QName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1514:1: ( rule__QName__Group__1__Impl )
            // InternalQuizDSL.g:1515:2: rule__QName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__1"


    // $ANTLR start "rule__QName__Group__1__Impl"
    // InternalQuizDSL.g:1521:1: rule__QName__Group__1__Impl : ( ( rule__QName__Group_1__0 )* ) ;
    public final void rule__QName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1525:1: ( ( ( rule__QName__Group_1__0 )* ) )
            // InternalQuizDSL.g:1526:1: ( ( rule__QName__Group_1__0 )* )
            {
            // InternalQuizDSL.g:1526:1: ( ( rule__QName__Group_1__0 )* )
            // InternalQuizDSL.g:1527:2: ( rule__QName__Group_1__0 )*
            {
             before(grammarAccess.getQNameAccess().getGroup_1()); 
            // InternalQuizDSL.g:1528:2: ( rule__QName__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==20) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalQuizDSL.g:1528:3: rule__QName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__QName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getQNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__1__Impl"


    // $ANTLR start "rule__QName__Group_1__0"
    // InternalQuizDSL.g:1537:1: rule__QName__Group_1__0 : rule__QName__Group_1__0__Impl rule__QName__Group_1__1 ;
    public final void rule__QName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1541:1: ( rule__QName__Group_1__0__Impl rule__QName__Group_1__1 )
            // InternalQuizDSL.g:1542:2: rule__QName__Group_1__0__Impl rule__QName__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__QName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__0"


    // $ANTLR start "rule__QName__Group_1__0__Impl"
    // InternalQuizDSL.g:1549:1: rule__QName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1553:1: ( ( '.' ) )
            // InternalQuizDSL.g:1554:1: ( '.' )
            {
            // InternalQuizDSL.g:1554:1: ( '.' )
            // InternalQuizDSL.g:1555:2: '.'
            {
             before(grammarAccess.getQNameAccess().getFullStopKeyword_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__0__Impl"


    // $ANTLR start "rule__QName__Group_1__1"
    // InternalQuizDSL.g:1564:1: rule__QName__Group_1__1 : rule__QName__Group_1__1__Impl ;
    public final void rule__QName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1568:1: ( rule__QName__Group_1__1__Impl )
            // InternalQuizDSL.g:1569:2: rule__QName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__1"


    // $ANTLR start "rule__QName__Group_1__1__Impl"
    // InternalQuizDSL.g:1575:1: rule__QName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1579:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:1580:1: ( RULE_ID )
            {
            // InternalQuizDSL.g:1580:1: ( RULE_ID )
            // InternalQuizDSL.g:1581:2: RULE_ID
            {
             before(grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__1__Impl"


    // $ANTLR start "rule__QuizPart__Group__0"
    // InternalQuizDSL.g:1591:1: rule__QuizPart__Group__0 : rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1 ;
    public final void rule__QuizPart__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1595:1: ( rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1 )
            // InternalQuizDSL.g:1596:2: rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__QuizPart__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__0"


    // $ANTLR start "rule__QuizPart__Group__0__Impl"
    // InternalQuizDSL.g:1603:1: rule__QuizPart__Group__0__Impl : ( 'part' ) ;
    public final void rule__QuizPart__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1607:1: ( ( 'part' ) )
            // InternalQuizDSL.g:1608:1: ( 'part' )
            {
            // InternalQuizDSL.g:1608:1: ( 'part' )
            // InternalQuizDSL.g:1609:2: 'part'
            {
             before(grammarAccess.getQuizPartAccess().getPartKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getPartKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__0__Impl"


    // $ANTLR start "rule__QuizPart__Group__1"
    // InternalQuizDSL.g:1618:1: rule__QuizPart__Group__1 : rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2 ;
    public final void rule__QuizPart__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1622:1: ( rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2 )
            // InternalQuizDSL.g:1623:2: rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__QuizPart__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__1"


    // $ANTLR start "rule__QuizPart__Group__1__Impl"
    // InternalQuizDSL.g:1630:1: rule__QuizPart__Group__1__Impl : ( ( rule__QuizPart__NameAssignment_1 ) ) ;
    public final void rule__QuizPart__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1634:1: ( ( ( rule__QuizPart__NameAssignment_1 ) ) )
            // InternalQuizDSL.g:1635:1: ( ( rule__QuizPart__NameAssignment_1 ) )
            {
            // InternalQuizDSL.g:1635:1: ( ( rule__QuizPart__NameAssignment_1 ) )
            // InternalQuizDSL.g:1636:2: ( rule__QuizPart__NameAssignment_1 )
            {
             before(grammarAccess.getQuizPartAccess().getNameAssignment_1()); 
            // InternalQuizDSL.g:1637:2: ( rule__QuizPart__NameAssignment_1 )
            // InternalQuizDSL.g:1637:3: rule__QuizPart__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__1__Impl"


    // $ANTLR start "rule__QuizPart__Group__2"
    // InternalQuizDSL.g:1645:1: rule__QuizPart__Group__2 : rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3 ;
    public final void rule__QuizPart__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1649:1: ( rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3 )
            // InternalQuizDSL.g:1650:2: rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__QuizPart__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__2"


    // $ANTLR start "rule__QuizPart__Group__2__Impl"
    // InternalQuizDSL.g:1657:1: rule__QuizPart__Group__2__Impl : ( ( rule__QuizPart__TitleAssignment_2 ) ) ;
    public final void rule__QuizPart__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1661:1: ( ( ( rule__QuizPart__TitleAssignment_2 ) ) )
            // InternalQuizDSL.g:1662:1: ( ( rule__QuizPart__TitleAssignment_2 ) )
            {
            // InternalQuizDSL.g:1662:1: ( ( rule__QuizPart__TitleAssignment_2 ) )
            // InternalQuizDSL.g:1663:2: ( rule__QuizPart__TitleAssignment_2 )
            {
             before(grammarAccess.getQuizPartAccess().getTitleAssignment_2()); 
            // InternalQuizDSL.g:1664:2: ( rule__QuizPart__TitleAssignment_2 )
            // InternalQuizDSL.g:1664:3: rule__QuizPart__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__TitleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__2__Impl"


    // $ANTLR start "rule__QuizPart__Group__3"
    // InternalQuizDSL.g:1672:1: rule__QuizPart__Group__3 : rule__QuizPart__Group__3__Impl ;
    public final void rule__QuizPart__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1676:1: ( rule__QuizPart__Group__3__Impl )
            // InternalQuizDSL.g:1677:2: rule__QuizPart__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__3"


    // $ANTLR start "rule__QuizPart__Group__3__Impl"
    // InternalQuizDSL.g:1683:1: rule__QuizPart__Group__3__Impl : ( ( rule__QuizPart__QuestionsAssignment_3 )* ) ;
    public final void rule__QuizPart__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1687:1: ( ( ( rule__QuizPart__QuestionsAssignment_3 )* ) )
            // InternalQuizDSL.g:1688:1: ( ( rule__QuizPart__QuestionsAssignment_3 )* )
            {
            // InternalQuizDSL.g:1688:1: ( ( rule__QuizPart__QuestionsAssignment_3 )* )
            // InternalQuizDSL.g:1689:2: ( rule__QuizPart__QuestionsAssignment_3 )*
            {
             before(grammarAccess.getQuizPartAccess().getQuestionsAssignment_3()); 
            // InternalQuizDSL.g:1690:2: ( rule__QuizPart__QuestionsAssignment_3 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID||LA24_0==RULE_STRING||LA24_0==22||LA24_0==28) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalQuizDSL.g:1690:3: rule__QuizPart__QuestionsAssignment_3
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__QuizPart__QuestionsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getQuizPartAccess().getQuestionsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__3__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__0"
    // InternalQuizDSL.g:1699:1: rule__QuizPartRef__Group__0 : rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1 ;
    public final void rule__QuizPartRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1703:1: ( rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1 )
            // InternalQuizDSL.g:1704:2: rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__QuizPartRef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__0"


    // $ANTLR start "rule__QuizPartRef__Group__0__Impl"
    // InternalQuizDSL.g:1711:1: rule__QuizPartRef__Group__0__Impl : ( 'part' ) ;
    public final void rule__QuizPartRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1715:1: ( ( 'part' ) )
            // InternalQuizDSL.g:1716:1: ( 'part' )
            {
            // InternalQuizDSL.g:1716:1: ( 'part' )
            // InternalQuizDSL.g:1717:2: 'part'
            {
             before(grammarAccess.getQuizPartRefAccess().getPartKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getQuizPartRefAccess().getPartKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__0__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__1"
    // InternalQuizDSL.g:1726:1: rule__QuizPartRef__Group__1 : rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2 ;
    public final void rule__QuizPartRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1730:1: ( rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2 )
            // InternalQuizDSL.g:1731:2: rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__QuizPartRef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__1"


    // $ANTLR start "rule__QuizPartRef__Group__1__Impl"
    // InternalQuizDSL.g:1738:1: rule__QuizPartRef__Group__1__Impl : ( 'ref' ) ;
    public final void rule__QuizPartRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1742:1: ( ( 'ref' ) )
            // InternalQuizDSL.g:1743:1: ( 'ref' )
            {
            // InternalQuizDSL.g:1743:1: ( 'ref' )
            // InternalQuizDSL.g:1744:2: 'ref'
            {
             before(grammarAccess.getQuizPartRefAccess().getRefKeyword_1()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getQuizPartRefAccess().getRefKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__1__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__2"
    // InternalQuizDSL.g:1753:1: rule__QuizPartRef__Group__2 : rule__QuizPartRef__Group__2__Impl ;
    public final void rule__QuizPartRef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1757:1: ( rule__QuizPartRef__Group__2__Impl )
            // InternalQuizDSL.g:1758:2: rule__QuizPartRef__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__2"


    // $ANTLR start "rule__QuizPartRef__Group__2__Impl"
    // InternalQuizDSL.g:1764:1: rule__QuizPartRef__Group__2__Impl : ( ( rule__QuizPartRef__PartRefAssignment_2 ) ) ;
    public final void rule__QuizPartRef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1768:1: ( ( ( rule__QuizPartRef__PartRefAssignment_2 ) ) )
            // InternalQuizDSL.g:1769:1: ( ( rule__QuizPartRef__PartRefAssignment_2 ) )
            {
            // InternalQuizDSL.g:1769:1: ( ( rule__QuizPartRef__PartRefAssignment_2 ) )
            // InternalQuizDSL.g:1770:2: ( rule__QuizPartRef__PartRefAssignment_2 )
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefAssignment_2()); 
            // InternalQuizDSL.g:1771:2: ( rule__QuizPartRef__PartRefAssignment_2 )
            // InternalQuizDSL.g:1771:3: rule__QuizPartRef__PartRefAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__PartRefAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartRefAccess().getPartRefAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__2__Impl"


    // $ANTLR start "rule__AnonymousQuizPart__Group__0"
    // InternalQuizDSL.g:1780:1: rule__AnonymousQuizPart__Group__0 : rule__AnonymousQuizPart__Group__0__Impl rule__AnonymousQuizPart__Group__1 ;
    public final void rule__AnonymousQuizPart__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1784:1: ( rule__AnonymousQuizPart__Group__0__Impl rule__AnonymousQuizPart__Group__1 )
            // InternalQuizDSL.g:1785:2: rule__AnonymousQuizPart__Group__0__Impl rule__AnonymousQuizPart__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__AnonymousQuizPart__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AnonymousQuizPart__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnonymousQuizPart__Group__0"


    // $ANTLR start "rule__AnonymousQuizPart__Group__0__Impl"
    // InternalQuizDSL.g:1792:1: rule__AnonymousQuizPart__Group__0__Impl : ( () ) ;
    public final void rule__AnonymousQuizPart__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1796:1: ( ( () ) )
            // InternalQuizDSL.g:1797:1: ( () )
            {
            // InternalQuizDSL.g:1797:1: ( () )
            // InternalQuizDSL.g:1798:2: ()
            {
             before(grammarAccess.getAnonymousQuizPartAccess().getQuizPartAction_0()); 
            // InternalQuizDSL.g:1799:2: ()
            // InternalQuizDSL.g:1799:3: 
            {
            }

             after(grammarAccess.getAnonymousQuizPartAccess().getQuizPartAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnonymousQuizPart__Group__0__Impl"


    // $ANTLR start "rule__AnonymousQuizPart__Group__1"
    // InternalQuizDSL.g:1807:1: rule__AnonymousQuizPart__Group__1 : rule__AnonymousQuizPart__Group__1__Impl ;
    public final void rule__AnonymousQuizPart__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1811:1: ( rule__AnonymousQuizPart__Group__1__Impl )
            // InternalQuizDSL.g:1812:2: rule__AnonymousQuizPart__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AnonymousQuizPart__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnonymousQuizPart__Group__1"


    // $ANTLR start "rule__AnonymousQuizPart__Group__1__Impl"
    // InternalQuizDSL.g:1818:1: rule__AnonymousQuizPart__Group__1__Impl : ( ( rule__AnonymousQuizPart__QuestionsAssignment_1 )* ) ;
    public final void rule__AnonymousQuizPart__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1822:1: ( ( ( rule__AnonymousQuizPart__QuestionsAssignment_1 )* ) )
            // InternalQuizDSL.g:1823:1: ( ( rule__AnonymousQuizPart__QuestionsAssignment_1 )* )
            {
            // InternalQuizDSL.g:1823:1: ( ( rule__AnonymousQuizPart__QuestionsAssignment_1 )* )
            // InternalQuizDSL.g:1824:2: ( rule__AnonymousQuizPart__QuestionsAssignment_1 )*
            {
             before(grammarAccess.getAnonymousQuizPartAccess().getQuestionsAssignment_1()); 
            // InternalQuizDSL.g:1825:2: ( rule__AnonymousQuizPart__QuestionsAssignment_1 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_ID||LA25_0==RULE_STRING||LA25_0==22||LA25_0==28) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalQuizDSL.g:1825:3: rule__AnonymousQuizPart__QuestionsAssignment_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__AnonymousQuizPart__QuestionsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getAnonymousQuizPartAccess().getQuestionsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnonymousQuizPart__Group__1__Impl"


    // $ANTLR start "rule__QARef__Group__0"
    // InternalQuizDSL.g:1834:1: rule__QARef__Group__0 : rule__QARef__Group__0__Impl rule__QARef__Group__1 ;
    public final void rule__QARef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1838:1: ( rule__QARef__Group__0__Impl rule__QARef__Group__1 )
            // InternalQuizDSL.g:1839:2: rule__QARef__Group__0__Impl rule__QARef__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__QARef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QARef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__0"


    // $ANTLR start "rule__QARef__Group__0__Impl"
    // InternalQuizDSL.g:1846:1: rule__QARef__Group__0__Impl : ( 'ref' ) ;
    public final void rule__QARef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1850:1: ( ( 'ref' ) )
            // InternalQuizDSL.g:1851:1: ( 'ref' )
            {
            // InternalQuizDSL.g:1851:1: ( 'ref' )
            // InternalQuizDSL.g:1852:2: 'ref'
            {
             before(grammarAccess.getQARefAccess().getRefKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getQARefAccess().getRefKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__0__Impl"


    // $ANTLR start "rule__QARef__Group__1"
    // InternalQuizDSL.g:1861:1: rule__QARef__Group__1 : rule__QARef__Group__1__Impl ;
    public final void rule__QARef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1865:1: ( rule__QARef__Group__1__Impl )
            // InternalQuizDSL.g:1866:2: rule__QARef__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QARef__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__1"


    // $ANTLR start "rule__QARef__Group__1__Impl"
    // InternalQuizDSL.g:1872:1: rule__QARef__Group__1__Impl : ( ( rule__QARef__QaRefAssignment_1 ) ) ;
    public final void rule__QARef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1876:1: ( ( ( rule__QARef__QaRefAssignment_1 ) ) )
            // InternalQuizDSL.g:1877:1: ( ( rule__QARef__QaRefAssignment_1 ) )
            {
            // InternalQuizDSL.g:1877:1: ( ( rule__QARef__QaRefAssignment_1 ) )
            // InternalQuizDSL.g:1878:2: ( rule__QARef__QaRefAssignment_1 )
            {
             before(grammarAccess.getQARefAccess().getQaRefAssignment_1()); 
            // InternalQuizDSL.g:1879:2: ( rule__QARef__QaRefAssignment_1 )
            // InternalQuizDSL.g:1879:3: rule__QARef__QaRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QARef__QaRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQARefAccess().getQaRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__1__Impl"


    // $ANTLR start "rule__QA__Group__0"
    // InternalQuizDSL.g:1888:1: rule__QA__Group__0 : rule__QA__Group__0__Impl rule__QA__Group__1 ;
    public final void rule__QA__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1892:1: ( rule__QA__Group__0__Impl rule__QA__Group__1 )
            // InternalQuizDSL.g:1893:2: rule__QA__Group__0__Impl rule__QA__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__QA__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QA__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__0"


    // $ANTLR start "rule__QA__Group__0__Impl"
    // InternalQuizDSL.g:1900:1: rule__QA__Group__0__Impl : ( ( rule__QA__NameAssignment_0 )? ) ;
    public final void rule__QA__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1904:1: ( ( ( rule__QA__NameAssignment_0 )? ) )
            // InternalQuizDSL.g:1905:1: ( ( rule__QA__NameAssignment_0 )? )
            {
            // InternalQuizDSL.g:1905:1: ( ( rule__QA__NameAssignment_0 )? )
            // InternalQuizDSL.g:1906:2: ( rule__QA__NameAssignment_0 )?
            {
             before(grammarAccess.getQAAccess().getNameAssignment_0()); 
            // InternalQuizDSL.g:1907:2: ( rule__QA__NameAssignment_0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_ID) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalQuizDSL.g:1907:3: rule__QA__NameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QA__NameAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQAAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__0__Impl"


    // $ANTLR start "rule__QA__Group__1"
    // InternalQuizDSL.g:1915:1: rule__QA__Group__1 : rule__QA__Group__1__Impl rule__QA__Group__2 ;
    public final void rule__QA__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1919:1: ( rule__QA__Group__1__Impl rule__QA__Group__2 )
            // InternalQuizDSL.g:1920:2: rule__QA__Group__1__Impl rule__QA__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__QA__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QA__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__1"


    // $ANTLR start "rule__QA__Group__1__Impl"
    // InternalQuizDSL.g:1927:1: rule__QA__Group__1__Impl : ( ( rule__QA__QAssignment_1 ) ) ;
    public final void rule__QA__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1931:1: ( ( ( rule__QA__QAssignment_1 ) ) )
            // InternalQuizDSL.g:1932:1: ( ( rule__QA__QAssignment_1 ) )
            {
            // InternalQuizDSL.g:1932:1: ( ( rule__QA__QAssignment_1 ) )
            // InternalQuizDSL.g:1933:2: ( rule__QA__QAssignment_1 )
            {
             before(grammarAccess.getQAAccess().getQAssignment_1()); 
            // InternalQuizDSL.g:1934:2: ( rule__QA__QAssignment_1 )
            // InternalQuizDSL.g:1934:3: rule__QA__QAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QA__QAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getQAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__1__Impl"


    // $ANTLR start "rule__QA__Group__2"
    // InternalQuizDSL.g:1942:1: rule__QA__Group__2 : rule__QA__Group__2__Impl ;
    public final void rule__QA__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1946:1: ( rule__QA__Group__2__Impl )
            // InternalQuizDSL.g:1947:2: rule__QA__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QA__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__2"


    // $ANTLR start "rule__QA__Group__2__Impl"
    // InternalQuizDSL.g:1953:1: rule__QA__Group__2__Impl : ( ( rule__QA__AAssignment_2 ) ) ;
    public final void rule__QA__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1957:1: ( ( ( rule__QA__AAssignment_2 ) ) )
            // InternalQuizDSL.g:1958:1: ( ( rule__QA__AAssignment_2 ) )
            {
            // InternalQuizDSL.g:1958:1: ( ( rule__QA__AAssignment_2 ) )
            // InternalQuizDSL.g:1959:2: ( rule__QA__AAssignment_2 )
            {
             before(grammarAccess.getQAAccess().getAAssignment_2()); 
            // InternalQuizDSL.g:1960:2: ( rule__QA__AAssignment_2 )
            // InternalQuizDSL.g:1960:3: rule__QA__AAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QA__AAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getAAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__2__Impl"


    // $ANTLR start "rule__StringAnswer__Group__0"
    // InternalQuizDSL.g:1969:1: rule__StringAnswer__Group__0 : rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1 ;
    public final void rule__StringAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1973:1: ( rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1 )
            // InternalQuizDSL.g:1974:2: rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__StringAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__0"


    // $ANTLR start "rule__StringAnswer__Group__0__Impl"
    // InternalQuizDSL.g:1981:1: rule__StringAnswer__Group__0__Impl : ( ( rule__StringAnswer__ValueAssignment_0 ) ) ;
    public final void rule__StringAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:1985:1: ( ( ( rule__StringAnswer__ValueAssignment_0 ) ) )
            // InternalQuizDSL.g:1986:1: ( ( rule__StringAnswer__ValueAssignment_0 ) )
            {
            // InternalQuizDSL.g:1986:1: ( ( rule__StringAnswer__ValueAssignment_0 ) )
            // InternalQuizDSL.g:1987:2: ( rule__StringAnswer__ValueAssignment_0 )
            {
             before(grammarAccess.getStringAnswerAccess().getValueAssignment_0()); 
            // InternalQuizDSL.g:1988:2: ( rule__StringAnswer__ValueAssignment_0 )
            // InternalQuizDSL.g:1988:3: rule__StringAnswer__ValueAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__ValueAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStringAnswerAccess().getValueAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__0__Impl"


    // $ANTLR start "rule__StringAnswer__Group__1"
    // InternalQuizDSL.g:1996:1: rule__StringAnswer__Group__1 : rule__StringAnswer__Group__1__Impl ;
    public final void rule__StringAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2000:1: ( rule__StringAnswer__Group__1__Impl )
            // InternalQuizDSL.g:2001:2: rule__StringAnswer__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__1"


    // $ANTLR start "rule__StringAnswer__Group__1__Impl"
    // InternalQuizDSL.g:2007:1: rule__StringAnswer__Group__1__Impl : ( ( rule__StringAnswer__IgnoreCaseAssignment_1 )? ) ;
    public final void rule__StringAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2011:1: ( ( ( rule__StringAnswer__IgnoreCaseAssignment_1 )? ) )
            // InternalQuizDSL.g:2012:1: ( ( rule__StringAnswer__IgnoreCaseAssignment_1 )? )
            {
            // InternalQuizDSL.g:2012:1: ( ( rule__StringAnswer__IgnoreCaseAssignment_1 )? )
            // InternalQuizDSL.g:2013:2: ( rule__StringAnswer__IgnoreCaseAssignment_1 )?
            {
             before(grammarAccess.getStringAnswerAccess().getIgnoreCaseAssignment_1()); 
            // InternalQuizDSL.g:2014:2: ( rule__StringAnswer__IgnoreCaseAssignment_1 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==32) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalQuizDSL.g:2014:3: rule__StringAnswer__IgnoreCaseAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__StringAnswer__IgnoreCaseAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStringAnswerAccess().getIgnoreCaseAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__1__Impl"


    // $ANTLR start "rule__RegexAnswer__Group__0"
    // InternalQuizDSL.g:2023:1: rule__RegexAnswer__Group__0 : rule__RegexAnswer__Group__0__Impl rule__RegexAnswer__Group__1 ;
    public final void rule__RegexAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2027:1: ( rule__RegexAnswer__Group__0__Impl rule__RegexAnswer__Group__1 )
            // InternalQuizDSL.g:2028:2: rule__RegexAnswer__Group__0__Impl rule__RegexAnswer__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__RegexAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RegexAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__0"


    // $ANTLR start "rule__RegexAnswer__Group__0__Impl"
    // InternalQuizDSL.g:2035:1: rule__RegexAnswer__Group__0__Impl : ( ( rule__RegexAnswer__RegexpAssignment_0 ) ) ;
    public final void rule__RegexAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2039:1: ( ( ( rule__RegexAnswer__RegexpAssignment_0 ) ) )
            // InternalQuizDSL.g:2040:1: ( ( rule__RegexAnswer__RegexpAssignment_0 ) )
            {
            // InternalQuizDSL.g:2040:1: ( ( rule__RegexAnswer__RegexpAssignment_0 ) )
            // InternalQuizDSL.g:2041:2: ( rule__RegexAnswer__RegexpAssignment_0 )
            {
             before(grammarAccess.getRegexAnswerAccess().getRegexpAssignment_0()); 
            // InternalQuizDSL.g:2042:2: ( rule__RegexAnswer__RegexpAssignment_0 )
            // InternalQuizDSL.g:2042:3: rule__RegexAnswer__RegexpAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__RegexAnswer__RegexpAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getRegexAnswerAccess().getRegexpAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__0__Impl"


    // $ANTLR start "rule__RegexAnswer__Group__1"
    // InternalQuizDSL.g:2050:1: rule__RegexAnswer__Group__1 : rule__RegexAnswer__Group__1__Impl rule__RegexAnswer__Group__2 ;
    public final void rule__RegexAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2054:1: ( rule__RegexAnswer__Group__1__Impl rule__RegexAnswer__Group__2 )
            // InternalQuizDSL.g:2055:2: rule__RegexAnswer__Group__1__Impl rule__RegexAnswer__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__RegexAnswer__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RegexAnswer__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__1"


    // $ANTLR start "rule__RegexAnswer__Group__1__Impl"
    // InternalQuizDSL.g:2062:1: rule__RegexAnswer__Group__1__Impl : ( ( rule__RegexAnswer__ValueAssignment_1 ) ) ;
    public final void rule__RegexAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2066:1: ( ( ( rule__RegexAnswer__ValueAssignment_1 ) ) )
            // InternalQuizDSL.g:2067:1: ( ( rule__RegexAnswer__ValueAssignment_1 ) )
            {
            // InternalQuizDSL.g:2067:1: ( ( rule__RegexAnswer__ValueAssignment_1 ) )
            // InternalQuizDSL.g:2068:2: ( rule__RegexAnswer__ValueAssignment_1 )
            {
             before(grammarAccess.getRegexAnswerAccess().getValueAssignment_1()); 
            // InternalQuizDSL.g:2069:2: ( rule__RegexAnswer__ValueAssignment_1 )
            // InternalQuizDSL.g:2069:3: rule__RegexAnswer__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__RegexAnswer__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRegexAnswerAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__1__Impl"


    // $ANTLR start "rule__RegexAnswer__Group__2"
    // InternalQuizDSL.g:2077:1: rule__RegexAnswer__Group__2 : rule__RegexAnswer__Group__2__Impl rule__RegexAnswer__Group__3 ;
    public final void rule__RegexAnswer__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2081:1: ( rule__RegexAnswer__Group__2__Impl rule__RegexAnswer__Group__3 )
            // InternalQuizDSL.g:2082:2: rule__RegexAnswer__Group__2__Impl rule__RegexAnswer__Group__3
            {
            pushFollow(FOLLOW_19);
            rule__RegexAnswer__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RegexAnswer__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__2"


    // $ANTLR start "rule__RegexAnswer__Group__2__Impl"
    // InternalQuizDSL.g:2089:1: rule__RegexAnswer__Group__2__Impl : ( '/' ) ;
    public final void rule__RegexAnswer__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2093:1: ( ( '/' ) )
            // InternalQuizDSL.g:2094:1: ( '/' )
            {
            // InternalQuizDSL.g:2094:1: ( '/' )
            // InternalQuizDSL.g:2095:2: '/'
            {
             before(grammarAccess.getRegexAnswerAccess().getSolidusKeyword_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getRegexAnswerAccess().getSolidusKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__2__Impl"


    // $ANTLR start "rule__RegexAnswer__Group__3"
    // InternalQuizDSL.g:2104:1: rule__RegexAnswer__Group__3 : rule__RegexAnswer__Group__3__Impl ;
    public final void rule__RegexAnswer__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2108:1: ( rule__RegexAnswer__Group__3__Impl )
            // InternalQuizDSL.g:2109:2: rule__RegexAnswer__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RegexAnswer__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__3"


    // $ANTLR start "rule__RegexAnswer__Group__3__Impl"
    // InternalQuizDSL.g:2115:1: rule__RegexAnswer__Group__3__Impl : ( ( rule__RegexAnswer__IgnoreCaseAssignment_3 )? ) ;
    public final void rule__RegexAnswer__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2119:1: ( ( ( rule__RegexAnswer__IgnoreCaseAssignment_3 )? ) )
            // InternalQuizDSL.g:2120:1: ( ( rule__RegexAnswer__IgnoreCaseAssignment_3 )? )
            {
            // InternalQuizDSL.g:2120:1: ( ( rule__RegexAnswer__IgnoreCaseAssignment_3 )? )
            // InternalQuizDSL.g:2121:2: ( rule__RegexAnswer__IgnoreCaseAssignment_3 )?
            {
             before(grammarAccess.getRegexAnswerAccess().getIgnoreCaseAssignment_3()); 
            // InternalQuizDSL.g:2122:2: ( rule__RegexAnswer__IgnoreCaseAssignment_3 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==32) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalQuizDSL.g:2122:3: rule__RegexAnswer__IgnoreCaseAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__RegexAnswer__IgnoreCaseAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRegexAnswerAccess().getIgnoreCaseAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__Group__3__Impl"


    // $ANTLR start "rule__NumberAnswer__Group__0"
    // InternalQuizDSL.g:2131:1: rule__NumberAnswer__Group__0 : rule__NumberAnswer__Group__0__Impl rule__NumberAnswer__Group__1 ;
    public final void rule__NumberAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2135:1: ( rule__NumberAnswer__Group__0__Impl rule__NumberAnswer__Group__1 )
            // InternalQuizDSL.g:2136:2: rule__NumberAnswer__Group__0__Impl rule__NumberAnswer__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__NumberAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NumberAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group__0"


    // $ANTLR start "rule__NumberAnswer__Group__0__Impl"
    // InternalQuizDSL.g:2143:1: rule__NumberAnswer__Group__0__Impl : ( ( rule__NumberAnswer__ValueAssignment_0 ) ) ;
    public final void rule__NumberAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2147:1: ( ( ( rule__NumberAnswer__ValueAssignment_0 ) ) )
            // InternalQuizDSL.g:2148:1: ( ( rule__NumberAnswer__ValueAssignment_0 ) )
            {
            // InternalQuizDSL.g:2148:1: ( ( rule__NumberAnswer__ValueAssignment_0 ) )
            // InternalQuizDSL.g:2149:2: ( rule__NumberAnswer__ValueAssignment_0 )
            {
             before(grammarAccess.getNumberAnswerAccess().getValueAssignment_0()); 
            // InternalQuizDSL.g:2150:2: ( rule__NumberAnswer__ValueAssignment_0 )
            // InternalQuizDSL.g:2150:3: rule__NumberAnswer__ValueAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__ValueAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getNumberAnswerAccess().getValueAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group__0__Impl"


    // $ANTLR start "rule__NumberAnswer__Group__1"
    // InternalQuizDSL.g:2158:1: rule__NumberAnswer__Group__1 : rule__NumberAnswer__Group__1__Impl ;
    public final void rule__NumberAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2162:1: ( rule__NumberAnswer__Group__1__Impl )
            // InternalQuizDSL.g:2163:2: rule__NumberAnswer__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group__1"


    // $ANTLR start "rule__NumberAnswer__Group__1__Impl"
    // InternalQuizDSL.g:2169:1: rule__NumberAnswer__Group__1__Impl : ( ( rule__NumberAnswer__Group_1__0 )? ) ;
    public final void rule__NumberAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2173:1: ( ( ( rule__NumberAnswer__Group_1__0 )? ) )
            // InternalQuizDSL.g:2174:1: ( ( rule__NumberAnswer__Group_1__0 )? )
            {
            // InternalQuizDSL.g:2174:1: ( ( rule__NumberAnswer__Group_1__0 )? )
            // InternalQuizDSL.g:2175:2: ( rule__NumberAnswer__Group_1__0 )?
            {
             before(grammarAccess.getNumberAnswerAccess().getGroup_1()); 
            // InternalQuizDSL.g:2176:2: ( rule__NumberAnswer__Group_1__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==23) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalQuizDSL.g:2176:3: rule__NumberAnswer__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NumberAnswer__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNumberAnswerAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group__1__Impl"


    // $ANTLR start "rule__NumberAnswer__Group_1__0"
    // InternalQuizDSL.g:2185:1: rule__NumberAnswer__Group_1__0 : rule__NumberAnswer__Group_1__0__Impl rule__NumberAnswer__Group_1__1 ;
    public final void rule__NumberAnswer__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2189:1: ( rule__NumberAnswer__Group_1__0__Impl rule__NumberAnswer__Group_1__1 )
            // InternalQuizDSL.g:2190:2: rule__NumberAnswer__Group_1__0__Impl rule__NumberAnswer__Group_1__1
            {
            pushFollow(FOLLOW_22);
            rule__NumberAnswer__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NumberAnswer__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group_1__0"


    // $ANTLR start "rule__NumberAnswer__Group_1__0__Impl"
    // InternalQuizDSL.g:2197:1: rule__NumberAnswer__Group_1__0__Impl : ( '+-' ) ;
    public final void rule__NumberAnswer__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2201:1: ( ( '+-' ) )
            // InternalQuizDSL.g:2202:1: ( '+-' )
            {
            // InternalQuizDSL.g:2202:1: ( '+-' )
            // InternalQuizDSL.g:2203:2: '+-'
            {
             before(grammarAccess.getNumberAnswerAccess().getPlusSignHyphenMinusKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getNumberAnswerAccess().getPlusSignHyphenMinusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group_1__0__Impl"


    // $ANTLR start "rule__NumberAnswer__Group_1__1"
    // InternalQuizDSL.g:2212:1: rule__NumberAnswer__Group_1__1 : rule__NumberAnswer__Group_1__1__Impl ;
    public final void rule__NumberAnswer__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2216:1: ( rule__NumberAnswer__Group_1__1__Impl )
            // InternalQuizDSL.g:2217:2: rule__NumberAnswer__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group_1__1"


    // $ANTLR start "rule__NumberAnswer__Group_1__1__Impl"
    // InternalQuizDSL.g:2223:1: rule__NumberAnswer__Group_1__1__Impl : ( ( rule__NumberAnswer__ErrorMarginAssignment_1_1 ) ) ;
    public final void rule__NumberAnswer__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2227:1: ( ( ( rule__NumberAnswer__ErrorMarginAssignment_1_1 ) ) )
            // InternalQuizDSL.g:2228:1: ( ( rule__NumberAnswer__ErrorMarginAssignment_1_1 ) )
            {
            // InternalQuizDSL.g:2228:1: ( ( rule__NumberAnswer__ErrorMarginAssignment_1_1 ) )
            // InternalQuizDSL.g:2229:2: ( rule__NumberAnswer__ErrorMarginAssignment_1_1 )
            {
             before(grammarAccess.getNumberAnswerAccess().getErrorMarginAssignment_1_1()); 
            // InternalQuizDSL.g:2230:2: ( rule__NumberAnswer__ErrorMarginAssignment_1_1 )
            // InternalQuizDSL.g:2230:3: rule__NumberAnswer__ErrorMarginAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__ErrorMarginAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getNumberAnswerAccess().getErrorMarginAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__Group_1__1__Impl"


    // $ANTLR start "rule__EDoubleObject__Group__0"
    // InternalQuizDSL.g:2239:1: rule__EDoubleObject__Group__0 : rule__EDoubleObject__Group__0__Impl rule__EDoubleObject__Group__1 ;
    public final void rule__EDoubleObject__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2243:1: ( rule__EDoubleObject__Group__0__Impl rule__EDoubleObject__Group__1 )
            // InternalQuizDSL.g:2244:2: rule__EDoubleObject__Group__0__Impl rule__EDoubleObject__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__EDoubleObject__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EDoubleObject__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group__0"


    // $ANTLR start "rule__EDoubleObject__Group__0__Impl"
    // InternalQuizDSL.g:2251:1: rule__EDoubleObject__Group__0__Impl : ( RULE_INT ) ;
    public final void rule__EDoubleObject__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2255:1: ( ( RULE_INT ) )
            // InternalQuizDSL.g:2256:1: ( RULE_INT )
            {
            // InternalQuizDSL.g:2256:1: ( RULE_INT )
            // InternalQuizDSL.g:2257:2: RULE_INT
            {
             before(grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group__0__Impl"


    // $ANTLR start "rule__EDoubleObject__Group__1"
    // InternalQuizDSL.g:2266:1: rule__EDoubleObject__Group__1 : rule__EDoubleObject__Group__1__Impl ;
    public final void rule__EDoubleObject__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2270:1: ( rule__EDoubleObject__Group__1__Impl )
            // InternalQuizDSL.g:2271:2: rule__EDoubleObject__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EDoubleObject__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group__1"


    // $ANTLR start "rule__EDoubleObject__Group__1__Impl"
    // InternalQuizDSL.g:2277:1: rule__EDoubleObject__Group__1__Impl : ( ( rule__EDoubleObject__Group_1__0 )? ) ;
    public final void rule__EDoubleObject__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2281:1: ( ( ( rule__EDoubleObject__Group_1__0 )? ) )
            // InternalQuizDSL.g:2282:1: ( ( rule__EDoubleObject__Group_1__0 )? )
            {
            // InternalQuizDSL.g:2282:1: ( ( rule__EDoubleObject__Group_1__0 )? )
            // InternalQuizDSL.g:2283:2: ( rule__EDoubleObject__Group_1__0 )?
            {
             before(grammarAccess.getEDoubleObjectAccess().getGroup_1()); 
            // InternalQuizDSL.g:2284:2: ( rule__EDoubleObject__Group_1__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==20) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalQuizDSL.g:2284:3: rule__EDoubleObject__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EDoubleObject__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEDoubleObjectAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group__1__Impl"


    // $ANTLR start "rule__EDoubleObject__Group_1__0"
    // InternalQuizDSL.g:2293:1: rule__EDoubleObject__Group_1__0 : rule__EDoubleObject__Group_1__0__Impl rule__EDoubleObject__Group_1__1 ;
    public final void rule__EDoubleObject__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2297:1: ( rule__EDoubleObject__Group_1__0__Impl rule__EDoubleObject__Group_1__1 )
            // InternalQuizDSL.g:2298:2: rule__EDoubleObject__Group_1__0__Impl rule__EDoubleObject__Group_1__1
            {
            pushFollow(FOLLOW_22);
            rule__EDoubleObject__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EDoubleObject__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group_1__0"


    // $ANTLR start "rule__EDoubleObject__Group_1__0__Impl"
    // InternalQuizDSL.g:2305:1: rule__EDoubleObject__Group_1__0__Impl : ( '.' ) ;
    public final void rule__EDoubleObject__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2309:1: ( ( '.' ) )
            // InternalQuizDSL.g:2310:1: ( '.' )
            {
            // InternalQuizDSL.g:2310:1: ( '.' )
            // InternalQuizDSL.g:2311:2: '.'
            {
             before(grammarAccess.getEDoubleObjectAccess().getFullStopKeyword_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getEDoubleObjectAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group_1__0__Impl"


    // $ANTLR start "rule__EDoubleObject__Group_1__1"
    // InternalQuizDSL.g:2320:1: rule__EDoubleObject__Group_1__1 : rule__EDoubleObject__Group_1__1__Impl ;
    public final void rule__EDoubleObject__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2324:1: ( rule__EDoubleObject__Group_1__1__Impl )
            // InternalQuizDSL.g:2325:2: rule__EDoubleObject__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EDoubleObject__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group_1__1"


    // $ANTLR start "rule__EDoubleObject__Group_1__1__Impl"
    // InternalQuizDSL.g:2331:1: rule__EDoubleObject__Group_1__1__Impl : ( RULE_INT ) ;
    public final void rule__EDoubleObject__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2335:1: ( ( RULE_INT ) )
            // InternalQuizDSL.g:2336:1: ( RULE_INT )
            {
            // InternalQuizDSL.g:2336:1: ( RULE_INT )
            // InternalQuizDSL.g:2337:2: RULE_INT
            {
             before(grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_1_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEDoubleObjectAccess().getINTTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDoubleObject__Group_1__1__Impl"


    // $ANTLR start "rule__BooleanAnswer__Group__0"
    // InternalQuizDSL.g:2347:1: rule__BooleanAnswer__Group__0 : rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1 ;
    public final void rule__BooleanAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2351:1: ( rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1 )
            // InternalQuizDSL.g:2352:2: rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__BooleanAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__0"


    // $ANTLR start "rule__BooleanAnswer__Group__0__Impl"
    // InternalQuizDSL.g:2359:1: rule__BooleanAnswer__Group__0__Impl : ( () ) ;
    public final void rule__BooleanAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2363:1: ( ( () ) )
            // InternalQuizDSL.g:2364:1: ( () )
            {
            // InternalQuizDSL.g:2364:1: ( () )
            // InternalQuizDSL.g:2365:2: ()
            {
             before(grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0()); 
            // InternalQuizDSL.g:2366:2: ()
            // InternalQuizDSL.g:2366:3: 
            {
            }

             after(grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__0__Impl"


    // $ANTLR start "rule__BooleanAnswer__Group__1"
    // InternalQuizDSL.g:2374:1: rule__BooleanAnswer__Group__1 : rule__BooleanAnswer__Group__1__Impl ;
    public final void rule__BooleanAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2378:1: ( rule__BooleanAnswer__Group__1__Impl )
            // InternalQuizDSL.g:2379:2: rule__BooleanAnswer__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__1"


    // $ANTLR start "rule__BooleanAnswer__Group__1__Impl"
    // InternalQuizDSL.g:2385:1: rule__BooleanAnswer__Group__1__Impl : ( ( rule__BooleanAnswer__Alternatives_1 ) ) ;
    public final void rule__BooleanAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2389:1: ( ( ( rule__BooleanAnswer__Alternatives_1 ) ) )
            // InternalQuizDSL.g:2390:1: ( ( rule__BooleanAnswer__Alternatives_1 ) )
            {
            // InternalQuizDSL.g:2390:1: ( ( rule__BooleanAnswer__Alternatives_1 ) )
            // InternalQuizDSL.g:2391:2: ( rule__BooleanAnswer__Alternatives_1 )
            {
             before(grammarAccess.getBooleanAnswerAccess().getAlternatives_1()); 
            // InternalQuizDSL.g:2392:2: ( rule__BooleanAnswer__Alternatives_1 )
            // InternalQuizDSL.g:2392:3: rule__BooleanAnswer__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAnswerAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__1__Impl"


    // $ANTLR start "rule__SingleBoxOption__Group__0"
    // InternalQuizDSL.g:2401:1: rule__SingleBoxOption__Group__0 : rule__SingleBoxOption__Group__0__Impl rule__SingleBoxOption__Group__1 ;
    public final void rule__SingleBoxOption__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2405:1: ( rule__SingleBoxOption__Group__0__Impl rule__SingleBoxOption__Group__1 )
            // InternalQuizDSL.g:2406:2: rule__SingleBoxOption__Group__0__Impl rule__SingleBoxOption__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__SingleBoxOption__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__0"


    // $ANTLR start "rule__SingleBoxOption__Group__0__Impl"
    // InternalQuizDSL.g:2413:1: rule__SingleBoxOption__Group__0__Impl : ( '(' ) ;
    public final void rule__SingleBoxOption__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2417:1: ( ( '(' ) )
            // InternalQuizDSL.g:2418:1: ( '(' )
            {
            // InternalQuizDSL.g:2418:1: ( '(' )
            // InternalQuizDSL.g:2419:2: '('
            {
             before(grammarAccess.getSingleBoxOptionAccess().getLeftParenthesisKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSingleBoxOptionAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__0__Impl"


    // $ANTLR start "rule__SingleBoxOption__Group__1"
    // InternalQuizDSL.g:2428:1: rule__SingleBoxOption__Group__1 : rule__SingleBoxOption__Group__1__Impl rule__SingleBoxOption__Group__2 ;
    public final void rule__SingleBoxOption__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2432:1: ( rule__SingleBoxOption__Group__1__Impl rule__SingleBoxOption__Group__2 )
            // InternalQuizDSL.g:2433:2: rule__SingleBoxOption__Group__1__Impl rule__SingleBoxOption__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__SingleBoxOption__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__1"


    // $ANTLR start "rule__SingleBoxOption__Group__1__Impl"
    // InternalQuizDSL.g:2440:1: rule__SingleBoxOption__Group__1__Impl : ( ( rule__SingleBoxOption__CorrectAssignment_1 )? ) ;
    public final void rule__SingleBoxOption__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2444:1: ( ( ( rule__SingleBoxOption__CorrectAssignment_1 )? ) )
            // InternalQuizDSL.g:2445:1: ( ( rule__SingleBoxOption__CorrectAssignment_1 )? )
            {
            // InternalQuizDSL.g:2445:1: ( ( rule__SingleBoxOption__CorrectAssignment_1 )? )
            // InternalQuizDSL.g:2446:2: ( rule__SingleBoxOption__CorrectAssignment_1 )?
            {
             before(grammarAccess.getSingleBoxOptionAccess().getCorrectAssignment_1()); 
            // InternalQuizDSL.g:2447:2: ( rule__SingleBoxOption__CorrectAssignment_1 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==17) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalQuizDSL.g:2447:3: rule__SingleBoxOption__CorrectAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SingleBoxOption__CorrectAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSingleBoxOptionAccess().getCorrectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__1__Impl"


    // $ANTLR start "rule__SingleBoxOption__Group__2"
    // InternalQuizDSL.g:2455:1: rule__SingleBoxOption__Group__2 : rule__SingleBoxOption__Group__2__Impl rule__SingleBoxOption__Group__3 ;
    public final void rule__SingleBoxOption__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2459:1: ( rule__SingleBoxOption__Group__2__Impl rule__SingleBoxOption__Group__3 )
            // InternalQuizDSL.g:2460:2: rule__SingleBoxOption__Group__2__Impl rule__SingleBoxOption__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__SingleBoxOption__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__2"


    // $ANTLR start "rule__SingleBoxOption__Group__2__Impl"
    // InternalQuizDSL.g:2467:1: rule__SingleBoxOption__Group__2__Impl : ( ')' ) ;
    public final void rule__SingleBoxOption__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2471:1: ( ( ')' ) )
            // InternalQuizDSL.g:2472:1: ( ')' )
            {
            // InternalQuizDSL.g:2472:1: ( ')' )
            // InternalQuizDSL.g:2473:2: ')'
            {
             before(grammarAccess.getSingleBoxOptionAccess().getRightParenthesisKeyword_2()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getSingleBoxOptionAccess().getRightParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__2__Impl"


    // $ANTLR start "rule__SingleBoxOption__Group__3"
    // InternalQuizDSL.g:2482:1: rule__SingleBoxOption__Group__3 : rule__SingleBoxOption__Group__3__Impl ;
    public final void rule__SingleBoxOption__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2486:1: ( rule__SingleBoxOption__Group__3__Impl )
            // InternalQuizDSL.g:2487:2: rule__SingleBoxOption__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__3"


    // $ANTLR start "rule__SingleBoxOption__Group__3__Impl"
    // InternalQuizDSL.g:2493:1: rule__SingleBoxOption__Group__3__Impl : ( ( rule__SingleBoxOption__OptionAssignment_3 ) ) ;
    public final void rule__SingleBoxOption__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2497:1: ( ( ( rule__SingleBoxOption__OptionAssignment_3 ) ) )
            // InternalQuizDSL.g:2498:1: ( ( rule__SingleBoxOption__OptionAssignment_3 ) )
            {
            // InternalQuizDSL.g:2498:1: ( ( rule__SingleBoxOption__OptionAssignment_3 ) )
            // InternalQuizDSL.g:2499:2: ( rule__SingleBoxOption__OptionAssignment_3 )
            {
             before(grammarAccess.getSingleBoxOptionAccess().getOptionAssignment_3()); 
            // InternalQuizDSL.g:2500:2: ( rule__SingleBoxOption__OptionAssignment_3 )
            // InternalQuizDSL.g:2500:3: rule__SingleBoxOption__OptionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__SingleBoxOption__OptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSingleBoxOptionAccess().getOptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__Group__3__Impl"


    // $ANTLR start "rule__SingleListOption__Group__0"
    // InternalQuizDSL.g:2509:1: rule__SingleListOption__Group__0 : rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1 ;
    public final void rule__SingleListOption__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2513:1: ( rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1 )
            // InternalQuizDSL.g:2514:2: rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__SingleListOption__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__0"


    // $ANTLR start "rule__SingleListOption__Group__0__Impl"
    // InternalQuizDSL.g:2521:1: rule__SingleListOption__Group__0__Impl : ( ( rule__SingleListOption__Alternatives_0 ) ) ;
    public final void rule__SingleListOption__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2525:1: ( ( ( rule__SingleListOption__Alternatives_0 ) ) )
            // InternalQuizDSL.g:2526:1: ( ( rule__SingleListOption__Alternatives_0 ) )
            {
            // InternalQuizDSL.g:2526:1: ( ( rule__SingleListOption__Alternatives_0 ) )
            // InternalQuizDSL.g:2527:2: ( rule__SingleListOption__Alternatives_0 )
            {
             before(grammarAccess.getSingleListOptionAccess().getAlternatives_0()); 
            // InternalQuizDSL.g:2528:2: ( rule__SingleListOption__Alternatives_0 )
            // InternalQuizDSL.g:2528:3: rule__SingleListOption__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__0__Impl"


    // $ANTLR start "rule__SingleListOption__Group__1"
    // InternalQuizDSL.g:2536:1: rule__SingleListOption__Group__1 : rule__SingleListOption__Group__1__Impl ;
    public final void rule__SingleListOption__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2540:1: ( rule__SingleListOption__Group__1__Impl )
            // InternalQuizDSL.g:2541:2: rule__SingleListOption__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__1"


    // $ANTLR start "rule__SingleListOption__Group__1__Impl"
    // InternalQuizDSL.g:2547:1: rule__SingleListOption__Group__1__Impl : ( ( rule__SingleListOption__OptionAssignment_1 ) ) ;
    public final void rule__SingleListOption__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2551:1: ( ( ( rule__SingleListOption__OptionAssignment_1 ) ) )
            // InternalQuizDSL.g:2552:1: ( ( rule__SingleListOption__OptionAssignment_1 ) )
            {
            // InternalQuizDSL.g:2552:1: ( ( rule__SingleListOption__OptionAssignment_1 ) )
            // InternalQuizDSL.g:2553:2: ( rule__SingleListOption__OptionAssignment_1 )
            {
             before(grammarAccess.getSingleListOptionAccess().getOptionAssignment_1()); 
            // InternalQuizDSL.g:2554:2: ( rule__SingleListOption__OptionAssignment_1 )
            // InternalQuizDSL.g:2554:3: rule__SingleListOption__OptionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__OptionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionAccess().getOptionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__1__Impl"


    // $ANTLR start "rule__ManyOption__Group__0"
    // InternalQuizDSL.g:2563:1: rule__ManyOption__Group__0 : rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1 ;
    public final void rule__ManyOption__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2567:1: ( rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1 )
            // InternalQuizDSL.g:2568:2: rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__ManyOption__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__0"


    // $ANTLR start "rule__ManyOption__Group__0__Impl"
    // InternalQuizDSL.g:2575:1: rule__ManyOption__Group__0__Impl : ( '[' ) ;
    public final void rule__ManyOption__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2579:1: ( ( '[' ) )
            // InternalQuizDSL.g:2580:1: ( '[' )
            {
            // InternalQuizDSL.g:2580:1: ( '[' )
            // InternalQuizDSL.g:2581:2: '['
            {
             before(grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__0__Impl"


    // $ANTLR start "rule__ManyOption__Group__1"
    // InternalQuizDSL.g:2590:1: rule__ManyOption__Group__1 : rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2 ;
    public final void rule__ManyOption__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2594:1: ( rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2 )
            // InternalQuizDSL.g:2595:2: rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__ManyOption__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__1"


    // $ANTLR start "rule__ManyOption__Group__1__Impl"
    // InternalQuizDSL.g:2602:1: rule__ManyOption__Group__1__Impl : ( ( rule__ManyOption__CorrectAssignment_1 )? ) ;
    public final void rule__ManyOption__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2606:1: ( ( ( rule__ManyOption__CorrectAssignment_1 )? ) )
            // InternalQuizDSL.g:2607:1: ( ( rule__ManyOption__CorrectAssignment_1 )? )
            {
            // InternalQuizDSL.g:2607:1: ( ( rule__ManyOption__CorrectAssignment_1 )? )
            // InternalQuizDSL.g:2608:2: ( rule__ManyOption__CorrectAssignment_1 )?
            {
             before(grammarAccess.getManyOptionAccess().getCorrectAssignment_1()); 
            // InternalQuizDSL.g:2609:2: ( rule__ManyOption__CorrectAssignment_1 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==17) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalQuizDSL.g:2609:3: rule__ManyOption__CorrectAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ManyOption__CorrectAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getManyOptionAccess().getCorrectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__1__Impl"


    // $ANTLR start "rule__ManyOption__Group__2"
    // InternalQuizDSL.g:2617:1: rule__ManyOption__Group__2 : rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3 ;
    public final void rule__ManyOption__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2621:1: ( rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3 )
            // InternalQuizDSL.g:2622:2: rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__ManyOption__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__2"


    // $ANTLR start "rule__ManyOption__Group__2__Impl"
    // InternalQuizDSL.g:2629:1: rule__ManyOption__Group__2__Impl : ( ']' ) ;
    public final void rule__ManyOption__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2633:1: ( ( ']' ) )
            // InternalQuizDSL.g:2634:1: ( ']' )
            {
            // InternalQuizDSL.g:2634:1: ( ']' )
            // InternalQuizDSL.g:2635:2: ']'
            {
             before(grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__2__Impl"


    // $ANTLR start "rule__ManyOption__Group__3"
    // InternalQuizDSL.g:2644:1: rule__ManyOption__Group__3 : rule__ManyOption__Group__3__Impl ;
    public final void rule__ManyOption__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2648:1: ( rule__ManyOption__Group__3__Impl )
            // InternalQuizDSL.g:2649:2: rule__ManyOption__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__3"


    // $ANTLR start "rule__ManyOption__Group__3__Impl"
    // InternalQuizDSL.g:2655:1: rule__ManyOption__Group__3__Impl : ( ( rule__ManyOption__OptionAssignment_3 ) ) ;
    public final void rule__ManyOption__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2659:1: ( ( ( rule__ManyOption__OptionAssignment_3 ) ) )
            // InternalQuizDSL.g:2660:1: ( ( rule__ManyOption__OptionAssignment_3 ) )
            {
            // InternalQuizDSL.g:2660:1: ( ( rule__ManyOption__OptionAssignment_3 ) )
            // InternalQuizDSL.g:2661:2: ( rule__ManyOption__OptionAssignment_3 )
            {
             before(grammarAccess.getManyOptionAccess().getOptionAssignment_3()); 
            // InternalQuizDSL.g:2662:2: ( rule__ManyOption__OptionAssignment_3 )
            // InternalQuizDSL.g:2662:3: rule__ManyOption__OptionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__OptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionAccess().getOptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__3__Impl"


    // $ANTLR start "rule__Xml__Group__0"
    // InternalQuizDSL.g:2671:1: rule__Xml__Group__0 : rule__Xml__Group__0__Impl rule__Xml__Group__1 ;
    public final void rule__Xml__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2675:1: ( rule__Xml__Group__0__Impl rule__Xml__Group__1 )
            // InternalQuizDSL.g:2676:2: rule__Xml__Group__0__Impl rule__Xml__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Xml__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Xml__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__0"


    // $ANTLR start "rule__Xml__Group__0__Impl"
    // InternalQuizDSL.g:2683:1: rule__Xml__Group__0__Impl : ( '<<' ) ;
    public final void rule__Xml__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2687:1: ( ( '<<' ) )
            // InternalQuizDSL.g:2688:1: ( '<<' )
            {
            // InternalQuizDSL.g:2688:1: ( '<<' )
            // InternalQuizDSL.g:2689:2: '<<'
            {
             before(grammarAccess.getXmlAccess().getLessThanSignLessThanSignKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getXmlAccess().getLessThanSignLessThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__0__Impl"


    // $ANTLR start "rule__Xml__Group__1"
    // InternalQuizDSL.g:2698:1: rule__Xml__Group__1 : rule__Xml__Group__1__Impl rule__Xml__Group__2 ;
    public final void rule__Xml__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2702:1: ( rule__Xml__Group__1__Impl rule__Xml__Group__2 )
            // InternalQuizDSL.g:2703:2: rule__Xml__Group__1__Impl rule__Xml__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__Xml__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Xml__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__1"


    // $ANTLR start "rule__Xml__Group__1__Impl"
    // InternalQuizDSL.g:2710:1: rule__Xml__Group__1__Impl : ( ( rule__Xml__ElementAssignment_1 ) ) ;
    public final void rule__Xml__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2714:1: ( ( ( rule__Xml__ElementAssignment_1 ) ) )
            // InternalQuizDSL.g:2715:1: ( ( rule__Xml__ElementAssignment_1 ) )
            {
            // InternalQuizDSL.g:2715:1: ( ( rule__Xml__ElementAssignment_1 ) )
            // InternalQuizDSL.g:2716:2: ( rule__Xml__ElementAssignment_1 )
            {
             before(grammarAccess.getXmlAccess().getElementAssignment_1()); 
            // InternalQuizDSL.g:2717:2: ( rule__Xml__ElementAssignment_1 )
            // InternalQuizDSL.g:2717:3: rule__Xml__ElementAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Xml__ElementAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlAccess().getElementAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__1__Impl"


    // $ANTLR start "rule__Xml__Group__2"
    // InternalQuizDSL.g:2725:1: rule__Xml__Group__2 : rule__Xml__Group__2__Impl ;
    public final void rule__Xml__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2729:1: ( rule__Xml__Group__2__Impl )
            // InternalQuizDSL.g:2730:2: rule__Xml__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Xml__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__2"


    // $ANTLR start "rule__Xml__Group__2__Impl"
    // InternalQuizDSL.g:2736:1: rule__Xml__Group__2__Impl : ( '>>' ) ;
    public final void rule__Xml__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2740:1: ( ( '>>' ) )
            // InternalQuizDSL.g:2741:1: ( '>>' )
            {
            // InternalQuizDSL.g:2741:1: ( '>>' )
            // InternalQuizDSL.g:2742:2: '>>'
            {
             before(grammarAccess.getXmlAccess().getGreaterThanSignGreaterThanSignKeyword_2()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getXmlAccess().getGreaterThanSignGreaterThanSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__Group__2__Impl"


    // $ANTLR start "rule__XmlContents__Group__0"
    // InternalQuizDSL.g:2752:1: rule__XmlContents__Group__0 : rule__XmlContents__Group__0__Impl rule__XmlContents__Group__1 ;
    public final void rule__XmlContents__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2756:1: ( rule__XmlContents__Group__0__Impl rule__XmlContents__Group__1 )
            // InternalQuizDSL.g:2757:2: rule__XmlContents__Group__0__Impl rule__XmlContents__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__XmlContents__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlContents__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__Group__0"


    // $ANTLR start "rule__XmlContents__Group__0__Impl"
    // InternalQuizDSL.g:2764:1: rule__XmlContents__Group__0__Impl : ( ( rule__XmlContents__ElementAssignment_0 ) ) ;
    public final void rule__XmlContents__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2768:1: ( ( ( rule__XmlContents__ElementAssignment_0 ) ) )
            // InternalQuizDSL.g:2769:1: ( ( rule__XmlContents__ElementAssignment_0 ) )
            {
            // InternalQuizDSL.g:2769:1: ( ( rule__XmlContents__ElementAssignment_0 ) )
            // InternalQuizDSL.g:2770:2: ( rule__XmlContents__ElementAssignment_0 )
            {
             before(grammarAccess.getXmlContentsAccess().getElementAssignment_0()); 
            // InternalQuizDSL.g:2771:2: ( rule__XmlContents__ElementAssignment_0 )
            // InternalQuizDSL.g:2771:3: rule__XmlContents__ElementAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlContents__ElementAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlContentsAccess().getElementAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__Group__0__Impl"


    // $ANTLR start "rule__XmlContents__Group__1"
    // InternalQuizDSL.g:2779:1: rule__XmlContents__Group__1 : rule__XmlContents__Group__1__Impl ;
    public final void rule__XmlContents__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2783:1: ( rule__XmlContents__Group__1__Impl )
            // InternalQuizDSL.g:2784:2: rule__XmlContents__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlContents__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__Group__1"


    // $ANTLR start "rule__XmlContents__Group__1__Impl"
    // InternalQuizDSL.g:2790:1: rule__XmlContents__Group__1__Impl : ( ( rule__XmlContents__PostAssignment_1 ) ) ;
    public final void rule__XmlContents__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2794:1: ( ( ( rule__XmlContents__PostAssignment_1 ) ) )
            // InternalQuizDSL.g:2795:1: ( ( rule__XmlContents__PostAssignment_1 ) )
            {
            // InternalQuizDSL.g:2795:1: ( ( rule__XmlContents__PostAssignment_1 ) )
            // InternalQuizDSL.g:2796:2: ( rule__XmlContents__PostAssignment_1 )
            {
             before(grammarAccess.getXmlContentsAccess().getPostAssignment_1()); 
            // InternalQuizDSL.g:2797:2: ( rule__XmlContents__PostAssignment_1 )
            // InternalQuizDSL.g:2797:3: rule__XmlContents__PostAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlContents__PostAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlContentsAccess().getPostAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__Group__1__Impl"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__0"
    // InternalQuizDSL.g:2806:1: rule__XmlPIAnswerElement__Group__0 : rule__XmlPIAnswerElement__Group__0__Impl rule__XmlPIAnswerElement__Group__1 ;
    public final void rule__XmlPIAnswerElement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2810:1: ( rule__XmlPIAnswerElement__Group__0__Impl rule__XmlPIAnswerElement__Group__1 )
            // InternalQuizDSL.g:2811:2: rule__XmlPIAnswerElement__Group__0__Impl rule__XmlPIAnswerElement__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__XmlPIAnswerElement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlPIAnswerElement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__0"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__0__Impl"
    // InternalQuizDSL.g:2818:1: rule__XmlPIAnswerElement__Group__0__Impl : ( '?' ) ;
    public final void rule__XmlPIAnswerElement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2822:1: ( ( '?' ) )
            // InternalQuizDSL.g:2823:1: ( '?' )
            {
            // InternalQuizDSL.g:2823:1: ( '?' )
            // InternalQuizDSL.g:2824:2: '?'
            {
             before(grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__0__Impl"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__1"
    // InternalQuizDSL.g:2833:1: rule__XmlPIAnswerElement__Group__1 : rule__XmlPIAnswerElement__Group__1__Impl rule__XmlPIAnswerElement__Group__2 ;
    public final void rule__XmlPIAnswerElement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2837:1: ( rule__XmlPIAnswerElement__Group__1__Impl rule__XmlPIAnswerElement__Group__2 )
            // InternalQuizDSL.g:2838:2: rule__XmlPIAnswerElement__Group__1__Impl rule__XmlPIAnswerElement__Group__2
            {
            pushFollow(FOLLOW_30);
            rule__XmlPIAnswerElement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlPIAnswerElement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__1"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__1__Impl"
    // InternalQuizDSL.g:2845:1: rule__XmlPIAnswerElement__Group__1__Impl : ( ( rule__XmlPIAnswerElement__AnswerAssignment_1 ) ) ;
    public final void rule__XmlPIAnswerElement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2849:1: ( ( ( rule__XmlPIAnswerElement__AnswerAssignment_1 ) ) )
            // InternalQuizDSL.g:2850:1: ( ( rule__XmlPIAnswerElement__AnswerAssignment_1 ) )
            {
            // InternalQuizDSL.g:2850:1: ( ( rule__XmlPIAnswerElement__AnswerAssignment_1 ) )
            // InternalQuizDSL.g:2851:2: ( rule__XmlPIAnswerElement__AnswerAssignment_1 )
            {
             before(grammarAccess.getXmlPIAnswerElementAccess().getAnswerAssignment_1()); 
            // InternalQuizDSL.g:2852:2: ( rule__XmlPIAnswerElement__AnswerAssignment_1 )
            // InternalQuizDSL.g:2852:3: rule__XmlPIAnswerElement__AnswerAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlPIAnswerElement__AnswerAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlPIAnswerElementAccess().getAnswerAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__1__Impl"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__2"
    // InternalQuizDSL.g:2860:1: rule__XmlPIAnswerElement__Group__2 : rule__XmlPIAnswerElement__Group__2__Impl ;
    public final void rule__XmlPIAnswerElement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2864:1: ( rule__XmlPIAnswerElement__Group__2__Impl )
            // InternalQuizDSL.g:2865:2: rule__XmlPIAnswerElement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlPIAnswerElement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__2"


    // $ANTLR start "rule__XmlPIAnswerElement__Group__2__Impl"
    // InternalQuizDSL.g:2871:1: rule__XmlPIAnswerElement__Group__2__Impl : ( '?' ) ;
    public final void rule__XmlPIAnswerElement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2875:1: ( ( '?' ) )
            // InternalQuizDSL.g:2876:1: ( '?' )
            {
            // InternalQuizDSL.g:2876:1: ( '?' )
            // InternalQuizDSL.g:2877:2: '?'
            {
             before(grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_2()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getXmlPIAnswerElementAccess().getQuestionMarkKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__Group__2__Impl"


    // $ANTLR start "rule__XmlTagElement__Group__0"
    // InternalQuizDSL.g:2887:1: rule__XmlTagElement__Group__0 : rule__XmlTagElement__Group__0__Impl rule__XmlTagElement__Group__1 ;
    public final void rule__XmlTagElement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2891:1: ( rule__XmlTagElement__Group__0__Impl rule__XmlTagElement__Group__1 )
            // InternalQuizDSL.g:2892:2: rule__XmlTagElement__Group__0__Impl rule__XmlTagElement__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__XmlTagElement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group__0"


    // $ANTLR start "rule__XmlTagElement__Group__0__Impl"
    // InternalQuizDSL.g:2899:1: rule__XmlTagElement__Group__0__Impl : ( ( rule__XmlTagElement__StartTagAssignment_0 ) ) ;
    public final void rule__XmlTagElement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2903:1: ( ( ( rule__XmlTagElement__StartTagAssignment_0 ) ) )
            // InternalQuizDSL.g:2904:1: ( ( rule__XmlTagElement__StartTagAssignment_0 ) )
            {
            // InternalQuizDSL.g:2904:1: ( ( rule__XmlTagElement__StartTagAssignment_0 ) )
            // InternalQuizDSL.g:2905:2: ( rule__XmlTagElement__StartTagAssignment_0 )
            {
             before(grammarAccess.getXmlTagElementAccess().getStartTagAssignment_0()); 
            // InternalQuizDSL.g:2906:2: ( rule__XmlTagElement__StartTagAssignment_0 )
            // InternalQuizDSL.g:2906:3: rule__XmlTagElement__StartTagAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__StartTagAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagElementAccess().getStartTagAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group__0__Impl"


    // $ANTLR start "rule__XmlTagElement__Group__1"
    // InternalQuizDSL.g:2914:1: rule__XmlTagElement__Group__1 : rule__XmlTagElement__Group__1__Impl ;
    public final void rule__XmlTagElement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2918:1: ( rule__XmlTagElement__Group__1__Impl )
            // InternalQuizDSL.g:2919:2: rule__XmlTagElement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group__1"


    // $ANTLR start "rule__XmlTagElement__Group__1__Impl"
    // InternalQuizDSL.g:2925:1: rule__XmlTagElement__Group__1__Impl : ( ( rule__XmlTagElement__Alternatives_1 ) ) ;
    public final void rule__XmlTagElement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2929:1: ( ( ( rule__XmlTagElement__Alternatives_1 ) ) )
            // InternalQuizDSL.g:2930:1: ( ( rule__XmlTagElement__Alternatives_1 ) )
            {
            // InternalQuizDSL.g:2930:1: ( ( rule__XmlTagElement__Alternatives_1 ) )
            // InternalQuizDSL.g:2931:2: ( rule__XmlTagElement__Alternatives_1 )
            {
             before(grammarAccess.getXmlTagElementAccess().getAlternatives_1()); 
            // InternalQuizDSL.g:2932:2: ( rule__XmlTagElement__Alternatives_1 )
            // InternalQuizDSL.g:2932:3: rule__XmlTagElement__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagElementAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group__1__Impl"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__0"
    // InternalQuizDSL.g:2941:1: rule__XmlTagElement__Group_1_1__0 : rule__XmlTagElement__Group_1_1__0__Impl rule__XmlTagElement__Group_1_1__1 ;
    public final void rule__XmlTagElement__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2945:1: ( rule__XmlTagElement__Group_1_1__0__Impl rule__XmlTagElement__Group_1_1__1 )
            // InternalQuizDSL.g:2946:2: rule__XmlTagElement__Group_1_1__0__Impl rule__XmlTagElement__Group_1_1__1
            {
            pushFollow(FOLLOW_32);
            rule__XmlTagElement__Group_1_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__0"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__0__Impl"
    // InternalQuizDSL.g:2953:1: rule__XmlTagElement__Group_1_1__0__Impl : ( ( rule__XmlTagElement__PreAssignment_1_1_0 ) ) ;
    public final void rule__XmlTagElement__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2957:1: ( ( ( rule__XmlTagElement__PreAssignment_1_1_0 ) ) )
            // InternalQuizDSL.g:2958:1: ( ( rule__XmlTagElement__PreAssignment_1_1_0 ) )
            {
            // InternalQuizDSL.g:2958:1: ( ( rule__XmlTagElement__PreAssignment_1_1_0 ) )
            // InternalQuizDSL.g:2959:2: ( rule__XmlTagElement__PreAssignment_1_1_0 )
            {
             before(grammarAccess.getXmlTagElementAccess().getPreAssignment_1_1_0()); 
            // InternalQuizDSL.g:2960:2: ( rule__XmlTagElement__PreAssignment_1_1_0 )
            // InternalQuizDSL.g:2960:3: rule__XmlTagElement__PreAssignment_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__PreAssignment_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagElementAccess().getPreAssignment_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__0__Impl"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__1"
    // InternalQuizDSL.g:2968:1: rule__XmlTagElement__Group_1_1__1 : rule__XmlTagElement__Group_1_1__1__Impl rule__XmlTagElement__Group_1_1__2 ;
    public final void rule__XmlTagElement__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2972:1: ( rule__XmlTagElement__Group_1_1__1__Impl rule__XmlTagElement__Group_1_1__2 )
            // InternalQuizDSL.g:2973:2: rule__XmlTagElement__Group_1_1__1__Impl rule__XmlTagElement__Group_1_1__2
            {
            pushFollow(FOLLOW_32);
            rule__XmlTagElement__Group_1_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__1"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__1__Impl"
    // InternalQuizDSL.g:2980:1: rule__XmlTagElement__Group_1_1__1__Impl : ( ( rule__XmlTagElement__ContentsAssignment_1_1_1 )* ) ;
    public final void rule__XmlTagElement__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2984:1: ( ( ( rule__XmlTagElement__ContentsAssignment_1_1_1 )* ) )
            // InternalQuizDSL.g:2985:1: ( ( rule__XmlTagElement__ContentsAssignment_1_1_1 )* )
            {
            // InternalQuizDSL.g:2985:1: ( ( rule__XmlTagElement__ContentsAssignment_1_1_1 )* )
            // InternalQuizDSL.g:2986:2: ( rule__XmlTagElement__ContentsAssignment_1_1_1 )*
            {
             before(grammarAccess.getXmlTagElementAccess().getContentsAssignment_1_1_1()); 
            // InternalQuizDSL.g:2987:2: ( rule__XmlTagElement__ContentsAssignment_1_1_1 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==RULE_ID||LA33_0==30) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalQuizDSL.g:2987:3: rule__XmlTagElement__ContentsAssignment_1_1_1
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__XmlTagElement__ContentsAssignment_1_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getXmlTagElementAccess().getContentsAssignment_1_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__1__Impl"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__2"
    // InternalQuizDSL.g:2995:1: rule__XmlTagElement__Group_1_1__2 : rule__XmlTagElement__Group_1_1__2__Impl ;
    public final void rule__XmlTagElement__Group_1_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:2999:1: ( rule__XmlTagElement__Group_1_1__2__Impl )
            // InternalQuizDSL.g:3000:2: rule__XmlTagElement__Group_1_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__2"


    // $ANTLR start "rule__XmlTagElement__Group_1_1__2__Impl"
    // InternalQuizDSL.g:3006:1: rule__XmlTagElement__Group_1_1__2__Impl : ( ( rule__XmlTagElement__Group_1_1_2__0 ) ) ;
    public final void rule__XmlTagElement__Group_1_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3010:1: ( ( ( rule__XmlTagElement__Group_1_1_2__0 ) ) )
            // InternalQuizDSL.g:3011:1: ( ( rule__XmlTagElement__Group_1_1_2__0 ) )
            {
            // InternalQuizDSL.g:3011:1: ( ( rule__XmlTagElement__Group_1_1_2__0 ) )
            // InternalQuizDSL.g:3012:2: ( rule__XmlTagElement__Group_1_1_2__0 )
            {
             before(grammarAccess.getXmlTagElementAccess().getGroup_1_1_2()); 
            // InternalQuizDSL.g:3013:2: ( rule__XmlTagElement__Group_1_1_2__0 )
            // InternalQuizDSL.g:3013:3: rule__XmlTagElement__Group_1_1_2__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1_2__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagElementAccess().getGroup_1_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1__2__Impl"


    // $ANTLR start "rule__XmlTagElement__Group_1_1_2__0"
    // InternalQuizDSL.g:3022:1: rule__XmlTagElement__Group_1_1_2__0 : rule__XmlTagElement__Group_1_1_2__0__Impl rule__XmlTagElement__Group_1_1_2__1 ;
    public final void rule__XmlTagElement__Group_1_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3026:1: ( rule__XmlTagElement__Group_1_1_2__0__Impl rule__XmlTagElement__Group_1_1_2__1 )
            // InternalQuizDSL.g:3027:2: rule__XmlTagElement__Group_1_1_2__0__Impl rule__XmlTagElement__Group_1_1_2__1
            {
            pushFollow(FOLLOW_12);
            rule__XmlTagElement__Group_1_1_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1_2__0"


    // $ANTLR start "rule__XmlTagElement__Group_1_1_2__0__Impl"
    // InternalQuizDSL.g:3034:1: rule__XmlTagElement__Group_1_1_2__0__Impl : ( '/' ) ;
    public final void rule__XmlTagElement__Group_1_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3038:1: ( ( '/' ) )
            // InternalQuizDSL.g:3039:1: ( '/' )
            {
            // InternalQuizDSL.g:3039:1: ( '/' )
            // InternalQuizDSL.g:3040:2: '/'
            {
             before(grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_1_2_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getXmlTagElementAccess().getSolidusKeyword_1_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1_2__0__Impl"


    // $ANTLR start "rule__XmlTagElement__Group_1_1_2__1"
    // InternalQuizDSL.g:3049:1: rule__XmlTagElement__Group_1_1_2__1 : rule__XmlTagElement__Group_1_1_2__1__Impl ;
    public final void rule__XmlTagElement__Group_1_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3053:1: ( rule__XmlTagElement__Group_1_1_2__1__Impl )
            // InternalQuizDSL.g:3054:2: rule__XmlTagElement__Group_1_1_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlTagElement__Group_1_1_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1_2__1"


    // $ANTLR start "rule__XmlTagElement__Group_1_1_2__1__Impl"
    // InternalQuizDSL.g:3060:1: rule__XmlTagElement__Group_1_1_2__1__Impl : ( ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )? ) ;
    public final void rule__XmlTagElement__Group_1_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3064:1: ( ( ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )? ) )
            // InternalQuizDSL.g:3065:1: ( ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )? )
            {
            // InternalQuizDSL.g:3065:1: ( ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )? )
            // InternalQuizDSL.g:3066:2: ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )?
            {
             before(grammarAccess.getXmlTagElementAccess().getEndTagAssignment_1_1_2_1()); 
            // InternalQuizDSL.g:3067:2: ( rule__XmlTagElement__EndTagAssignment_1_1_2_1 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_ID) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalQuizDSL.g:3067:3: rule__XmlTagElement__EndTagAssignment_1_1_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlTagElement__EndTagAssignment_1_1_2_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getXmlTagElementAccess().getEndTagAssignment_1_1_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__Group_1_1_2__1__Impl"


    // $ANTLR start "rule__XmlTag__Group__0"
    // InternalQuizDSL.g:3076:1: rule__XmlTag__Group__0 : rule__XmlTag__Group__0__Impl rule__XmlTag__Group__1 ;
    public final void rule__XmlTag__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3080:1: ( rule__XmlTag__Group__0__Impl rule__XmlTag__Group__1 )
            // InternalQuizDSL.g:3081:2: rule__XmlTag__Group__0__Impl rule__XmlTag__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__XmlTag__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlTag__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__Group__0"


    // $ANTLR start "rule__XmlTag__Group__0__Impl"
    // InternalQuizDSL.g:3088:1: rule__XmlTag__Group__0__Impl : ( ( rule__XmlTag__NameAssignment_0 ) ) ;
    public final void rule__XmlTag__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3092:1: ( ( ( rule__XmlTag__NameAssignment_0 ) ) )
            // InternalQuizDSL.g:3093:1: ( ( rule__XmlTag__NameAssignment_0 ) )
            {
            // InternalQuizDSL.g:3093:1: ( ( rule__XmlTag__NameAssignment_0 ) )
            // InternalQuizDSL.g:3094:2: ( rule__XmlTag__NameAssignment_0 )
            {
             before(grammarAccess.getXmlTagAccess().getNameAssignment_0()); 
            // InternalQuizDSL.g:3095:2: ( rule__XmlTag__NameAssignment_0 )
            // InternalQuizDSL.g:3095:3: rule__XmlTag__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlTag__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlTagAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__Group__0__Impl"


    // $ANTLR start "rule__XmlTag__Group__1"
    // InternalQuizDSL.g:3103:1: rule__XmlTag__Group__1 : rule__XmlTag__Group__1__Impl ;
    public final void rule__XmlTag__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3107:1: ( rule__XmlTag__Group__1__Impl )
            // InternalQuizDSL.g:3108:2: rule__XmlTag__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlTag__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__Group__1"


    // $ANTLR start "rule__XmlTag__Group__1__Impl"
    // InternalQuizDSL.g:3114:1: rule__XmlTag__Group__1__Impl : ( ( rule__XmlTag__AttributesAssignment_1 )* ) ;
    public final void rule__XmlTag__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3118:1: ( ( ( rule__XmlTag__AttributesAssignment_1 )* ) )
            // InternalQuizDSL.g:3119:1: ( ( rule__XmlTag__AttributesAssignment_1 )* )
            {
            // InternalQuizDSL.g:3119:1: ( ( rule__XmlTag__AttributesAssignment_1 )* )
            // InternalQuizDSL.g:3120:2: ( rule__XmlTag__AttributesAssignment_1 )*
            {
             before(grammarAccess.getXmlTagAccess().getAttributesAssignment_1()); 
            // InternalQuizDSL.g:3121:2: ( rule__XmlTag__AttributesAssignment_1 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_ID) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalQuizDSL.g:3121:3: rule__XmlTag__AttributesAssignment_1
            	    {
            	    pushFollow(FOLLOW_34);
            	    rule__XmlTag__AttributesAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getXmlTagAccess().getAttributesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__Group__1__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__0"
    // InternalQuizDSL.g:3130:1: rule__XmlAttribute__Group__0 : rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1 ;
    public final void rule__XmlAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3134:1: ( rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1 )
            // InternalQuizDSL.g:3135:2: rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__XmlAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__0"


    // $ANTLR start "rule__XmlAttribute__Group__0__Impl"
    // InternalQuizDSL.g:3142:1: rule__XmlAttribute__Group__0__Impl : ( ( rule__XmlAttribute__NameAssignment_0 ) ) ;
    public final void rule__XmlAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3146:1: ( ( ( rule__XmlAttribute__NameAssignment_0 ) ) )
            // InternalQuizDSL.g:3147:1: ( ( rule__XmlAttribute__NameAssignment_0 ) )
            {
            // InternalQuizDSL.g:3147:1: ( ( rule__XmlAttribute__NameAssignment_0 ) )
            // InternalQuizDSL.g:3148:2: ( rule__XmlAttribute__NameAssignment_0 )
            {
             before(grammarAccess.getXmlAttributeAccess().getNameAssignment_0()); 
            // InternalQuizDSL.g:3149:2: ( rule__XmlAttribute__NameAssignment_0 )
            // InternalQuizDSL.g:3149:3: rule__XmlAttribute__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__0__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__1"
    // InternalQuizDSL.g:3157:1: rule__XmlAttribute__Group__1 : rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2 ;
    public final void rule__XmlAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3161:1: ( rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2 )
            // InternalQuizDSL.g:3162:2: rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__XmlAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__1"


    // $ANTLR start "rule__XmlAttribute__Group__1__Impl"
    // InternalQuizDSL.g:3169:1: rule__XmlAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__XmlAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3173:1: ( ( '=' ) )
            // InternalQuizDSL.g:3174:1: ( '=' )
            {
            // InternalQuizDSL.g:3174:1: ( '=' )
            // InternalQuizDSL.g:3175:2: '='
            {
             before(grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__1__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__2"
    // InternalQuizDSL.g:3184:1: rule__XmlAttribute__Group__2 : rule__XmlAttribute__Group__2__Impl ;
    public final void rule__XmlAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3188:1: ( rule__XmlAttribute__Group__2__Impl )
            // InternalQuizDSL.g:3189:2: rule__XmlAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__2"


    // $ANTLR start "rule__XmlAttribute__Group__2__Impl"
    // InternalQuizDSL.g:3195:1: rule__XmlAttribute__Group__2__Impl : ( ( rule__XmlAttribute__ValueAssignment_2 ) ) ;
    public final void rule__XmlAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3199:1: ( ( ( rule__XmlAttribute__ValueAssignment_2 ) ) )
            // InternalQuizDSL.g:3200:1: ( ( rule__XmlAttribute__ValueAssignment_2 ) )
            {
            // InternalQuizDSL.g:3200:1: ( ( rule__XmlAttribute__ValueAssignment_2 ) )
            // InternalQuizDSL.g:3201:2: ( rule__XmlAttribute__ValueAssignment_2 )
            {
             before(grammarAccess.getXmlAttributeAccess().getValueAssignment_2()); 
            // InternalQuizDSL.g:3202:2: ( rule__XmlAttribute__ValueAssignment_2 )
            // InternalQuizDSL.g:3202:3: rule__XmlAttribute__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__2__Impl"


    // $ANTLR start "rule__Quiz__NameAssignment_2_0_0"
    // InternalQuizDSL.g:3211:1: rule__Quiz__NameAssignment_2_0_0 : ( ruleQName ) ;
    public final void rule__Quiz__NameAssignment_2_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3215:1: ( ( ruleQName ) )
            // InternalQuizDSL.g:3216:2: ( ruleQName )
            {
            // InternalQuizDSL.g:3216:2: ( ruleQName )
            // InternalQuizDSL.g:3217:3: ruleQName
            {
             before(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_2_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_2_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__NameAssignment_2_0_0"


    // $ANTLR start "rule__Quiz__TitleAssignment_2_0_1"
    // InternalQuizDSL.g:3226:1: rule__Quiz__TitleAssignment_2_0_1 : ( RULE_STRING ) ;
    public final void rule__Quiz__TitleAssignment_2_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3230:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3231:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3231:2: ( RULE_STRING )
            // InternalQuizDSL.g:3232:3: RULE_STRING
            {
             before(grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__TitleAssignment_2_0_1"


    // $ANTLR start "rule__Quiz__PartsAssignment_2_0_2"
    // InternalQuizDSL.g:3241:1: rule__Quiz__PartsAssignment_2_0_2 : ( ruleAbstractQuizPart ) ;
    public final void rule__Quiz__PartsAssignment_2_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3245:1: ( ( ruleAbstractQuizPart ) )
            // InternalQuizDSL.g:3246:2: ( ruleAbstractQuizPart )
            {
            // InternalQuizDSL.g:3246:2: ( ruleAbstractQuizPart )
            // InternalQuizDSL.g:3247:3: ruleAbstractQuizPart
            {
             before(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_2_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractQuizPart();

            state._fsp--;

             after(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_2_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__PartsAssignment_2_0_2"


    // $ANTLR start "rule__Quiz__PartsAssignment_2_1"
    // InternalQuizDSL.g:3256:1: rule__Quiz__PartsAssignment_2_1 : ( ruleAnonymousQuizPart ) ;
    public final void rule__Quiz__PartsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3260:1: ( ( ruleAnonymousQuizPart ) )
            // InternalQuizDSL.g:3261:2: ( ruleAnonymousQuizPart )
            {
            // InternalQuizDSL.g:3261:2: ( ruleAnonymousQuizPart )
            // InternalQuizDSL.g:3262:3: ruleAnonymousQuizPart
            {
             before(grammarAccess.getQuizAccess().getPartsAnonymousQuizPartParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAnonymousQuizPart();

            state._fsp--;

             after(grammarAccess.getQuizAccess().getPartsAnonymousQuizPartParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__PartsAssignment_2_1"


    // $ANTLR start "rule__QuizPart__NameAssignment_1"
    // InternalQuizDSL.g:3271:1: rule__QuizPart__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__QuizPart__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3275:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:3276:2: ( RULE_ID )
            {
            // InternalQuizDSL.g:3276:2: ( RULE_ID )
            // InternalQuizDSL.g:3277:3: RULE_ID
            {
             before(grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__NameAssignment_1"


    // $ANTLR start "rule__QuizPart__TitleAssignment_2"
    // InternalQuizDSL.g:3286:1: rule__QuizPart__TitleAssignment_2 : ( RULE_STRING ) ;
    public final void rule__QuizPart__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3290:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3291:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3291:2: ( RULE_STRING )
            // InternalQuizDSL.g:3292:3: RULE_STRING
            {
             before(grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__TitleAssignment_2"


    // $ANTLR start "rule__QuizPart__QuestionsAssignment_3"
    // InternalQuizDSL.g:3301:1: rule__QuizPart__QuestionsAssignment_3 : ( ruleAbstractQA ) ;
    public final void rule__QuizPart__QuestionsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3305:1: ( ( ruleAbstractQA ) )
            // InternalQuizDSL.g:3306:2: ( ruleAbstractQA )
            {
            // InternalQuizDSL.g:3306:2: ( ruleAbstractQA )
            // InternalQuizDSL.g:3307:3: ruleAbstractQA
            {
             before(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractQA();

            state._fsp--;

             after(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__QuestionsAssignment_3"


    // $ANTLR start "rule__QuizPartRef__PartRefAssignment_2"
    // InternalQuizDSL.g:3316:1: rule__QuizPartRef__PartRefAssignment_2 : ( ( ruleQName ) ) ;
    public final void rule__QuizPartRef__PartRefAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3320:1: ( ( ( ruleQName ) ) )
            // InternalQuizDSL.g:3321:2: ( ( ruleQName ) )
            {
            // InternalQuizDSL.g:3321:2: ( ( ruleQName ) )
            // InternalQuizDSL.g:3322:3: ( ruleQName )
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0()); 
            // InternalQuizDSL.g:3323:3: ( ruleQName )
            // InternalQuizDSL.g:3324:4: ruleQName
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartQNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartQNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__PartRefAssignment_2"


    // $ANTLR start "rule__AnonymousQuizPart__QuestionsAssignment_1"
    // InternalQuizDSL.g:3335:1: rule__AnonymousQuizPart__QuestionsAssignment_1 : ( ruleAbstractQA ) ;
    public final void rule__AnonymousQuizPart__QuestionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3339:1: ( ( ruleAbstractQA ) )
            // InternalQuizDSL.g:3340:2: ( ruleAbstractQA )
            {
            // InternalQuizDSL.g:3340:2: ( ruleAbstractQA )
            // InternalQuizDSL.g:3341:3: ruleAbstractQA
            {
             before(grammarAccess.getAnonymousQuizPartAccess().getQuestionsAbstractQAParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractQA();

            state._fsp--;

             after(grammarAccess.getAnonymousQuizPartAccess().getQuestionsAbstractQAParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnonymousQuizPart__QuestionsAssignment_1"


    // $ANTLR start "rule__QARef__QaRefAssignment_1"
    // InternalQuizDSL.g:3350:1: rule__QARef__QaRefAssignment_1 : ( ( ruleQName ) ) ;
    public final void rule__QARef__QaRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3354:1: ( ( ( ruleQName ) ) )
            // InternalQuizDSL.g:3355:2: ( ( ruleQName ) )
            {
            // InternalQuizDSL.g:3355:2: ( ( ruleQName ) )
            // InternalQuizDSL.g:3356:3: ( ruleQName )
            {
             before(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0()); 
            // InternalQuizDSL.g:3357:3: ( ruleQName )
            // InternalQuizDSL.g:3358:4: ruleQName
            {
             before(grammarAccess.getQARefAccess().getQaRefQAQNameParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQARefAccess().getQaRefQAQNameParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__QaRefAssignment_1"


    // $ANTLR start "rule__QA__NameAssignment_0"
    // InternalQuizDSL.g:3369:1: rule__QA__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__QA__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3373:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:3374:2: ( RULE_ID )
            {
            // InternalQuizDSL.g:3374:2: ( RULE_ID )
            // InternalQuizDSL.g:3375:3: RULE_ID
            {
             before(grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__NameAssignment_0"


    // $ANTLR start "rule__QA__QAssignment_1"
    // InternalQuizDSL.g:3384:1: rule__QA__QAssignment_1 : ( ruleQuestion ) ;
    public final void rule__QA__QAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3388:1: ( ( ruleQuestion ) )
            // InternalQuizDSL.g:3389:2: ( ruleQuestion )
            {
            // InternalQuizDSL.g:3389:2: ( ruleQuestion )
            // InternalQuizDSL.g:3390:3: ruleQuestion
            {
             before(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__QAssignment_1"


    // $ANTLR start "rule__QA__AAssignment_2"
    // InternalQuizDSL.g:3399:1: rule__QA__AAssignment_2 : ( ruleAnswer ) ;
    public final void rule__QA__AAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3403:1: ( ( ruleAnswer ) )
            // InternalQuizDSL.g:3404:2: ( ruleAnswer )
            {
            // InternalQuizDSL.g:3404:2: ( ruleAnswer )
            // InternalQuizDSL.g:3405:3: ruleAnswer
            {
             before(grammarAccess.getQAAccess().getAAnswerParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnswer();

            state._fsp--;

             after(grammarAccess.getQAAccess().getAAnswerParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__AAssignment_2"


    // $ANTLR start "rule__StringQuestion__QuestionAssignment"
    // InternalQuizDSL.g:3414:1: rule__StringQuestion__QuestionAssignment : ( RULE_STRING ) ;
    public final void rule__StringQuestion__QuestionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3418:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3419:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3419:2: ( RULE_STRING )
            // InternalQuizDSL.g:3420:3: RULE_STRING
            {
             before(grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringQuestion__QuestionAssignment"


    // $ANTLR start "rule__XmlQuestion__XmlAssignment"
    // InternalQuizDSL.g:3429:1: rule__XmlQuestion__XmlAssignment : ( ruleXml ) ;
    public final void rule__XmlQuestion__XmlAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3433:1: ( ( ruleXml ) )
            // InternalQuizDSL.g:3434:2: ( ruleXml )
            {
            // InternalQuizDSL.g:3434:2: ( ruleXml )
            // InternalQuizDSL.g:3435:3: ruleXml
            {
             before(grammarAccess.getXmlQuestionAccess().getXmlXmlParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleXml();

            state._fsp--;

             after(grammarAccess.getXmlQuestionAccess().getXmlXmlParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlQuestion__XmlAssignment"


    // $ANTLR start "rule__StringAnswer__ValueAssignment_0"
    // InternalQuizDSL.g:3444:1: rule__StringAnswer__ValueAssignment_0 : ( RULE_STRING ) ;
    public final void rule__StringAnswer__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3448:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3449:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3449:2: ( RULE_STRING )
            // InternalQuizDSL.g:3450:3: RULE_STRING
            {
             before(grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__ValueAssignment_0"


    // $ANTLR start "rule__StringAnswer__IgnoreCaseAssignment_1"
    // InternalQuizDSL.g:3459:1: rule__StringAnswer__IgnoreCaseAssignment_1 : ( ( '~' ) ) ;
    public final void rule__StringAnswer__IgnoreCaseAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3463:1: ( ( ( '~' ) ) )
            // InternalQuizDSL.g:3464:2: ( ( '~' ) )
            {
            // InternalQuizDSL.g:3464:2: ( ( '~' ) )
            // InternalQuizDSL.g:3465:3: ( '~' )
            {
             before(grammarAccess.getStringAnswerAccess().getIgnoreCaseTildeKeyword_1_0()); 
            // InternalQuizDSL.g:3466:3: ( '~' )
            // InternalQuizDSL.g:3467:4: '~'
            {
             before(grammarAccess.getStringAnswerAccess().getIgnoreCaseTildeKeyword_1_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getStringAnswerAccess().getIgnoreCaseTildeKeyword_1_0()); 

            }

             after(grammarAccess.getStringAnswerAccess().getIgnoreCaseTildeKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__IgnoreCaseAssignment_1"


    // $ANTLR start "rule__RegexAnswer__RegexpAssignment_0"
    // InternalQuizDSL.g:3478:1: rule__RegexAnswer__RegexpAssignment_0 : ( ( '/' ) ) ;
    public final void rule__RegexAnswer__RegexpAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3482:1: ( ( ( '/' ) ) )
            // InternalQuizDSL.g:3483:2: ( ( '/' ) )
            {
            // InternalQuizDSL.g:3483:2: ( ( '/' ) )
            // InternalQuizDSL.g:3484:3: ( '/' )
            {
             before(grammarAccess.getRegexAnswerAccess().getRegexpSolidusKeyword_0_0()); 
            // InternalQuizDSL.g:3485:3: ( '/' )
            // InternalQuizDSL.g:3486:4: '/'
            {
             before(grammarAccess.getRegexAnswerAccess().getRegexpSolidusKeyword_0_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getRegexAnswerAccess().getRegexpSolidusKeyword_0_0()); 

            }

             after(grammarAccess.getRegexAnswerAccess().getRegexpSolidusKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__RegexpAssignment_0"


    // $ANTLR start "rule__RegexAnswer__ValueAssignment_1"
    // InternalQuizDSL.g:3497:1: rule__RegexAnswer__ValueAssignment_1 : ( RULE_STRING ) ;
    public final void rule__RegexAnswer__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3501:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3502:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3502:2: ( RULE_STRING )
            // InternalQuizDSL.g:3503:3: RULE_STRING
            {
             before(grammarAccess.getRegexAnswerAccess().getValueSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getRegexAnswerAccess().getValueSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__ValueAssignment_1"


    // $ANTLR start "rule__RegexAnswer__IgnoreCaseAssignment_3"
    // InternalQuizDSL.g:3512:1: rule__RegexAnswer__IgnoreCaseAssignment_3 : ( ( '~' ) ) ;
    public final void rule__RegexAnswer__IgnoreCaseAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3516:1: ( ( ( '~' ) ) )
            // InternalQuizDSL.g:3517:2: ( ( '~' ) )
            {
            // InternalQuizDSL.g:3517:2: ( ( '~' ) )
            // InternalQuizDSL.g:3518:3: ( '~' )
            {
             before(grammarAccess.getRegexAnswerAccess().getIgnoreCaseTildeKeyword_3_0()); 
            // InternalQuizDSL.g:3519:3: ( '~' )
            // InternalQuizDSL.g:3520:4: '~'
            {
             before(grammarAccess.getRegexAnswerAccess().getIgnoreCaseTildeKeyword_3_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getRegexAnswerAccess().getIgnoreCaseTildeKeyword_3_0()); 

            }

             after(grammarAccess.getRegexAnswerAccess().getIgnoreCaseTildeKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegexAnswer__IgnoreCaseAssignment_3"


    // $ANTLR start "rule__NumberAnswer__ValueAssignment_0"
    // InternalQuizDSL.g:3531:1: rule__NumberAnswer__ValueAssignment_0 : ( ruleEDoubleObject ) ;
    public final void rule__NumberAnswer__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3535:1: ( ( ruleEDoubleObject ) )
            // InternalQuizDSL.g:3536:2: ( ruleEDoubleObject )
            {
            // InternalQuizDSL.g:3536:2: ( ruleEDoubleObject )
            // InternalQuizDSL.g:3537:3: ruleEDoubleObject
            {
             before(grammarAccess.getNumberAnswerAccess().getValueEDoubleObjectParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEDoubleObject();

            state._fsp--;

             after(grammarAccess.getNumberAnswerAccess().getValueEDoubleObjectParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__ValueAssignment_0"


    // $ANTLR start "rule__NumberAnswer__ErrorMarginAssignment_1_1"
    // InternalQuizDSL.g:3546:1: rule__NumberAnswer__ErrorMarginAssignment_1_1 : ( ruleEDoubleObject ) ;
    public final void rule__NumberAnswer__ErrorMarginAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3550:1: ( ( ruleEDoubleObject ) )
            // InternalQuizDSL.g:3551:2: ( ruleEDoubleObject )
            {
            // InternalQuizDSL.g:3551:2: ( ruleEDoubleObject )
            // InternalQuizDSL.g:3552:3: ruleEDoubleObject
            {
             before(grammarAccess.getNumberAnswerAccess().getErrorMarginEDoubleObjectParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEDoubleObject();

            state._fsp--;

             after(grammarAccess.getNumberAnswerAccess().getErrorMarginEDoubleObjectParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__ErrorMarginAssignment_1_1"


    // $ANTLR start "rule__BooleanAnswer__ValueAssignment_1_0"
    // InternalQuizDSL.g:3561:1: rule__BooleanAnswer__ValueAssignment_1_0 : ( ( rule__BooleanAnswer__ValueAlternatives_1_0_0 ) ) ;
    public final void rule__BooleanAnswer__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3565:1: ( ( ( rule__BooleanAnswer__ValueAlternatives_1_0_0 ) ) )
            // InternalQuizDSL.g:3566:2: ( ( rule__BooleanAnswer__ValueAlternatives_1_0_0 ) )
            {
            // InternalQuizDSL.g:3566:2: ( ( rule__BooleanAnswer__ValueAlternatives_1_0_0 ) )
            // InternalQuizDSL.g:3567:3: ( rule__BooleanAnswer__ValueAlternatives_1_0_0 )
            {
             before(grammarAccess.getBooleanAnswerAccess().getValueAlternatives_1_0_0()); 
            // InternalQuizDSL.g:3568:3: ( rule__BooleanAnswer__ValueAlternatives_1_0_0 )
            // InternalQuizDSL.g:3568:4: rule__BooleanAnswer__ValueAlternatives_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__ValueAlternatives_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAnswerAccess().getValueAlternatives_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__ValueAssignment_1_0"


    // $ANTLR start "rule__XmlAnswer__XmlAssignment"
    // InternalQuizDSL.g:3576:1: rule__XmlAnswer__XmlAssignment : ( ruleXml ) ;
    public final void rule__XmlAnswer__XmlAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3580:1: ( ( ruleXml ) )
            // InternalQuizDSL.g:3581:2: ( ruleXml )
            {
            // InternalQuizDSL.g:3581:2: ( ruleXml )
            // InternalQuizDSL.g:3582:3: ruleXml
            {
             before(grammarAccess.getXmlAnswerAccess().getXmlXmlParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleXml();

            state._fsp--;

             after(grammarAccess.getXmlAnswerAccess().getXmlXmlParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAnswer__XmlAssignment"


    // $ANTLR start "rule__SingleBoxOptionsAnswer__OptionsAssignment"
    // InternalQuizDSL.g:3591:1: rule__SingleBoxOptionsAnswer__OptionsAssignment : ( ruleSingleBoxOption ) ;
    public final void rule__SingleBoxOptionsAnswer__OptionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3595:1: ( ( ruleSingleBoxOption ) )
            // InternalQuizDSL.g:3596:2: ( ruleSingleBoxOption )
            {
            // InternalQuizDSL.g:3596:2: ( ruleSingleBoxOption )
            // InternalQuizDSL.g:3597:3: ruleSingleBoxOption
            {
             before(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsSingleBoxOptionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSingleBoxOption();

            state._fsp--;

             after(grammarAccess.getSingleBoxOptionsAnswerAccess().getOptionsSingleBoxOptionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOptionsAnswer__OptionsAssignment"


    // $ANTLR start "rule__SingleBoxOption__CorrectAssignment_1"
    // InternalQuizDSL.g:3606:1: rule__SingleBoxOption__CorrectAssignment_1 : ( ( 'x' ) ) ;
    public final void rule__SingleBoxOption__CorrectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3610:1: ( ( ( 'x' ) ) )
            // InternalQuizDSL.g:3611:2: ( ( 'x' ) )
            {
            // InternalQuizDSL.g:3611:2: ( ( 'x' ) )
            // InternalQuizDSL.g:3612:3: ( 'x' )
            {
             before(grammarAccess.getSingleBoxOptionAccess().getCorrectXKeyword_1_0()); 
            // InternalQuizDSL.g:3613:3: ( 'x' )
            // InternalQuizDSL.g:3614:4: 'x'
            {
             before(grammarAccess.getSingleBoxOptionAccess().getCorrectXKeyword_1_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSingleBoxOptionAccess().getCorrectXKeyword_1_0()); 

            }

             after(grammarAccess.getSingleBoxOptionAccess().getCorrectXKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__CorrectAssignment_1"


    // $ANTLR start "rule__SingleBoxOption__OptionAssignment_3"
    // InternalQuizDSL.g:3625:1: rule__SingleBoxOption__OptionAssignment_3 : ( ruleOptionAnswer ) ;
    public final void rule__SingleBoxOption__OptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3629:1: ( ( ruleOptionAnswer ) )
            // InternalQuizDSL.g:3630:2: ( ruleOptionAnswer )
            {
            // InternalQuizDSL.g:3630:2: ( ruleOptionAnswer )
            // InternalQuizDSL.g:3631:3: ruleOptionAnswer
            {
             before(grammarAccess.getSingleBoxOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getSingleBoxOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleBoxOption__OptionAssignment_3"


    // $ANTLR start "rule__SingleListOptionsAnswer__OptionsAssignment"
    // InternalQuizDSL.g:3640:1: rule__SingleListOptionsAnswer__OptionsAssignment : ( ruleSingleListOption ) ;
    public final void rule__SingleListOptionsAnswer__OptionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3644:1: ( ( ruleSingleListOption ) )
            // InternalQuizDSL.g:3645:2: ( ruleSingleListOption )
            {
            // InternalQuizDSL.g:3645:2: ( ruleSingleListOption )
            // InternalQuizDSL.g:3646:3: ruleSingleListOption
            {
             before(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSingleListOption();

            state._fsp--;

             after(grammarAccess.getSingleListOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOptionsAnswer__OptionsAssignment"


    // $ANTLR start "rule__SingleListOption__CorrectAssignment_0_1"
    // InternalQuizDSL.g:3655:1: rule__SingleListOption__CorrectAssignment_0_1 : ( ( 'v' ) ) ;
    public final void rule__SingleListOption__CorrectAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3659:1: ( ( ( 'v' ) ) )
            // InternalQuizDSL.g:3660:2: ( ( 'v' ) )
            {
            // InternalQuizDSL.g:3660:2: ( ( 'v' ) )
            // InternalQuizDSL.g:3661:3: ( 'v' )
            {
             before(grammarAccess.getSingleListOptionAccess().getCorrectVKeyword_0_1_0()); 
            // InternalQuizDSL.g:3662:3: ( 'v' )
            // InternalQuizDSL.g:3663:4: 'v'
            {
             before(grammarAccess.getSingleListOptionAccess().getCorrectVKeyword_0_1_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getSingleListOptionAccess().getCorrectVKeyword_0_1_0()); 

            }

             after(grammarAccess.getSingleListOptionAccess().getCorrectVKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__CorrectAssignment_0_1"


    // $ANTLR start "rule__SingleListOption__OptionAssignment_1"
    // InternalQuizDSL.g:3674:1: rule__SingleListOption__OptionAssignment_1 : ( ruleOptionAnswer ) ;
    public final void rule__SingleListOption__OptionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3678:1: ( ( ruleOptionAnswer ) )
            // InternalQuizDSL.g:3679:2: ( ruleOptionAnswer )
            {
            // InternalQuizDSL.g:3679:2: ( ruleOptionAnswer )
            // InternalQuizDSL.g:3680:3: ruleOptionAnswer
            {
             before(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__OptionAssignment_1"


    // $ANTLR start "rule__ManyOptionsAnswer__OptionsAssignment"
    // InternalQuizDSL.g:3689:1: rule__ManyOptionsAnswer__OptionsAssignment : ( ruleManyOption ) ;
    public final void rule__ManyOptionsAnswer__OptionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3693:1: ( ( ruleManyOption ) )
            // InternalQuizDSL.g:3694:2: ( ruleManyOption )
            {
            // InternalQuizDSL.g:3694:2: ( ruleManyOption )
            // InternalQuizDSL.g:3695:3: ruleManyOption
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleManyOption();

            state._fsp--;

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOptionsAnswer__OptionsAssignment"


    // $ANTLR start "rule__ManyOption__CorrectAssignment_1"
    // InternalQuizDSL.g:3704:1: rule__ManyOption__CorrectAssignment_1 : ( ( 'x' ) ) ;
    public final void rule__ManyOption__CorrectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3708:1: ( ( ( 'x' ) ) )
            // InternalQuizDSL.g:3709:2: ( ( 'x' ) )
            {
            // InternalQuizDSL.g:3709:2: ( ( 'x' ) )
            // InternalQuizDSL.g:3710:3: ( 'x' )
            {
             before(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 
            // InternalQuizDSL.g:3711:3: ( 'x' )
            // InternalQuizDSL.g:3712:4: 'x'
            {
             before(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 

            }

             after(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__CorrectAssignment_1"


    // $ANTLR start "rule__ManyOption__OptionAssignment_3"
    // InternalQuizDSL.g:3723:1: rule__ManyOption__OptionAssignment_3 : ( ruleOptionAnswer ) ;
    public final void rule__ManyOption__OptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3727:1: ( ( ruleOptionAnswer ) )
            // InternalQuizDSL.g:3728:2: ( ruleOptionAnswer )
            {
            // InternalQuizDSL.g:3728:2: ( ruleOptionAnswer )
            // InternalQuizDSL.g:3729:3: ruleOptionAnswer
            {
             before(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__OptionAssignment_3"


    // $ANTLR start "rule__Xml__ElementAssignment_1"
    // InternalQuizDSL.g:3738:1: rule__Xml__ElementAssignment_1 : ( ruleXmlElement ) ;
    public final void rule__Xml__ElementAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3742:1: ( ( ruleXmlElement ) )
            // InternalQuizDSL.g:3743:2: ( ruleXmlElement )
            {
            // InternalQuizDSL.g:3743:2: ( ruleXmlElement )
            // InternalQuizDSL.g:3744:3: ruleXmlElement
            {
             before(grammarAccess.getXmlAccess().getElementXmlElementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlElement();

            state._fsp--;

             after(grammarAccess.getXmlAccess().getElementXmlElementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xml__ElementAssignment_1"


    // $ANTLR start "rule__XmlContents__ElementAssignment_0"
    // InternalQuizDSL.g:3753:1: rule__XmlContents__ElementAssignment_0 : ( ruleXmlElement ) ;
    public final void rule__XmlContents__ElementAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3757:1: ( ( ruleXmlElement ) )
            // InternalQuizDSL.g:3758:2: ( ruleXmlElement )
            {
            // InternalQuizDSL.g:3758:2: ( ruleXmlElement )
            // InternalQuizDSL.g:3759:3: ruleXmlElement
            {
             before(grammarAccess.getXmlContentsAccess().getElementXmlElementParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlElement();

            state._fsp--;

             after(grammarAccess.getXmlContentsAccess().getElementXmlElementParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__ElementAssignment_0"


    // $ANTLR start "rule__XmlContents__PostAssignment_1"
    // InternalQuizDSL.g:3768:1: rule__XmlContents__PostAssignment_1 : ( RULE_XML_TEXT ) ;
    public final void rule__XmlContents__PostAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3772:1: ( ( RULE_XML_TEXT ) )
            // InternalQuizDSL.g:3773:2: ( RULE_XML_TEXT )
            {
            // InternalQuizDSL.g:3773:2: ( RULE_XML_TEXT )
            // InternalQuizDSL.g:3774:3: RULE_XML_TEXT
            {
             before(grammarAccess.getXmlContentsAccess().getPostXML_TEXTTerminalRuleCall_1_0()); 
            match(input,RULE_XML_TEXT,FOLLOW_2); 
             after(grammarAccess.getXmlContentsAccess().getPostXML_TEXTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlContents__PostAssignment_1"


    // $ANTLR start "rule__XmlPIAnswerElement__AnswerAssignment_1"
    // InternalQuizDSL.g:3783:1: rule__XmlPIAnswerElement__AnswerAssignment_1 : ( ruleSimpleAnswer ) ;
    public final void rule__XmlPIAnswerElement__AnswerAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3787:1: ( ( ruleSimpleAnswer ) )
            // InternalQuizDSL.g:3788:2: ( ruleSimpleAnswer )
            {
            // InternalQuizDSL.g:3788:2: ( ruleSimpleAnswer )
            // InternalQuizDSL.g:3789:3: ruleSimpleAnswer
            {
             before(grammarAccess.getXmlPIAnswerElementAccess().getAnswerSimpleAnswerParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSimpleAnswer();

            state._fsp--;

             after(grammarAccess.getXmlPIAnswerElementAccess().getAnswerSimpleAnswerParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlPIAnswerElement__AnswerAssignment_1"


    // $ANTLR start "rule__XmlTagElement__StartTagAssignment_0"
    // InternalQuizDSL.g:3798:1: rule__XmlTagElement__StartTagAssignment_0 : ( ruleXmlTag ) ;
    public final void rule__XmlTagElement__StartTagAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3802:1: ( ( ruleXmlTag ) )
            // InternalQuizDSL.g:3803:2: ( ruleXmlTag )
            {
            // InternalQuizDSL.g:3803:2: ( ruleXmlTag )
            // InternalQuizDSL.g:3804:3: ruleXmlTag
            {
             before(grammarAccess.getXmlTagElementAccess().getStartTagXmlTagParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlTag();

            state._fsp--;

             after(grammarAccess.getXmlTagElementAccess().getStartTagXmlTagParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__StartTagAssignment_0"


    // $ANTLR start "rule__XmlTagElement__PreAssignment_1_1_0"
    // InternalQuizDSL.g:3813:1: rule__XmlTagElement__PreAssignment_1_1_0 : ( RULE_XML_TEXT ) ;
    public final void rule__XmlTagElement__PreAssignment_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3817:1: ( ( RULE_XML_TEXT ) )
            // InternalQuizDSL.g:3818:2: ( RULE_XML_TEXT )
            {
            // InternalQuizDSL.g:3818:2: ( RULE_XML_TEXT )
            // InternalQuizDSL.g:3819:3: RULE_XML_TEXT
            {
             before(grammarAccess.getXmlTagElementAccess().getPreXML_TEXTTerminalRuleCall_1_1_0_0()); 
            match(input,RULE_XML_TEXT,FOLLOW_2); 
             after(grammarAccess.getXmlTagElementAccess().getPreXML_TEXTTerminalRuleCall_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__PreAssignment_1_1_0"


    // $ANTLR start "rule__XmlTagElement__ContentsAssignment_1_1_1"
    // InternalQuizDSL.g:3828:1: rule__XmlTagElement__ContentsAssignment_1_1_1 : ( ruleXmlContents ) ;
    public final void rule__XmlTagElement__ContentsAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3832:1: ( ( ruleXmlContents ) )
            // InternalQuizDSL.g:3833:2: ( ruleXmlContents )
            {
            // InternalQuizDSL.g:3833:2: ( ruleXmlContents )
            // InternalQuizDSL.g:3834:3: ruleXmlContents
            {
             before(grammarAccess.getXmlTagElementAccess().getContentsXmlContentsParserRuleCall_1_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlContents();

            state._fsp--;

             after(grammarAccess.getXmlTagElementAccess().getContentsXmlContentsParserRuleCall_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__ContentsAssignment_1_1_1"


    // $ANTLR start "rule__XmlTagElement__EndTagAssignment_1_1_2_1"
    // InternalQuizDSL.g:3843:1: rule__XmlTagElement__EndTagAssignment_1_1_2_1 : ( RULE_ID ) ;
    public final void rule__XmlTagElement__EndTagAssignment_1_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3847:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:3848:2: ( RULE_ID )
            {
            // InternalQuizDSL.g:3848:2: ( RULE_ID )
            // InternalQuizDSL.g:3849:3: RULE_ID
            {
             before(grammarAccess.getXmlTagElementAccess().getEndTagIDTerminalRuleCall_1_1_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlTagElementAccess().getEndTagIDTerminalRuleCall_1_1_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTagElement__EndTagAssignment_1_1_2_1"


    // $ANTLR start "rule__XmlTag__NameAssignment_0"
    // InternalQuizDSL.g:3858:1: rule__XmlTag__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__XmlTag__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3862:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:3863:2: ( RULE_ID )
            {
            // InternalQuizDSL.g:3863:2: ( RULE_ID )
            // InternalQuizDSL.g:3864:3: RULE_ID
            {
             before(grammarAccess.getXmlTagAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlTagAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__NameAssignment_0"


    // $ANTLR start "rule__XmlTag__AttributesAssignment_1"
    // InternalQuizDSL.g:3873:1: rule__XmlTag__AttributesAssignment_1 : ( ruleXmlAttribute ) ;
    public final void rule__XmlTag__AttributesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3877:1: ( ( ruleXmlAttribute ) )
            // InternalQuizDSL.g:3878:2: ( ruleXmlAttribute )
            {
            // InternalQuizDSL.g:3878:2: ( ruleXmlAttribute )
            // InternalQuizDSL.g:3879:3: ruleXmlAttribute
            {
             before(grammarAccess.getXmlTagAccess().getAttributesXmlAttributeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlAttribute();

            state._fsp--;

             after(grammarAccess.getXmlTagAccess().getAttributesXmlAttributeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlTag__AttributesAssignment_1"


    // $ANTLR start "rule__XmlAttribute__NameAssignment_0"
    // InternalQuizDSL.g:3888:1: rule__XmlAttribute__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__XmlAttribute__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3892:1: ( ( RULE_ID ) )
            // InternalQuizDSL.g:3893:2: ( RULE_ID )
            {
            // InternalQuizDSL.g:3893:2: ( RULE_ID )
            // InternalQuizDSL.g:3894:3: RULE_ID
            {
             before(grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__NameAssignment_0"


    // $ANTLR start "rule__XmlAttribute__ValueAssignment_2"
    // InternalQuizDSL.g:3903:1: rule__XmlAttribute__ValueAssignment_2 : ( RULE_STRING ) ;
    public final void rule__XmlAttribute__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQuizDSL.g:3907:1: ( ( RULE_STRING ) )
            // InternalQuizDSL.g:3908:2: ( RULE_STRING )
            {
            // InternalQuizDSL.g:3908:2: ( RULE_STRING )
            // InternalQuizDSL.g:3909:3: RULE_STRING
            {
             before(grammarAccess.getXmlAttributeAccess().getValueSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getXmlAttributeAccess().getValueSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__ValueAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000200030002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000010600050L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000200050L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000010400050L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000010400052L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010000050L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000021507F070L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000000004F060L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000002020000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x000000001004F070L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000008020000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000040080L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000040040010L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000040000012L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000080000000L});

}