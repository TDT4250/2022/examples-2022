package no.ntnu.tdt4250.osgi.helloworld.api;

public interface IContributor {
	
	public String getMessage();
}
