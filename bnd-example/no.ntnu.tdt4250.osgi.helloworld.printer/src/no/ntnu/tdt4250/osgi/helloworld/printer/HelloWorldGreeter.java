package no.ntnu.tdt4250.osgi.helloworld.printer;

import org.osgi.service.component.annotations.*;

import no.ntnu.tdt4250.osgi.helloworld.api.IContributor;

@Component
public class HelloWorldGreeter {
	
	@Reference
	private IContributor externalContributor;
	
	@Activate
	public void printMessage() {
		
		System.out.println("- Hello from component «" + this.getClass().getName() + "» !");
		System.out.println("- I have just been activated!");
		
		System.out.println("> Contribution from «" + externalContributor.getClass().getName()  + "»");
		System.out.println(externalContributor.getMessage());
		
	}

}
