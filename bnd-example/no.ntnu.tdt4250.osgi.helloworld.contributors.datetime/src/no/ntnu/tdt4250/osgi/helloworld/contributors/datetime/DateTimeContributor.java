package no.ntnu.tdt4250.osgi.helloworld.contributors.datetime;

import java.time.ZonedDateTime;

import org.osgi.service.component.annotations.*;

import no.ntnu.tdt4250.osgi.helloworld.api.IContributor;

@Component
public class DateTimeContributor implements IContributor {

	@Override
	public String getMessage() {
		
		ZonedDateTime now = ZonedDateTime.now();
		return now.toString();
		
	}

}
